export type ExecuteResponseType = {
	success: boolean,
	message?: string
};

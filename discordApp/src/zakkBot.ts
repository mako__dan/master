import {
	Channel, Client, Collection, Guild, GuildChannel, GuildEmoji, GuildMember, Interaction, Message,
	MessageReaction, Role, ThreadChannel, User
} from 'discord.js';
import { io, Socket } from 'socket.io-client';

import { CommandInterface } from './bases/commandInterface';
import { FeatureInterface } from './bases/featureInterface';
import {
	ChannelCreateFeature, ChannelDeleteFeature, ChannelUpdateFeature, EmojiCreateFeature,
	EmojiDeleteFeature, EmojiUpdateFeature, GuildBanAddFeature, GuildBanRemoveFeature,
	GuildCreateFeature, GuildMemberUpdateFeature, GuildUpdateFeature, InteractionCreateFeature,
	InviteCreateFeature, InviteDeleteFeature, MessageCreateFeature, MessageDeleteBulkFeature,
	MessageDeleteFeature, MessageUpdateFeature, RawFeature, ReadyFeature, RoleCreateFeature,
	RoleDeleteFeature, RoleUpdateFeature, StageInstanceCreateFeature, StageInstanceDeleteFeature,
	StageInstanceUpdateFeature, StickerCreateFeature, StickerDeleteFeature, StickerUpdateFeature,
	ThreadCreateFeature, ThreadDeleteFeature, ThreadUpdateFeature, VoiceStateUpdateFeature
} from './features';
import {
	AutoreplyModule, AutoroleModule, EmbedSenderModule, ModerationModule, PlayerModule,
	RoomthemerModule, StatusroleModule, StickyMessageModule, VerifyModule, WelcomerModule
} from './modules';
import { ClientConfig, SocketRequest } from './types/configs';
import { Logger } from './util/logger';
import { ModuleLoader } from './util/moduleLoader';
import { MusicSubscription } from './util/music/subscription';

export class ZakkBot {
	public discordClient: Client;
	public clientConfig: ClientConfig;
	public static features: FeatureInterface[] = [];
	public static commandHandlers: Record<string, CommandInterface> = {};
	public commandResolvers: Record<string, Record<string, string>> = {};
	public interactionResolvers: Record<string, Record<string, string>> = {};

	public playerSubscriptions: Record<string, MusicSubscription> = {};
	public socket: Socket | undefined;

	constructor(clientConfig: ClientConfig) {
		this.clientConfig = clientConfig;
		this.discordClient = new Client(
			{
				intents: [
					'GUILDS',
					'GUILD_MEMBERS',
					'GUILD_BANS',
					'GUILD_EMOJIS_AND_STICKERS',
					'GUILD_INTEGRATIONS',
					'GUILD_WEBHOOKS',
					'GUILD_INVITES',
					'GUILD_VOICE_STATES',
					'GUILD_PRESENCES',
					'GUILD_MESSAGES',
					'GUILD_MESSAGE_REACTIONS',
					'GUILD_MESSAGE_TYPING',
					'DIRECT_MESSAGES',
					'DIRECT_MESSAGE_REACTIONS',
					'DIRECT_MESSAGE_TYPING'
				]
			}
		);
	}

	private async registerSocketCalls(): Promise<void> {
		this.socket = io("ws://localhost:8123/", {});
		this.socket.on("connect", (): void => {
			Logger.log(`connect ${this.socket?.id}`, "warning");
		});

		this.socket.on("disconnect", (): void => {
			Logger.log(`disconnect`, "warning");
		});

		this.socket.on("refreshPrefix",(data: SocketRequest, cb: () => void): void => {
			ModuleLoader.applyCommands(this, data.guildId, "text");
			cb();
		});

		this.socket.on("refreshSlashCommands",(data: SocketRequest, cb: () => void): void => {
			ModuleLoader.applyCommands(this, data.guildId, "slash");
			cb();
		});

		this.socket.on("reloadGuildData",(data: SocketRequest, cb: () => void): void => {
			ModuleLoader.applyCommands(this, data.guildId, "all");
			cb();
		});

		this.socket.on("getChannels",async (data: SocketRequest, cb: (returnString: string) => void): Promise<void> => {
			const queryResult: Guild | void = await this.discordClient.guilds.fetch(data.guildId).catch(() => {return;});
			if (queryResult) {
				const channelsData: Collection<string, GuildChannel | ThreadChannel> = queryResult.channels.cache;
				// const result: Collection<string, GuildChannel | ThreadChannel> = JSON.parse(JSON.stringify(channelsData));
				const result: Collection<string, GuildChannel | ThreadChannel> = channelsData;
				result.forEach((channel: GuildChannel | ThreadChannel): void => {
					if (channel.parentId){
						channel.parentId = channelsData.find((c: Channel): boolean => c.id === channel.parentId)?.name ?? null;
					}
					// delete channel.guild;
					// delete channel.lastMessageId;
					// delete channel.threads;
					// delete channel.deleted;
					// delete channel.permissionOverwrites;
					// delete channel.messages;
					// delete channel.rateLimitPerUser;
					// delete channel.guildId;
					// delete channel.topic;
					// delete channel.lastPinTimestamp;
					// delete channel.rtcRegion;
					// delete channel.bitrate;
					// delete channel.userLimit;
				});
				cb(JSON.stringify(result.filter((pog: GuildChannel | ThreadChannel): boolean => pog.type !== 'GUILD_CATEGORY')));
			}
		});

		this.socket.on("getRoles",async (data: SocketRequest, cb: (returnString: string) => void): Promise<void> => {
			const rolesData:  Collection<string, Role> = (await this.discordClient.guilds.fetch(data.guildId)).roles.cache;
			const result: Collection<string, Role> = rolesData;
			result.forEach((role: any): void => {
				delete role.guild;
				delete role.hoist;
				delete role.permissions;
				delete role.mentionable;
				delete role.deleted;
				delete role.tags;
				delete role.managed;
			});
			cb(JSON.stringify(result));
		});

		this.socket.on("getEmotes",async (data: SocketRequest, cb: (returnString: string) => void): Promise<void> => {
			const emotesData: Collection<string, GuildEmoji> = (await this.discordClient.guilds.fetch(data.guildId)).emojis.cache;
			const result: Collection<string, GuildEmoji> = emotesData.filter((emoji: GuildEmoji): boolean => {if (emoji.available) return true; else return false;});
			cb(JSON.stringify(result));
		});
	}

	private static async registerCommands(): Promise<void[]> {
		return Promise.all([]);
	}

	private static async registerFeatures(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadFeature(new GuildCreateFeature()),
			ModuleLoader.loadFeature(new InteractionCreateFeature()),
			ModuleLoader.loadFeature(new MessageCreateFeature()),
			ModuleLoader.loadFeature(new RawFeature()),
			ModuleLoader.loadFeature(new ReadyFeature()),
			ModuleLoader.loadFeature(new ChannelCreateFeature()),
			ModuleLoader.loadFeature(new ChannelDeleteFeature()),
			ModuleLoader.loadFeature(new ChannelUpdateFeature()),
			ModuleLoader.loadFeature(new EmojiCreateFeature()),
			ModuleLoader.loadFeature(new EmojiDeleteFeature()),
			ModuleLoader.loadFeature(new EmojiUpdateFeature()),
			ModuleLoader.loadFeature(new GuildBanAddFeature()),
			ModuleLoader.loadFeature(new GuildBanRemoveFeature()),
			ModuleLoader.loadFeature(new GuildMemberUpdateFeature()),
			ModuleLoader.loadFeature(new GuildUpdateFeature()),
			ModuleLoader.loadFeature(new InviteCreateFeature()),
			ModuleLoader.loadFeature(new InviteDeleteFeature()),
			ModuleLoader.loadFeature(new MessageDeleteBulkFeature()),
			ModuleLoader.loadFeature(new MessageDeleteFeature()),
			ModuleLoader.loadFeature(new MessageUpdateFeature()),
			ModuleLoader.loadFeature(new RoleCreateFeature()),
			ModuleLoader.loadFeature(new RoleDeleteFeature()),
			ModuleLoader.loadFeature(new RoleUpdateFeature()),
			ModuleLoader.loadFeature(new StageInstanceCreateFeature()),
			ModuleLoader.loadFeature(new StageInstanceDeleteFeature()),
			ModuleLoader.loadFeature(new StageInstanceUpdateFeature()),
			ModuleLoader.loadFeature(new StickerCreateFeature()),
			ModuleLoader.loadFeature(new StickerDeleteFeature()),
			ModuleLoader.loadFeature(new StickerUpdateFeature()),
			ModuleLoader.loadFeature(new ThreadCreateFeature()),
			ModuleLoader.loadFeature(new ThreadDeleteFeature()),
			ModuleLoader.loadFeature(new ThreadUpdateFeature()),
			ModuleLoader.loadFeature(new VoiceStateUpdateFeature()),
		]);
	}

	private static async registerInteractions(): Promise<void[]> {
		return Promise.all([]);
	}

	private async registerModules(): Promise<void[][][]> {
		return Promise.all([
			ModuleLoader.loadModule(this, new AutoreplyModule()),
			ModuleLoader.loadModule(this, new AutoroleModule()),
			ModuleLoader.loadModule(this, new EmbedSenderModule()),
			ModuleLoader.loadModule(this, new ModerationModule()),
			ModuleLoader.loadModule(this, new RoomthemerModule()),
			ModuleLoader.loadModule(this, new StatusroleModule()),
			ModuleLoader.loadModule(this, new StickyMessageModule()),
			ModuleLoader.loadModule(this, new VerifyModule()),
			ModuleLoader.loadModule(this, new WelcomerModule()),
			ModuleLoader.loadModule(this, new PlayerModule())
		]);
	}

	public async runBot(): Promise<void> {
		await this.discordClient.login(this.clientConfig.token).catch(Logger.error);
		await Promise.all([
			this.registerSocketCalls(),
			this.registerModules()
		]);
		await ModuleLoader.applyCommands(this, null, "all");
		ZakkBot.features.forEach(async (event: FeatureInterface): Promise<void> => {
			event.name = event.name.split("/")[0];
			if (event.disabled) return;
			const emitter: Client = this.discordClient;
			emitter[event.once ? 'once' : 'on'](event.name, (...args: (Message | Interaction | GuildMember | MessageReaction | User)[]): Promise<void> => event.run(this, ...args));
		});

		this.discordClient.on('messageCreate', (msg: Message): void => {
			if (msg.channel.type === "DM") {
				msg.author.send("You are DMing me now!");
				return;
			}
		});
	}

	public static async loadGlobalVariables(): Promise<void> {
		await Promise.all([
			this.registerCommands(),
			this.registerFeatures(),
			this.registerInteractions(),
		]);
	}
}
import { model, Model, Schema } from 'mongoose';

import { ConfigModule } from '../types/configs';

// tslint:disable:no-any
export const CommandShema: Schema<ConfigModule, Model<ConfigModule, any, any>, {}>  = new Schema<ConfigModule>({
	id: {type: String, index: true, unique: true},
	enabled: Boolean,
	data: Schema.Types.Mixed,
}, { strict: false });

export const ModuleModel: Model<ConfigModule, any, any>  = model("Module", CommandShema);
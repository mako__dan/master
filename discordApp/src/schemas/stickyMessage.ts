import { model, Model, Schema } from 'mongoose';

import { StickyMessageDatabase } from '../types/configs';

// tslint:disable:no-any
export const StickyMessageShema: Schema<StickyMessageDatabase, Model<StickyMessageDatabase, any, any>, {}> = new Schema<StickyMessageDatabase>({
	channelId: {type: String, index: true, unique: true},
	stickyMessageId: String,
	messagesCount: Number,
	maxMessages: Number
});

export const StickyMessageModel: Model<StickyMessageDatabase, any, any> = model("StickyMessage", StickyMessageShema);
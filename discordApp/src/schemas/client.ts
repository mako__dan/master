import { model, Model, Schema } from 'mongoose';

import { ClientConfig } from '../types/configs';

// tslint:disable:no-any
export const ClientShema: Schema<ClientConfig, Model<ClientConfig, any, any>, {}> = new Schema<ClientConfig>({
	token: {type: String, index: true, unique: true},
	activities: Schema.Types.Mixed,
	mainClusterId: Number,
	logicId: Number,
	availability: String,
	reservedFor: Array,
	id: String
});

export const ClientModel: Model<ClientConfig, any, any> = model("Client", ClientShema);
import { model, Model, Schema } from 'mongoose';

import { InfractionData } from '../types/configs';

// tslint:disable:no-any
export const InfractionShema: Schema<InfractionData, Model<InfractionData, any, any>, {}> = new Schema<InfractionData>({
	id: String,
	guildId: String,
	memberId: String,
	penalisationType: String,
	penalized: Number,
	endsIn: Number
});

export const InfractionModel: Model<InfractionData, any, any> = model("Infraction", InfractionShema);
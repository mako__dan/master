import { model, Model, Schema } from 'mongoose';

import { ConfigCommandObject } from '../types/configs';

// tslint:disable:no-any
export const CommandShema: Schema<ConfigCommandObject, Model<ConfigCommandObject, any, any>, {}> = new Schema<ConfigCommandObject>({
	id: {type: String, index: true, unique: true},

	enabled: Boolean,
	prefixedPhrases: [String],
	slashCommand: {
		name: String,
		description: String,
	},
	phrases: [],
	respondChannelWhitelist: [String],
	respondChannelBlacklist: [String],
	respondRoleWhitelist: [String],
	respondRoleBlacklist: [String],
	removeSelf: Boolean,
	removeTrigger: Boolean,
	removeInvalidTrigger: Boolean,
	removeInvalidSelf: Number,
	sendToChannel: String,
	sendToOrigin: Boolean
});

export const CommandModel: Model<ConfigCommandObject, any, any> = model("Command", CommandShema);
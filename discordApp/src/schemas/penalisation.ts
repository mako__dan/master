import { model, Model, Schema } from 'mongoose';

import { PenalisationData } from '../types/configs';

// tslint:disable:no-any
export const PenalisationShema: Schema<PenalisationData, Model<PenalisationData, any, any>, {}> = new Schema<PenalisationData>({
	guildId: String,
	guildMemberId: String,
	penalisationType: String,
	endsIn: Number
});

export const PenalisationModel: Model<PenalisationData, any, any> = model("Penalisation", PenalisationShema);
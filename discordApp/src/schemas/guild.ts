import { model, Model, Schema } from 'mongoose';

import { ConfigGuild } from '../types/configs';

// tslint:disable:no-any
export const GuildShema: Schema<ConfigGuild, Model<ConfigGuild, any, any>, {}> = new Schema<ConfigGuild>({
	id: {type: String, index: true, unique: true},
	name: String,
	commander: String,
	removeUnknown: Boolean,
	themeColor: String,
	prefix: String,
	argsDivider: String,
	removeInvalidTrigger: Boolean,
	removeInvalidSelf: Number,
	guildId: String
});

export const GuildModel: Model<ConfigGuild, any, any> = model("Guild", GuildShema);
import * as dotenv from 'dotenv';
import mongoose from 'mongoose';

import { ClientConfig } from './types/configs';
import { AppConfig, Configure } from './util/config';
import { ConfigHelper } from './util/helpers/configHelper';
import { Logger } from './util/logger';
import { ZakkBot } from './zakkBot';

dotenv.config();
Configure();
Logger.init();

Logger.log("Trying to connect to mongo","warning");
mongoose.connect(AppConfig.mongoConnection).then((): void => {
	Logger.log('Connected to MongoDB', "success");
	run();
}).catch((err: string): void => {
	Logger.log('Unable to connect to MongoDB Database.\nError: ' + err, "error");
});

async function run(): Promise<void> {
	const startTime: number = Date.now();
	Logger.log('Starting services');
	await ZakkBot.loadGlobalVariables();

	const clients: ClientConfig[] = await ConfigHelper.listClients(Number(AppConfig.ClusterId));
	await Promise.all(clients.map(async (client: ClientConfig): Promise<void> => {
		new ZakkBot(client).runBot();
	}));
	Logger.log('Bot is now running');
	Logger.log(Date.now() - startTime, 'warning');
}

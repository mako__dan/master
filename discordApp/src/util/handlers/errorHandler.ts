import {
	ColorResolvable, CommandInteraction, DMChannel, Message, MessageEmbed, TextBasedChannels
} from 'discord.js';

import { ConfigGuild } from '../../types/configs';
import { ZakkBot } from '../../zakkBot';
import { ConfigHelper } from '../helpers/configHelper';
import { MessageHelper } from '../helpers/messageHelper';
import { Logger } from '../logger';

export class ErrorHandler {
	static handleError(error: string, zakkbotClient: ZakkBot): void {
		Logger.log("Error ocured in app runtime", 'red');
		Logger.log("", 'red', error);
		Logger.log("End of dataDump", 'red');
	}

	static async handleWrongMessage(content: string, zakkBotClient: ZakkBot, messageOrigin: Message, removeTimeout: number): Promise<void> {
		const channel: TextBasedChannels = messageOrigin.channel;
		if (channel instanceof DMChannel || channel.partial){
			return ; // TODO: dm replies;
		} else {
			const serverOptions: ConfigGuild | undefined = await ConfigHelper.getServerOptions(zakkBotClient.clientConfig.id, channel.guild.id);
			if (!serverOptions) return;
			const newEmbed: MessageEmbed = new MessageEmbed()
			.setColor(serverOptions.themeColor as ColorResolvable)
			.setTitle('**Nastala chyba**')
			.setDescription(content);
			MessageHelper.SendRemovable({sendToChannel: "", sendToOrigin: true} , { embeds: [newEmbed] }, messageOrigin.channel, removeTimeout);
		}
	}

	static handleWrongInteraction(content: string, zakkbotClient: ZakkBot, interaction: CommandInteraction): boolean {
		interaction.reply(content);
		return false;
	}
}

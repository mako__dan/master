import { DMChannel, GuildMember, Message, Role, Snowflake, TextBasedChannels } from 'discord.js';

import { CommandInterface } from '../../bases/commandInterface';
import { ConfigCommandObject, ConfigGuild, ConfigModule } from '../../types/configs';
import { ExecuteResponseType } from '../../types/types';
import { ZakkBot } from '../../zakkBot';
import { ConfigHelper } from '../helpers/configHelper';
import { Logger } from '../logger';
import { ErrorHandler } from './errorHandler';

export class CommandHandler {
	public static async handleCommand(message: Message , zakkBotClient: ZakkBot): Promise<void> {
		if (message.author.bot) return;
		const messageChannel: TextBasedChannels = message.channel;
		if (messageChannel instanceof DMChannel || messageChannel.partial){
			Logger.log("DM MESSAGE GOT", message.content);
			// TODO handle dm messages
		} else {
			const guildId: Snowflake = messageChannel.guild.id;
			const serverConfig: ConfigGuild | undefined = await ConfigHelper.getServerOptions(zakkBotClient.clientConfig.id, guildId);
			if (!serverConfig) return;
			if (!message.content.startsWith(serverConfig.prefix)) { return; }
			const args: string[] = message.content.split(serverConfig.argsDivider);
			args[0] = args[0].substring(serverConfig.prefix.length);
			const commandName: string = zakkBotClient.commandResolvers[guildId][args[0]];
			if (!commandName) {
				const errMessage: string = `${args[0]} is disabled or not valid, use ${serverConfig.prefix}help to list all available commands.`;
				ErrorHandler.handleWrongMessage(errMessage, zakkBotClient, message, serverConfig.removeInvalidSelf);
				if (serverConfig.removeInvalidTrigger){
					message.delete();
				}
				return;
			}
			args.shift();

			const commandNameSplit: string[] = commandName.split("/");
			const commandOptions: ConfigCommandObject | undefined = await ConfigHelper.getCommandOptions(zakkBotClient.clientConfig.id, guildId, commandNameSplit[0], commandNameSplit[1]);
			const moduleOption: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, guildId, commandNameSplit[0]);
			if (!commandOptions || !moduleOption || !moduleOption.enabled) return;

			// check if channel whitelisted or blacklisted
			if (message.channel.id !== serverConfig.commander) {
				// check if not blacklisted
				if (commandOptions.respondChannelBlacklist.includes(message.channel.id)) {
					return;
				}
				// check if whitelisted
				if (
					commandOptions.respondChannelWhitelist.length !== 0 &&
					!commandOptions.respondChannelWhitelist.includes(message.channel.id)
				){
					return;
				}
			}
			const MessageAuthor: GuildMember = await messageChannel.guild.members.fetch(message.author.id);
			// check if role not blacklisted
			if (MessageAuthor.roles.cache.some((r: Role)=>(commandOptions.respondChannelBlacklist).includes(r.id))){
				return;
			}
			// check if role whitelisted if possible
			if(
				commandOptions.respondRoleWhitelist.length !== 0 &&
				!MessageAuthor.roles.cache.some((r: Role)=>(commandOptions.respondRoleWhitelist).includes(r.id))
			){
				return;
			}
			const commandHandler: CommandInterface = ZakkBot.commandHandlers[commandNameSplit[1]];
			const CommandHandlerResponse: ExecuteResponseType = await commandHandler.execute(
				message,
				args,
				args.join(serverConfig.argsDivider),
				{
					guild: serverConfig,
					module: moduleOption,
					command: commandOptions
				},
				zakkBotClient
				);
			if(CommandHandlerResponse.success) {
				if (commandOptions.removeTrigger) {
					message.delete();
				}
			} else {
				if (!CommandHandlerResponse.message){
					CommandHandlerResponse.message = `You tried to use ${message.content}, but there was an error. \n ${commandHandler.description} \n ${commandHandler.help}`;
				}
				ErrorHandler.handleWrongMessage(CommandHandlerResponse.message, zakkBotClient, message, commandOptions.removeInvalidSelf);
				if (commandOptions.removeInvalidTrigger) {
					message.delete();
				}
			}
		}
	}
}
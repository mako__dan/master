import { CommandInteraction, GuildMember, Role, Snowflake } from 'discord.js';

import { CommandInterface } from '../../bases/commandInterface';
import { ConfigCommandObject, ConfigGuild, ConfigModule } from '../../types/configs';
import { ExecuteResponseType } from '../../types/types';
import { ZakkBot } from '../../zakkBot';
import { ConfigHelper } from '../helpers/configHelper';
import { Logger } from '../logger';

export class InteractionHandler {
	public static async handleInteraction(zakkBotClient: ZakkBot, interaction: CommandInteraction): Promise<void> {
		await interaction.deferReply();
		if (!interaction || !interaction.isCommand() || !interaction.guild) {
			interaction.deleteReply();
			return;
		}
		const guildId: Snowflake = interaction.guild.id;
		const serverConfig: ConfigGuild | undefined = await ConfigHelper.getServerOptions(zakkBotClient.clientConfig.id, guildId);
		if (!serverConfig) {
			interaction.deleteReply();
			return;
		}
		const commandName: string = zakkBotClient.interactionResolvers[guildId][interaction.commandName];
		if (!commandName) {
			interaction.deleteReply();
			return;
		}
		const commandNameSplit: string[] = commandName.split("/");
		const commandOptions: ConfigCommandObject | undefined = await ConfigHelper.getCommandOptions(zakkBotClient.clientConfig.id, guildId, commandNameSplit[0], commandNameSplit[1]);
		const moduleOption: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, guildId, commandNameSplit[0]);
		if (!commandOptions || !moduleOption || !moduleOption.enabled) {
			interaction.deleteReply();
			return;
		}
		// check if channel whitelisted or blacklisted
		if (interaction.channel && interaction.channel.id !== serverConfig.commander) {
			// check if not blacklisted
			if (commandOptions.respondChannelBlacklist.includes(interaction.channel.id)) {
				interaction.deleteReply();
				return;
			}
			// check if whitelisted
			if (
				commandOptions.respondChannelWhitelist.length !== 0 &&
				!commandOptions.respondChannelWhitelist.includes(interaction.channel.id)
			) {
				interaction.deleteReply();
				return;
			}
		}
		const MessageAuthor: GuildMember | null = await interaction.member as GuildMember | null;
		if (!MessageAuthor) {
			interaction.deleteReply();
			return;
		}
		// check if role not blacklisted
		if (MessageAuthor.roles.cache.some((r: Role) => (commandOptions.respondChannelBlacklist).includes(r.id))) {
			interaction.deleteReply();
			return;
		}
		// check if role whitelisted if possible
		if (
			commandOptions.respondRoleWhitelist.length !== 0 &&
			!MessageAuthor.roles.cache.some((r: Role) => (commandOptions.respondRoleWhitelist).includes(r.id))
		) {
			interaction.deleteReply();
			return;
		}
		const commandHandler: CommandInterface = ZakkBot.commandHandlers[commandNameSplit[1]];
		const commandHandlerResponse: ExecuteResponseType = await commandHandler.executeInteraction(
			interaction,
			{
				guild: serverConfig,
				module: moduleOption,
				command: commandOptions
			},
			zakkBotClient
		);
		if (commandHandlerResponse.success) {
			if (commandOptions.removeTrigger) {
				await interaction.deleteReply().catch(Logger.error);
			}
		} else {
			if (!commandHandlerResponse.message) {
				commandHandlerResponse.message = `You tried to use /${interaction.commandName}, but there was an error.`;
			}
			await interaction.editReply(commandHandlerResponse.message);
			if (commandOptions.removeInvalidTrigger) {
				setTimeout((): void => {
					interaction.deleteReply().catch(Logger.error);
				}, 5000);
			}
		}
	}
}
import { GuildMember, Role, Snowflake } from 'discord.js';

import { Logger } from '../logger';

export class RoleHelper {
	public static applyRoles(user: GuildMember, roles: Snowflake[]): void {
		if (roles.length !== 0) {
			for (const role of roles) {
				this.applyRole(user, role);
			}
		}
	}

	public static applyRole(user: GuildMember, role: Snowflake): void {
		const addRole: Role | undefined = user.guild.roles.cache.find((roleToFind: Role): boolean => roleToFind.id === role);
		if (addRole) {
			user.roles.add(addRole).catch((err: string) => {Logger.error(err); Logger.error("src/util/helpers/roleHelper.ts:16");});
		}
	}

	public static removeRoles(user: GuildMember, roles: Snowflake[]): void {
		if (roles.length !== 0) {
			for (const role of roles) {
				this.removeRole(user, role);
			}
		}
	}

	public static removeRole(user: GuildMember, role: Snowflake): void {
		const remRole: Role | undefined = user.guild.roles.cache.find((roleToFind: Role): boolean => roleToFind.id === role);
		if (remRole) {
			user.roles.remove(remRole).catch((err: string) => {Logger.error(err); Logger.error("src/util/helpers/roleHelper.ts:31");});
		}
	}
}
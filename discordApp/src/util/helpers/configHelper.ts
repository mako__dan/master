import { Snowflake } from 'discord.js';

import { ClientModel } from '../../schemas/client';
import { CommandModel } from '../../schemas/command';
import { GuildModel } from '../../schemas/guild';
import { InfractionModel } from '../../schemas/infraction';
import { ModuleModel } from '../../schemas/module';
import { PenalisationModel } from '../../schemas/penalisation';
import { StickyMessageModel } from '../../schemas/stickyMessage';
import {
	ClientConfig, ConfigCommandObject, ConfigGuild, ConfigModule, InfractionData, PenalisationData,
	StickyMessageDatabase
} from '../../types/configs';

export class ConfigHelper {
	public static async getModuleOptions(botId: string, guildId: string, moduleName: string): Promise<ConfigModule | undefined> {
		return await ModuleModel.findOne({ id: botId + "/" + guildId + "/" + moduleName});
	}

	public static async getModulesByGuild(botId: string, guildId: string): Promise<ConfigModule[] | undefined> {
		return await ModuleModel.find({ id: new RegExp(botId + "/" + guildId)});
	}

	public static async getCommandOptions(botId: string, guildId: string, moduleName: string, commandName: string): Promise<ConfigCommandObject | undefined> {
		return await CommandModel.findOne({ id: botId + "/" + guildId + "/" + moduleName + "/" + commandName});
	}

	public static async getCommandsByGuild(botId: string, guildId: string): Promise<ConfigCommandObject[] | undefined> {
		return await CommandModel.find({ id: new RegExp(botId + "/" + guildId)});
	}

	public static async getCommandOptionsByModule(botId: string, guildId: string, moduleName: string): Promise<ConfigCommandObject[] | undefined> {
		return await CommandModel.find({ id: new RegExp(botId + "/" + guildId + "/" + moduleName + "/", "g")});
	}

	public static async getServerOptions(botId: string, guildId: Snowflake): Promise<ConfigGuild | undefined> {
		return await GuildModel.findOne({ id: botId + "/" + guildId});
	}

	public static async getServers(botId: string): Promise<ConfigGuild[] | undefined> {
		return await GuildModel.find({ id: new RegExp(botId + "/", "g")});
	}

	public static async getStickymessage(channelId: Snowflake): Promise<StickyMessageDatabase | undefined> {
		return await StickyMessageModel.findOne({channelId});
	}

	public static async setStickymessage(channelId: Snowflake,data: StickyMessageDatabase): Promise<StickyMessageDatabase | undefined> {
		return await StickyMessageModel.findOneAndReplace({channelId}, data);
	}

	public static async addInfraction(data: InfractionData |  InfractionData[]): Promise< InfractionData | undefined> {
		return await InfractionModel.create(data);
	}

	public static async getInfractions(guildId: Snowflake, memberId: Snowflake): Promise<ConfigGuild[] | undefined> {
		return await InfractionModel.find({ id: new RegExp(guildId + "/" + memberId, "g")});
	}

	public static async addPenalisation(data: PenalisationData | PenalisationData[]): Promise<PenalisationData | undefined> {
		return await PenalisationModel.create(data);
	}

	public static async getPenalisation(guildId: Snowflake, memberId: Snowflake, type: number): Promise<PenalisationData | undefined> {
		return await PenalisationModel.findOne({guildId, guildMemberId: memberId, penalisationType: type});
	}

	public static async updatePenalisation(data: PenalisationData): Promise<PenalisationData | undefined> {
		return await PenalisationModel.findOneAndReplace({guildMemberId: data.guildMemberId, guildId: data.guildId, penalisationType: data.penalisationType}, data);
	}

	public static async deletePenalisation(data: PenalisationData): Promise<PenalisationData | undefined> {
		return await PenalisationModel.findOneAndDelete(data);
	}

	public static async listPenalisations(): Promise<PenalisationData[] | undefined> {
		return await PenalisationModel.find();
	}

	public static async listClients(clusterId: number): Promise<ClientConfig[]> {
		return await ClientModel.find({mainClusterId: clusterId});
	}

}
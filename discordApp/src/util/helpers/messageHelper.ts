import {
	Channel, DMChannel, EmbedFieldData, Guild, GuildMember, Message, MessageEmbed,
	MessageEmbedOptions, MessageOptions, TextBasedChannels, TextChannel
} from 'discord.js';
import moment from 'moment';

import { ConfigBatch, ConfigCommandObject, ConfigMessageSender } from '../../types/configs';

export class MessageHelper {
	public static async Send(config: ConfigCommandObject | ConfigMessageSender, messageToSend: MessageOptions, originalMessageChannel: TextBasedChannels): Promise<Message | void> {
		if (config.sendToChannel){
			if (!originalMessageChannel || originalMessageChannel instanceof DMChannel || originalMessageChannel?.partial) {
				return;
			}
			const channel: TextChannel = originalMessageChannel.guild.channels.cache.get(config.sendToChannel) as TextChannel;

			if (channel) {
				return await channel.send(messageToSend).catch(() => {return;});
			}
		}
		if (originalMessageChannel && config.sendToOrigin && config.sendToChannel !== originalMessageChannel.id){
			return await originalMessageChannel.send(messageToSend);
		}
	}

	public static async SendRemovable(config: ConfigCommandObject | ConfigMessageSender, messageToSend: MessageOptions, originalMessageChannel: TextBasedChannels, timeout: number): Promise<Message | void> {
		if (timeout === 0){
			return await this.Send(config, messageToSend, originalMessageChannel);
		}
		const newMessage: Message | void = await this.Send(config, messageToSend, originalMessageChannel);
		if (newMessage) {
			setTimeout((): void => {
				newMessage.delete();
			}, timeout*1000);
		}
	}

	public static ReplaceWildcardsInBuilder(embed: MessageOptions , data: {
		channel?: Channel,
		guild?: Guild,
		member?:GuildMember,
		botUser?: GuildMember,
		author?: GuildMember,
		penalty?: {time?: string, reason: string}
	}): MessageOptions {
		if (embed.content) embed.content = this.ReplaceWildcards(embed.content, data);
		if (embed.embeds) {
			embed.embeds?.forEach((embedData: MessageEmbed | MessageEmbedOptions): void => {
				if (embedData.author) {
					if (embedData.author.name) embedData.author.name = this.ReplaceWildcards(embedData.author.name, data);
					if (embedData.author.url) embedData.author.url = this.ReplaceWildcards(embedData.author.url, data);
					if (embedData.author.iconURL) embedData.author.iconURL = this.ReplaceWildcards(embedData.author.iconURL, data);
				}
				if (embedData.description) embedData.description = this.ReplaceWildcards(embedData.description, data);
				if (embedData.fields) {
					embedData.fields?.forEach((field: EmbedFieldData) => {
						if (field.name) field.name = this.ReplaceWildcards(field.name, data);
						if (field.value) field.value = this.ReplaceWildcards(field.value, data);
					});
				}
				if (embedData.description) embedData.description = this.ReplaceWildcards(embedData.description, data);
				if (embedData.footer) {
					if (embedData.footer.iconURL) embedData.footer.iconURL = this.ReplaceWildcards(embedData.footer.iconURL, data);
					if (embedData.footer.text) embedData.footer.text = this.ReplaceWildcards(embedData.footer.text, data);
				}
				if (embedData.image?.url) embedData.image.url = this.ReplaceWildcards(embedData.image.url, data);
				if (embedData.title) embedData.title = this.ReplaceWildcards(embedData.title, data);
				if (embedData.url) embedData.url = this.ReplaceWildcards(embedData.url, data);
				if (embedData.video?.url) embedData.video.url = this.ReplaceWildcards(embedData.video.url, data);
				if (embedData.thumbnail?.url) embedData.thumbnail.url = this.ReplaceWildcards(embedData.thumbnail.url, data);
			});
		}
		return embed;
	}

	public static ReplaceWildcards(content: string, data: {
		channel?: Channel,
		guild?: Guild,
		member?:GuildMember,
		botUser?: GuildMember,
		author?: GuildMember,
		penalty?: {time?: string, reason: string},
		config?: ConfigBatch
	}): string {
		if (data.guild) {
			content = content.replace(/{server}/gi, data.guild.name);
			content = content.replace(/{server.members}/gi, data.guild.memberCount.toString());
		}

		if (data.channel) {
			content = content.replace(/{channel}/gi, `<#${data.channel.id}>`);
		}

		if (data.member) {
			content = content
				.replace(/{user}/gi, data.member.user.username)
				.replace(/{user.mention}/gi, `<@${data.member.id}>`)
				.replace(/{user.nickname}/gi, data.member.nickname ?? data.member.user.username)
				.replace(/{user.avatar}/gi, data.member.user.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png")
				.replace(/{user.joined}/gi, moment(data.member.joinedTimestamp).format('DD/MM/YYYY hh:mm:ss'))
				.replace(/{user.created}/gi, moment(data.member.user.createdTimestamp).format('DD/MM/YYYY hh:mm:ss'));
		}

		if (data.botUser) {
			content = content
				.replace(/{bot}/gi, data.botUser.user.username)
				.replace(/{bot.mention}/gi, `<@${data.botUser.id}>`)
				.replace(/{bot.nickname}/gi, data.botUser.nickname ?? data.botUser.user.username)
				.replace(/{bot.avatar}/gi, data.botUser.user.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png")
				.replace(/{bot.joined}/gi, moment(data.botUser.joinedTimestamp).format('DD/MM/YYYY'))
				.replace(/{bot.created}/gi, moment(data.botUser.user.createdTimestamp).format('DD/MM/YYYY'));
		}

		if (data.author) {
			content = content
				.replace(/{author}/gi, data.author.user.username)
				.replace(/{author.mention}/gi, `<@${data.author.id}>`)
				.replace(/{author.nickname}/gi, data.author.nickname ?? data.author.user.username)
				.replace(/{author.avatar}/gi, data.author.user.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png")
				.replace(/{author.joined}/gi, moment(data.author.joinedTimestamp).format('DD/MM/YYYY'))
				.replace(/{author.created}/gi, moment(data.author.user.createdTimestamp).format('DD/MM/YYYY'));
		}

		if (data.penalty) {
			content = content
				.replace(/{penalty.reason}/gi, data.penalty.reason);
		}
		if (data.penalty?.time) {
			content = content
				.replace(/{penalty.time}/gi, moment(data.penalty.time).format('DD/MM/YYYY'));
		}

		return content
			.replace(/{date}/gi, moment().format('DD/MM/YYYY'))
			.replace(/{datetime}/gi, moment().format('DD/MM/YYYY hh:mm:ss'))
			.replace(/{everyone}/gi, '@everyone')
			.replace(/{here}/gi, '@here')
			.replace(/{urlencode:(.*)}/g, (match: string, value: string) => encodeURIComponent(value));
	}
}
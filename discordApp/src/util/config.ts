import { Error } from 'mongoose';

export function Configure(): void {
	if (process.env.mongoConnection && process.env.ClusterId && process.env.lyricsToken) {
			AppConfig.mongoConnection = process.env.mongoConnection;
			AppConfig.ClusterId = Number(process.env.ClusterId);
			AppConfig.lyricsToken = process.env.lyricsToken;
	} else {
		throw new Error("Configuration is missing");
	}
}

export const AppConfig: {ClusterId: number, mongoConnection: string, lyricsToken: string} = {
	ClusterId: 0,
	mongoConnection: "",
	lyricsToken: ""
};
import { promisify } from 'util';

import {
	AudioPlayer, AudioPlayerError, AudioPlayerState, AudioPlayerStatus, AudioResource,
	createAudioPlayer, entersState, VoiceConnection, VoiceConnectionDisconnectReason,
	VoiceConnectionState, VoiceConnectionStatus
} from '@discordjs/voice';

import { Track } from './track';

const wait: {(ms: number): Promise<void>; <T>(ms: number, value: T): Promise<T>; } = promisify(setTimeout);

/**
 * A MusicSubscription exists for each active VoiceConnection. Each subscription has its own audio player and queue,
 * and it also attaches logic to the audio player and voice connection for error handling and reconnection logic.
 */
export class MusicSubscription {
	public readonly voiceConnection: VoiceConnection;
	public readonly audioPlayer: AudioPlayer;
	public queue: Track[];
	public queueLock: boolean = false;
	public readyLock: boolean = false;
	public autoplayQueue: boolean = false;
	public skipFirst: boolean = true;

	public constructor(voiceConnection: VoiceConnection) {
		this.voiceConnection = voiceConnection;
		this.audioPlayer = createAudioPlayer();
		this.queue = [];

		this.voiceConnection.on('stateChange', async (_: VoiceConnectionState, newState: VoiceConnectionState): Promise<void> => {
			if (newState.status === VoiceConnectionStatus.Disconnected) {
				if (newState.reason === VoiceConnectionDisconnectReason.WebSocketClose && newState.closeCode === 4014) {
					try {
						await entersState(this.voiceConnection, VoiceConnectionStatus.Connecting, 5_000);
					} catch {
						this.voiceConnection.destroy();
					}
				} else if (this.voiceConnection.rejoinAttempts < 5) {
					await wait((this.voiceConnection.rejoinAttempts + 1) * 5_000);
					this.voiceConnection.rejoin();
				} else {
					this.voiceConnection.destroy();
				}
			} else if (newState.status === VoiceConnectionStatus.Destroyed) {
				this.stop();
			} else if (
				!this.readyLock &&
				(newState.status === VoiceConnectionStatus.Connecting || newState.status === VoiceConnectionStatus.Signalling)
			) {
				this.readyLock = true;
				try {
					await entersState(this.voiceConnection, VoiceConnectionStatus.Ready, 20_000);
				} catch {
					if (this.voiceConnection.state.status !== VoiceConnectionStatus.Destroyed) this.voiceConnection.destroy();
				} finally {
					this.readyLock = false;
				}
			}
		});

		// Configure audio player
		this.audioPlayer.on('stateChange', async (oldState: AudioPlayerState, newState: AudioPlayerState): Promise<void> => {
			if (newState.status === AudioPlayerStatus.Idle && oldState.status !== AudioPlayerStatus.Idle) {
				this.queue.shift();
				void this.processQueue();
			} else if (newState.status === AudioPlayerStatus.Playing) {
				(newState.resource as AudioResource<Track>).metadata.onStart(await this.queue[0], await this.queue[1]);
			}
		});

		this.audioPlayer.on('error', (error: AudioPlayerError): void => (error.resource as AudioResource<Track>).metadata.onError(error));

		voiceConnection.subscribe(this.audioPlayer);
	}

	/**
	 * Adds a new Track to the queue.
	 *
	 * @param track The track to add to the queue
	 */
	public enqueue(track: Track): void {
		if (this.autoplayQueue){
			this.audioPlayer.stop();
			this.queue = [];
			this.autoplayQueue = false;
			this.queue.push(track);
		}
		this.queue.push(track);
		void this.processQueue();
	}

	/**
	 * Adds a new Track to the top of queue.
	 *
	 * @param track The track to add to the queue
	 */
	public enqueueTop(track: Track): void {
		if (this.autoplayQueue){
			this.queue = [];
			this.audioPlayer.stop();
			this.autoplayQueue = false;
			this.queue.push(track);
		}
		this.queue.unshift(track);
		void this.processQueue();
	}

	/**
	 * Adds a new Track to the top of queue.
	 *
	 * @param track The track to add to the queue
	 */
	public enqueueNow(track: Track): void {
		if (this.autoplayQueue){
			this.queue = [];
			this.audioPlayer.stop();
			this.autoplayQueue = false;
			this.queue.push(track);
		}
		this.queue.unshift(track);
		this.audioPlayer.stop();
		void this.processQueue();
	}

	/**
	 * Stops audio playback and empties the queue
	 */
	public stop(): void {
		this.queueLock = true;
		this.queue = [];
		this.audioPlayer.stop(true);
	}

	/**
	 * Attempts to play a Track from the queue
	 */
	private async processQueue(): Promise<void> {
		// If the queue is locked (already being processed), is empty, or the audio player is already playing something, return
		if (this.queueLock || this.audioPlayer.state.status !== AudioPlayerStatus.Idle || this.queue.length === 0) {
			return;
		}
		// Lock the queue to guarantee safe access
		this.queueLock = true;

		// Take the first item from the queue. This is guaranteed to exist due to the non-empty check above.
		const nextTrack: Track = await this.queue[0];
		try {
			// Attempt to convert the Track into an AudioResource (i.e. start streaming the video)
			const resource: AudioResource<Track> = await nextTrack.createAudioResource();
			this.audioPlayer.play(resource);
			this.queueLock = false;
		} catch (error) {
			if (nextTrack) {
				// If an error occurred, try the next item of the queue instead
				nextTrack.onError(error as Error);
			}
			this.queueLock = false;
			return this.processQueue();
		}
	}
}

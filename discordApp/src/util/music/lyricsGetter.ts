import axios from 'axios';
import cheerio, { Cheerio, CheerioAPI, Element } from 'cheerio';

type SearchSongResult = {
	id: number;
	title: string;
	albumArt: string;
	url: string;
};

type GeniusApiResponse = {
	meta: {
		status: number;
	};
	response: {
		hits: GeniusApiResponseHit[];
	};
};

type GeniusApiResponseHit = {
	highlights: [];
	index: string;
	type: string;
	result: GeniusApiResponseResult
};

type GeniusApiResponseResult = {
	annotation_count: number;
	api_path: string;
	artist_names: string;
	full_title: string;
	header_image_thumbnail_url: string;
	header_image_url: string;
	id: number;
	lyrics_owner_id: number;
	lyrics_state: string;
	path: string;
	pyongs_count: number;
	song_art_image_thumbnail_url: string;
	song_art_image_url: string;
	stats: {
		unreviewed_annotations: number;
		hot: boolean;
	};
	title: string;
	title_with_featured: string;
	url: string;
	primary_artist: {
		api_path: string;
		header_image_url: string;
		id: number;
		image_url: string;
		is_meme_verified: boolean;
		is_verified: boolean;
		name: string;
		url: string;
	};
};

export class LyricsGetter {
	public static async getLyrics(arg: {
		apiKey: string;
		title: string;
		authHeader?: boolean;
	}): Promise<string | null> {
		try {
			if (arg && typeof arg === "string") {
				return await this.extractLyrics(arg);
			} else {
				const results: SearchSongResult[] | null =
					await LyricsGetter.searchSong(arg);
				if (!results)
					return 'This song has not been found in our database. Try to use song name in command. (Youtube and other sites can include "lyrics", "official video" and other texts.)';
				return await this.extractLyrics(results[0].url);
			}
		} catch (e) {
			throw e;
		}
	}

	public static async searchSong(options: {
		apiKey: string;
		title: string;
		authHeader?: boolean;
	}): Promise<SearchSongResult[] | null> {
		const searchUrl: string = "https://api.genius.com/search?q=";
		try {
			const {
				apiKey,
				title,
				authHeader = false,
			}: { apiKey: string; title: string; authHeader?: boolean } = options;
			const reqUrl: string = `${searchUrl}${encodeURIComponent(title)}`;
			const headers: { Authorization: string } = {
				Authorization: "Bearer " + apiKey,
			};
			const { data }: { data: GeniusApiResponse } = await axios.get(
				authHeader ? reqUrl : `${reqUrl}&access_token=${apiKey}`,
				authHeader ? { headers } : undefined
			);
			if (data.response.hits.length === 0) return null;
			const results: SearchSongResult[] = data.response.hits.map((val: GeniusApiResponseHit) => {
				const {
					full_title,
					song_art_image_url,
					id,
					url,
				}: {
					full_title: string;
					song_art_image_url: string;
					id: number;
					url: string;
				} = val.result;
				return { id, title: full_title, albumArt: song_art_image_url, url };
			});
			return results;
		} catch (e) {
			throw e;
		}
	}

	public static async extractLyrics(url: string): Promise<string> {
		try {
			const { data }: { data: string } = await axios.get(url);
			const $: CheerioAPI = cheerio.load(data);
			let lyrics: string = $('div[class="lyrics"]').text().trim();
			if (!lyrics) {
				lyrics = "";
				$('div[class^="Lyrics__Container"]').each(
					(i: number, elem: Element): void => {
						if ($(elem).text().length !== 0) {
							const element: Cheerio<Element> = $(elem);
							if (element) {
								let snippet: string = element?.html() ?? "";
								snippet = snippet.replace(/<br[^>]*>/gi, "\n");
								lyrics += cheerio.load(snippet).text() + "\n\n";
							}
						}
					}
				);
			}
			return (
				lyrics?.trim() ??
				'This song has not been found in our database. Try to use song name in command. (Youtube and other sites can include "lyrics", "official video" and other texts.)'
			);
		} catch (e) {
			throw e;
		}
	}

	public static async getTitle(title: string, artist: string): Promise<string> {
		return `${title} ${artist}`
			.toLowerCase()
			.replace(/ *\([^)]*\) */g, "")
			.replace(/ *\[[^\]]*]/, "")
			.replace(/feat.|ft./g, "")
			.replace(/\s+/g, " ")
			.trim();
	}
}

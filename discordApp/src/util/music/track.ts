import { GuildMember } from 'discord.js';
import execa from 'execa';
import { Readable } from 'stream';
import { raw as ytdl } from 'youtube-dl-exec';
import { getInfo, videoInfo } from 'ytdl-core';
import ytpl from 'ytpl';
import ytsr, { Result } from 'ytsr';

import { AudioResource, createAudioResource, demuxProbe, ProbeInfo } from '@discordjs/voice';

/**
 * This is the data required to create a Track object
 */
export interface TrackData {
	url: string;
	title?: string;
	author?: string;
	enquedBy: GuildMember;
	time?: string;
	thumbnail?: string;
	onStart: (track: Track, nextTrack: Track) => void;
	onError: (error: Error) => void;
}

// tslint-disable-next-line no-empty
const noop = (): void => {/**/};

export class Track implements TrackData {
	public url: string;
	public title: string;
	public author: string;
	public enquedBy: GuildMember;
	public time: string;
	public thumbnail: string;
	public onStart: (track: Track, nextTrack: Track) => void;
	public onError: (error: Error) => void;

	private constructor({ url, enquedBy, onStart, onError }: TrackData) {
		this.title = "Loading";
		this.author = "Loading";
		this.time = "00:00";
		this.thumbnail = "";
		this.url = url;
		this.enquedBy = enquedBy;
		this.onStart = onStart;
		this.onError = onError;
		this.fulfillTrack();
	}

	/**
	 * Creates an AudioResource from this Track.
	 */
	public createAudioResource(): Promise<AudioResource<Track>> {
		return new Promise((resolve: (value: AudioResource<Track> | PromiseLike<AudioResource<Track>>) => void, reject: (reason?: Error | string) => void): void => {
				const process: execa.ExecaChildProcess<string> = ytdl(
					this.url,
					{
						o: '-',
						q: '',
						f: 'bestaudio[ext=webm+acodec=opus+asr=48000]/bestaudio',
						r: '100K',
					},
					{ stdio: ['ignore', 'pipe', 'ignore'] },
				);
				if (!process.stdout) {
					reject(new Error('No stdout'));
					return;
				}
				const stream: Readable = process.stdout;
				const onError = (error: Error): void => {
					if (!process.killed) process.kill();
					stream.resume();
					reject(error);
				};
				process
					.once('spawn', (): void => {
						demuxProbe(stream)
							.then((probe: ProbeInfo): void => resolve(createAudioResource(probe.stream, { metadata: this, inputType: probe.type })))
							.catch(onError);
					})
					.catch(onError);
		});
	}

	/**
	 * Creates a Track from a video URL and lifecycle callback methods.
	 *
	 * @param url The URL of the video
	 * @param methods Lifecycle callbacks
	 * @returns The created Track
	 */
	public static async from(content: string, enquedBy: GuildMember, methods: Pick<Track, 'onStart' | 'onError'>): Promise<Track[] | void > {
		const youtubePlaylistRegexp: RegExp = /https?:\/\/(?:www\.)?(?:music\.)?youtu(?:be\.com\/playlist\?list=|\.be\/)([\w-]{34})/;
		const youtubeVideoRegexp: RegExp = /(?:(?:http(?:s?):\/\/)?(?:www\.)?(?:music\.)?)(?:youtu\.?be(?:\.com)?\/(?!oembed))(?:(?:watch\?v(?:=|%3D))|(?:v\/))([a-z0-9_-]+)/;

		// youtube video
		const ytVideoResult: RegExpMatchArray | null = content.match(youtubeVideoRegexp);

		if (content.endsWith(".mp3")) {
			return [await Track.createTrack(content, enquedBy, methods)];
		}

		if (ytVideoResult !== null) {
			return [await Track.createTrack(ytVideoResult[0], enquedBy, methods)];
		}

		// youtube playlist
		const ytPlaylistResult: RegExpMatchArray | null = content.match(youtubePlaylistRegexp);
		if (ytPlaylistResult !== null){
			const playlistData: ytpl.Result = await ytpl(ytPlaylistResult[0]);
			const tracks: Track[] = [];
			for(let i: number = 1; i < playlistData.items.length; i++) {
				tracks.push(this.createTrack(playlistData.items[i].shortUrl, enquedBy, methods));
			}
			return tracks;
		}

		// default: youtube search
		const searchResult: Result = await ytsr(content, {limit: 1});
		const item: ytsr.Item = searchResult.items[0];
		if (item.type === "video"){
			return [await Track.createTrack(item.url, enquedBy, methods)];
		}
	}

	public static createTrack(url: string, enquedBy: GuildMember, methods: Pick<Track, 'onStart' | 'onError'>): Track {
		// The methods are wrapped so that we can ensure that they are only called once.
		const wrappedMethods: { onStart(track: Track, nextTrack: Track): void; onError(error: Error): void; } = {
			onStart(track: Track, nexttrack: Track): void {
				wrappedMethods.onStart = noop;
				methods.onStart(track, nexttrack);
			},
			onError(error: Error): void {
				wrappedMethods.onError = noop;
				methods.onError(error);
			},
		};

		return new Track({
			url,
			enquedBy,
			...wrappedMethods,
		});

	}

	public async fulfillTrack(): Promise<void> {
		try {
			const info: videoInfo = await getInfo(this.url);
			let timeString: string = "";
			let totalSeconds: number = Number(info.videoDetails.lengthSeconds);
			const hours: number = Math.floor(totalSeconds / 3600);
			totalSeconds %= 3600;
			const minutes: number = Math.floor(totalSeconds / 60);
			const seconds: number = totalSeconds % 60;

			if (hours !== 0){
				timeString += hours + ":";
			}
			if (minutes !== 0){
				timeString += minutes + ":";
			}
			if (seconds !== 0){
				timeString += seconds;
			}
			this.title = info.videoDetails.title;
			this.author = info.videoDetails.author.name;
			this.time = timeString;
			this.thumbnail = info.videoDetails.thumbnails[3].url;
		} catch {
			this.title = "Unknown audio stream (probably radio station)";
			this.author = "Unknown";
			this.time = "Unknown";
			this.thumbnail = "";
		}
	}
}

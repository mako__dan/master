/* tslint:disable */

import fs from 'fs';

export class Logger {
	static colors = require('colors/safe');
	static loadTime: number;
	static init(){
		this.colors.setTheme({
			error: 'brightRed',
			warning: 'brightYellow',
			success: 'brightGreen',
			util: 'blue',
			module: 'magenta',
			feature: 'cyan'
		});
		this.loadTime = Date.now();
	}

	static log(message: string | number, color: string = 'green', dataJson?: unknown ): void {
		console.log(this.colors[color](message), dataJson ?? '');
	}

	static error(message: string ): void {
		Logger.log(message, "error");
	}

	static debug(message: string): void {
		Logger.log(message, 'warning');
	}

	static file(filename: string, data: unknown): void {
		fs.writeFileSync("./" + filename + ".log.json", JSON.stringify(data, null, 2));
	}
}

import { ChatInputApplicationCommandData, Guild, Snowflake } from 'discord.js';

import { CommandInterface } from '../bases/commandInterface';
import { FeatureInterface } from '../bases/featureInterface';
import { ModuleInterface } from '../bases/moduleInterface';
import { ConfigCommandObject, ConfigGuild, ConfigModule, FullConfig } from '../types/configs';
import { ZakkBot } from '../zakkBot';
import { ConfigHelper } from './helpers/configHelper';
import { Logger } from './logger';

export class ModuleLoader {
	static async loadCommand(moduleName: string, commandHandler: CommandInterface): Promise<void> {
		ZakkBot.commandHandlers[commandHandler.name] = commandHandler;
	}

	static async loadFeature(commandHandler: FeatureInterface): Promise<void> {
		ZakkBot.features.push(commandHandler);
	}

	static async loadModule(zakkBotClient: ZakkBot, moduleObject: ModuleInterface): Promise<void[][]>{
		return moduleObject.init(zakkBotClient);
	}

	static async applyTextCommands(zakkBotClient: ZakkBot, guildId: Snowflake, configBatch: FullConfig): Promise<void> {
		zakkBotClient.commandResolvers[guildId] = {};

		const commands: ConfigCommandObject[] | undefined = await ConfigHelper.getCommandsByGuild(zakkBotClient.clientConfig.id, guildId);
		if (commands) {
			for (const command of commands) {
				const moduleName: string = command.id.split("/").splice(0,3).join("/");
				const module: ConfigModule | undefined = configBatch.modules?.find((moduleToFind: ConfigModule): boolean => moduleToFind.id === moduleName);
				if (command.enabled && module && module.enabled) {
					const commandName: string[] = command.id.split("/");
					command.prefixedPhrases.forEach((prefix: string): void => {
						zakkBotClient.commandResolvers[guildId][prefix] = commandName[2] + "/" +  commandName[3];
					});
				}
			}
		}
	}

	static async prepareSlashCommands(guildId: Snowflake, zakkBotClient: ZakkBot, configBatch: FullConfig): Promise<ChatInputApplicationCommandData[]> {
		zakkBotClient.interactionResolvers[guildId] = {};

		const slashCommands: ChatInputApplicationCommandData[] = [];
		if (configBatch.commands) {
			for (const command of configBatch.commands) {
				const moduleName: string = command.id.split("/").splice(0,3).join("/");
				const module: ConfigModule | undefined = configBatch.modules?.find((moduleToFind: ConfigModule): boolean => moduleToFind.id === moduleName);
				if (command.slashCommand && command.enabled && module && module.enabled) {
					const commandName: string[] = command.id.split("/");
					zakkBotClient.interactionResolvers[guildId][command.slashCommand.name] = commandName[2] + "/" +  commandName[3];
					command.slashCommand.options = ZakkBot.commandHandlers[commandName[3]].slashCommandParameters;
					slashCommands.push(command.slashCommand);
				}
			}
		}
		return slashCommands;
	}

	static async applySlashCommands(zakkBotClient: ZakkBot, guildId: Snowflake, configBatch: FullConfig): Promise<void> {
		const commandsMap: ChatInputApplicationCommandData[] = await this.prepareSlashCommands(guildId, zakkBotClient, configBatch);
		const guildObject: Guild | undefined = zakkBotClient.discordClient.guilds.cache.get(guildId);
		await guildObject?.commands.set(commandsMap).catch((error: string): void => {Logger.log("Permission error on slash commands setup", "error", error);});
	}

	static async applyCommands(zakkBotClient: ZakkBot, guildId: Snowflake | null, type: ("text" | "slash" | "all")): Promise<void> {
		if (guildId){
			const configBatch: FullConfig = {
				modules: await ConfigHelper.getModulesByGuild(zakkBotClient.clientConfig.id, guildId),
				commands: await ConfigHelper.getCommandsByGuild(zakkBotClient.clientConfig.id, guildId)
			};
			if (type === "all") {
				await Promise.all([
					this.applySlashCommands(zakkBotClient, guildId, configBatch),
					this.applyTextCommands(zakkBotClient, guildId, configBatch)
				]);
			} else if (type === "text") {
				await this.applyTextCommands(zakkBotClient, guildId, configBatch);
			} else if (type === "slash") {
				await this.applySlashCommands(zakkBotClient, guildId, configBatch);
			}
			return;
		}

		const serversData: ConfigGuild[] | undefined = await ConfigHelper.getServers(zakkBotClient.clientConfig.id);
		if (!serversData) return;
		for (const guild of serversData) {
			const configBatch: FullConfig = {
				modules: await ConfigHelper.getModulesByGuild(zakkBotClient.clientConfig.id, guild.guildId),
				commands: await ConfigHelper.getCommandsByGuild(zakkBotClient.clientConfig.id, guild.guildId)
			};
			await Promise.all([
				await this.applySlashCommands(zakkBotClient, guild.guildId, configBatch),
				await this.applyTextCommands(zakkBotClient, guild.guildId, configBatch)
			]);
		}
	}
}
import { ModuleBase } from '../../bases/moduleBase';
import { ModuleInterface } from '../../bases/moduleInterface';
import { ModuleLoader } from '../../util/moduleLoader';
import { StatusroleStatusChangeFeature } from './features/statusChange';

export class StatusroleModule extends ModuleBase implements ModuleInterface {
	public moduleName: string = "verify";

	async initFeatures(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadFeature(new StatusroleStatusChangeFeature())
		]);
	}
}
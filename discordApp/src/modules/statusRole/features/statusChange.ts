import { GuildMember, Presence, Role } from 'discord.js';

import { FeatureBase } from '../../../bases/featureBase';
import { FeatureInterface } from '../../../bases/featureInterface';
import { ConfigModule, ConfigStatusroleModule, StatusroleGroup } from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { ZakkBot } from '../../../zakkBot';

export class StatusroleStatusChangeFeature extends FeatureBase implements FeatureInterface{
	name: string = "presenceUpdate/statusRole";
	disabled: boolean = false;
	once: boolean = false;
	async run(zakkBotClient: ZakkBot, oldMember: GuildMember, newMember: Presence): Promise<void> {
		if (!newMember.guild || !newMember.member) return;

		const moduleConfig: ConfigModule | undefined= await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, newMember.guild.id, "statusRole");
		if (!moduleConfig || !moduleConfig.enabled) return;
		moduleConfig.data = moduleConfig.data as ConfigStatusroleModule;

		const memberTarget: GuildMember = await newMember.guild.members.fetch(newMember.member.id);
		if (memberTarget.user.bot) { return; }

		moduleConfig.data.groups.forEach((group: StatusroleGroup): void => {
			if (!newMember.guild || !newMember.member) return;
			const role: Role | undefined = newMember.guild.roles.cache.find((roleToFind: Role): boolean => roleToFind.id === group.role);
			if (!role) return;
			if (newMember.activities[0] &&
				newMember.activities[0].state === group.text
			) {
				if (group.icon === "" ||
					(
						newMember.activities[0].emoji &&
						(
							newMember.activities[0].emoji.name === group.icon ||
							newMember.activities[0].emoji.id === group.icon
						)
					)
				) {
					memberTarget.roles.add(role);
					return;
				} else {
					memberTarget.roles.remove(role);
					return;
				}
			} else {
				memberTarget.roles.remove(role);
				return;
			}
		});
	}
}
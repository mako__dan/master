import {
	Collection, DMChannel, GuildEmoji, GuildMember, Message, MessageReaction, Role, Snowflake, User
} from 'discord.js';

import { FeatureBase } from '../../../bases/featureBase';
import { FeatureInterface } from '../../../bases/featureInterface';
import {
	ConfigModule, ConfigVerifyModule, VerifyEmotes, VerifyGroup
} from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { ZakkBot } from '../../../zakkBot';

export class VerifyReactionDetectorFeature extends FeatureBase implements FeatureInterface {
	name: string = "messageReactionAdd/verify";
	disabled: boolean = false;
	once:boolean = false;
	async run(zakkBotClient: ZakkBot, reaction: MessageReaction, user: User): Promise<void> {
		if (user.bot) return;

		const message: Message = reaction.message as Message;
		if (message.channel instanceof DMChannel || message.channel.partial) return;

		const moduleConfig: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, message.channel.guild.id, "verify");
		if (!moduleConfig || !moduleConfig.enabled || (moduleConfig.data as ConfigVerifyModule).channel !== message.channel.id) return;
		moduleConfig.data = moduleConfig.data as ConfigVerifyModule;
		if (message.channel.id !== moduleConfig.data.channel) return;

		// if admin check if has permissions to edit results
		const currentMember: GuildMember = await message.channel.guild.members.fetch(user.id);
		if (currentMember.roles.cache.hasAny(...moduleConfig.data.authority)) {
			// if forceYes is added
			if(reaction.emoji.name === moduleConfig.data.emotes.forceYes || reaction.emoji.id === moduleConfig.data.emotes.forceYes) {
				this.giveRoles(message, moduleConfig.data);
				this.clearReactions(message);
			}
			// if forceNo is added
			if(reaction.emoji.name === moduleConfig.data.emotes.forceNo || reaction.emoji.id === moduleConfig.data.emotes.forceNo) {
				this.removeRoles(message, moduleConfig.data);
				this.clearReactions(message);
			}
			// if reset is pressed
			if(reaction.emoji.name === moduleConfig.data.emotes.reset || reaction.emoji.id === moduleConfig.data.emotes.reset) {
				this.clearReactions(message);
				this.addReactions(moduleConfig.data, message);
			}
		}

		// remove invalid reactions
		let isValid: boolean = false;
		if (reaction.emoji.name === moduleConfig.data.emotes.yes || reaction.emoji.id === moduleConfig.data.emotes.yes) isValid = true;
		if (reaction.emoji.name === moduleConfig.data.emotes.no || reaction.emoji.id === moduleConfig.data.emotes.no) isValid = true;
		if (reaction.emoji.name === moduleConfig.data.emotes.undecided || reaction.emoji.id === moduleConfig.data.emotes.undecided) isValid = true;
		if (!isValid){
			let messageReaction: MessageReaction | null | undefined;
			if (reaction && reaction.emoji && reaction.emoji.id){
				messageReaction = message.reactions.resolve(reaction.emoji.id);
			} else if (reaction && reaction.emoji && reaction.emoji.name){
				messageReaction = message.reactions.resolve(reaction.emoji.name);
			}
			if (messageReaction) messageReaction.remove();
		}

		// count points and edit roles
		const reactionArray: number[] = await Promise.all(
			message.reactions.cache.map(async (currentReaction: MessageReaction): Promise<number> => this.getMembersFromReaction(moduleConfig.data as ConfigVerifyModule, currentReaction, message))
		);
		const pointsSum: number = reactionArray.reduce((a: number, b: number): number => {
			return a + b;
		}, 0);
		if (pointsSum >= moduleConfig.data.neededPoints){
			this.giveRoles(message, moduleConfig.data);
			this.clearReactions(message);
		}
	}
	async clearReactions(message: Message): Promise<void> {
		message.reactions.removeAll();
	}
	async addReactions(moduleConfig: ConfigVerifyModule, message: Message): Promise<void> {
		const emoteData: VerifyEmotes = moduleConfig.emotes;
		Object.keys(emoteData).forEach((emote: Snowflake | string): void => {
			let reactionEmoji: GuildEmoji | null;
			reactionEmoji = message.guild?.emojis.cache.get(emote) || null;
			if (reactionEmoji !== null)
			message.react(reactionEmoji);
		});
	}
	async giveRoles(message: Message, moduleConfig: ConfigVerifyModule): Promise<void> {
		const mentionedUser: User | undefined = message.mentions.users.first();
		if (!mentionedUser || message.channel instanceof DMChannel || message.channel.partial) return;
		const verifyRole: Role | undefined = message.channel.guild.roles.cache.get(moduleConfig.verifiedRole);
		if (!verifyRole) return;
		message.channel.guild.members.cache.get(mentionedUser.id)?.roles.add(verifyRole);
	}
	async removeRoles(message: Message, moduleConfig: ConfigVerifyModule): Promise<void> {
		const mentionedUser: User |undefined = message.mentions.users.first();
		if (!mentionedUser || message.channel instanceof DMChannel || message.channel.partial) return;
		const verifyRole: Role | undefined = message.channel.guild.roles.cache.get(moduleConfig.verifiedRole);
		if (!verifyRole) return;
		message.channel.guild.members.cache.get(mentionedUser.id)?.roles.remove(verifyRole);
	}
	async getMembersFromReaction(moduleConfig: ConfigVerifyModule, reaction: MessageReaction, message: Message): Promise<number> {
		let optionPoints: string | undefined;
		for (const emote in moduleConfig.emotes){
			if (
				reaction.emoji.name === moduleConfig.emotes[emote] ||
				reaction.emoji.id === moduleConfig.emotes[emote]
			) {
				optionPoints = emote;
			}
		}
		if (optionPoints) {
			const userList: Collection<string, User> = await reaction.users.fetch();

			const pointsArray: number[] = await Promise.all(
				userList.map(async (user: User): Promise<number> => this.getPointsPerMember(message, moduleConfig, user, optionPoints as string))
			);
			const pointsSum: number = pointsArray.reduce((a: number, b: number): number => {
				return a + b;
			});
			return pointsSum;
		}
		return 0;
	}
	async getPointsPerMember(message: Message, moduleConfig: ConfigVerifyModule, user: User, optionPoints: string): Promise<number> {
		let pointDifference: number = moduleConfig.points[optionPoints];
		if (message.channel instanceof DMChannel || message.channel.partial) return 0;
		const member: GuildMember = await message.channel.guild.members.fetch(user.id);
		moduleConfig.groups.every((group: VerifyGroup): boolean => {
			if (member.roles.cache.get(group.role)){
				pointDifference = group.points[optionPoints];
				return false;
			}
			return true;
		});
		return pointDifference;
	}
}
import { DMChannel, GuildEmoji, Message, Snowflake, User } from 'discord.js';

import { FeatureBase } from '../../../bases/featureBase';
import { FeatureInterface } from '../../../bases/featureInterface';
import { ConfigModule, ConfigVerifyModule, VerifyEmotes } from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { ZakkBot } from '../../../zakkBot';

export class VerifyMessageCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "messageCreate/verify";
	disabled: boolean = false;
	once: boolean = false;
	async run(zakkBotClient: ZakkBot, message: Message): Promise<void> {
		if (message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return;

		const moduleConfig: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, message.channel.guild.id, "verify");
		if (!moduleConfig || !moduleConfig.enabled || (moduleConfig.data as ConfigVerifyModule).channel !== message.channel.id) return;
		moduleConfig.data = moduleConfig.data as ConfigVerifyModule;
		const user: User | undefined = message.mentions.users.first();
		if (!user) message.delete();

		const emoteData: VerifyEmotes = moduleConfig.data.emotes;

		Object.keys(emoteData).forEach((emote: Snowflake): void => {
			let reactionEmoji: GuildEmoji | undefined | string;
			if (isNaN(Number(emoteData[emote]))){
				reactionEmoji = emoteData[emote];
			} else {
				reactionEmoji = message.guild?.emojis.cache.get(emoteData[emote]);
			}
			if (reactionEmoji) message.react(reactionEmoji);
		});
	}
}
import { ModuleBase } from '../../bases/moduleBase';
import { ModuleInterface } from '../../bases/moduleInterface';
import { ModuleLoader } from '../../util/moduleLoader';
import { VerifyMessageCreateFeature } from './features/messageCreate';
import { VerifyReactionDetectorFeature } from './features/reactionDetektor';

export class VerifyModule extends ModuleBase implements ModuleInterface {
	public moduleName: string = "verify";

	async initFeatures(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadFeature(new VerifyMessageCreateFeature()),
			ModuleLoader.loadFeature(new VerifyReactionDetectorFeature())
		]);
	}
}
import { Guild, GuildMember, TextChannel } from 'discord.js';

import { FeatureBase } from '../../../bases/featureBase';
import { FeatureInterface } from '../../../bases/featureInterface';
import { ConfigModule, ConfigWelcomerModule } from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';

export class WelcomerMemberLeaveFeature extends FeatureBase implements FeatureInterface{
	name: string = "guildMemberRemove/welcomer";
	disabled: boolean = false;
	once: boolean = false;
	async run(zakkBotClient: ZakkBot, memberData: GuildMember): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, memberData.guild.id, "welcomer");
		if (!moduleConfig) return;
		moduleConfig.data = moduleConfig.data as ConfigWelcomerModule;
		moduleConfig.data.leaveMessage = MessageHelper.ReplaceWildcardsInBuilder(moduleConfig.data.leaveMessage , {guild: memberData.guild, member: memberData, botUser: (await (await zakkBotClient.discordClient.guilds.fetch( memberData.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076"))});
		MessageHelper.Send({ sendToChannel: moduleConfig.data.sendToChannel,sendToOrigin: false },
			moduleConfig.data.leaveMessage,
			zakkBotClient.discordClient.guilds.cache.find((guildData: Guild): boolean => guildData.id === memberData.guild.id)?.channels.cache.first() as TextChannel
			);
	}
}
import { ModuleBase } from '../../bases/moduleBase';
import { ModuleInterface } from '../../bases/moduleInterface';
import { ModuleLoader } from '../../util/moduleLoader';
import { WelcomerMemberAddFeature } from './features/memberAdd';
import { WelcomerMemberLeaveFeature } from './features/memberLeave';

export class WelcomerModule extends ModuleBase implements ModuleInterface {
	public moduleName: string = "welcomer";

	async initFeatures(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadFeature(new WelcomerMemberAddFeature()),
			ModuleLoader.loadFeature(new WelcomerMemberLeaveFeature()),
		]);
	}
}
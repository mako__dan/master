import {
	ApplicationCommandOption, CommandInteraction, DMChannel, GuildMember, Message, MessageOptions,
	Role, Snowflake, User
} from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigAutoroleModule, ConfigBatch } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { Logger } from '../../../util/logger';

export class ApplyRolesCommand extends CommandBase implements CommandInterface {
	name: string = 'applyroles';
	description: string = 'Adds missing roles to member';
	help: string = '!automod applyroles';
	slashCommandParameters: ApplicationCommandOption[] = [
		{
			type: "USER",
			name: "member",
			description: "člen k upravení rolí",
			required: true
		}
	];

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch ): Promise<ExecuteResponseType> {
		if (message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return {success: false};

		const user: User | undefined = message.mentions.users.first();
		let userToFetch: Snowflake | undefined = user?.id;
		if (!user && !isNaN(Number(args[0]))){
			userToFetch = args[0];
		}
		if (!userToFetch)
			return {success:false, message:"Missing member reference."};

		const guildMemberResponse: GuildMember = await message.channel.guild.members.fetch(userToFetch);
		await this.applyRoles(guildMemberResponse, configBatch);
		const successEmbed: MessageOptions = {
			embeds: [{
				description: "Roles added succesfully"
			}]
		};

		MessageHelper.Send(configBatch.command, successEmbed, message.channel);
		return {success:true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch): Promise<ExecuteResponseType> {
		const memberOption: GuildMember = interaction.options.getMember("member", true) as GuildMember;
		await this.applyRoles(memberOption, configBatch);
		const successEmbed: MessageOptions = {
			embeds: [{
				description: "Role úspěšně doplněny"
			}]
		};
		interaction.editReply(successEmbed);
		return {success:true};
	}

	async applyRoles(user: GuildMember, configBatch: ConfigBatch ): Promise<void>{
		configBatch.module.data = configBatch.module.data as ConfigAutoroleModule;
		configBatch.module.data.roles.forEach((roleId: Snowflake): void => {
			const role: Role | undefined = user.guild.roles.cache.find((roleToFind: Role): boolean => roleToFind.id === roleId);
			if (role) {
				user.roles.add(role).catch(Logger.error);
			}
		});
	}
}
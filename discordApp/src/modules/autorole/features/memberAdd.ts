import { GuildMember, Role, Snowflake } from 'discord.js';

import { FeatureBase } from '../../../bases/featureBase';
import { FeatureInterface } from '../../../bases/featureInterface';
import { ConfigAutoroleModule, ConfigModule } from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { ZakkBot } from '../../../zakkBot';

export class AutoroleMemberAddFeature extends FeatureBase implements FeatureInterface{
	name: string = "guildMemberAdd/autorole";
	disabled: boolean = false;
	once: boolean = false;
	async run(zakkBotClient: ZakkBot, memberData: GuildMember): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, memberData.guild.id, "autorole");
		if (!moduleConfig || !moduleConfig.enabled || !(moduleConfig.data as ConfigAutoroleModule)) return;
		moduleConfig.data = moduleConfig.data as ConfigAutoroleModule;

		moduleConfig.data.roles.forEach((roleId: Snowflake): void => {
			const role: Role | undefined = memberData.guild.roles.cache.find(((roleToFind: Role): boolean => roleToFind.id === roleId));
			if (!role) return;
			memberData.roles.add(role);
		});
	}
}
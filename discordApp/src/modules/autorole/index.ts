

import { ModuleBase } from '../../bases/moduleBase';
import { ModuleInterface } from '../../bases/moduleInterface';
import { ModuleLoader } from '../../util/moduleLoader';
import { ApplyRolesCommand } from './commands/applyroles';
import { AutoroleMemberAddFeature } from './features/memberAdd';

export class AutoroleModule extends ModuleBase implements ModuleInterface {
	public moduleName: string = "autorole";


	async initCommands(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadCommand(this.moduleName, new ApplyRolesCommand())
		]);
	}
	async initFeatures(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadFeature(new AutoroleMemberAddFeature())
		]);
	}
}

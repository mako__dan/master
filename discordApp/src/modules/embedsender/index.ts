import { ModuleBase } from '../../bases/moduleBase';
import { ModuleInterface } from '../../bases/moduleInterface';
import { ModuleLoader } from '../../util/moduleLoader';
import { SendEmbedCommand } from './commands/sendembed';

export class EmbedSenderModule extends ModuleBase implements ModuleInterface {
	public moduleName: string = "embedsender";

	async initCommands(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadCommand(this.moduleName, new SendEmbedCommand())
		]);
	}
}
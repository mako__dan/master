import {
	ApplicationCommandOption, CommandInteraction, DMChannel, Message, MessageOptions
} from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch, ConfigEmbedsenderModule, EmbedsenderEmbed } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';

export class SendEmbedCommand extends CommandBase implements CommandInterface{
	name: string = 'sendembed';
	description: string = 'sends embed message form database or custom';
	help: string = '!automod sendembed ["+"identifier | json]';
	slashCommandParameters: ApplicationCommandOption[] = [
		{
			type: "SUB_COMMAND",
			name: "preset",
			description: "preset message",
			options: [
				{
					type: "STRING",
					name: "name",
					description: "Message name",
					required: true,
				}
			]
		},
		{
			type: "SUB_COMMAND",
			name: "custom",
			description: "custom message",
			options: [
				{
					type: "STRING",
					name: "content",
					description: "Custom content",
					required: true,
				}
			]
		},
		{
			type: "SUB_COMMAND",
			name: "channel",
			description: "channel default message",
		}
	];

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if (message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return {success: false};

		configBatch.module.data = configBatch.module.data as ConfigEmbedsenderModule;

		if(args.length === 0) {
			const channelDefaultData: EmbedsenderEmbed | undefined = configBatch.module.data.embeds.find((embed: EmbedsenderEmbed) => embed.name === message.channel.id);
			if (channelDefaultData) {
				channelDefaultData.messages.forEach(async (embedMessage: MessageOptions): Promise<{ success: boolean; } | undefined> => {
					if (!message.guild) return {success:false};
					embedMessage = MessageHelper.ReplaceWildcardsInBuilder(embedMessage , {guild: message.guild, botUser: (await (await zakkBotClient.discordClient.guilds.fetch( message.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076"))});

					MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, embedMessage, message.channel);
				});
				return {success: true};
			} else {
				return {success: false, message: "There is no default message in this channel"};
			}
		}

		const presetData: EmbedsenderEmbed | undefined = configBatch.module.data.embeds.find((embed: EmbedsenderEmbed) => embed.name === args[0]);
		if (presetData) {
			presetData.messages.forEach(async (embedMessage: MessageOptions) => {
				if (!message.guild) return {success:false};
				embedMessage = MessageHelper.ReplaceWildcardsInBuilder(embedMessage , {guild: message.guild, botUser: (await (await zakkBotClient.discordClient.guilds.fetch( message.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076"))});
				MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, embedMessage, message.channel);
			});
			return {success: true};
		}

		try {
			let jsonData: MessageOptions | undefined = JSON.parse(text);
			if ((jsonData as MessageOptions)) {
				if (!message.guild) return {success:false};
				jsonData = MessageHelper.ReplaceWildcardsInBuilder(jsonData as MessageOptions, {guild: message.guild, botUser: (await (await zakkBotClient.discordClient.guilds.fetch( message.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076"))});
				MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, (jsonData as MessageOptions), message.channel);
			} else {
				if (!message.guild) return {success:false};
				jsonData = MessageHelper.ReplaceWildcardsInBuilder(jsonData as MessageOptions , {guild: message.guild, botUser: (await (await zakkBotClient.discordClient.guilds.fetch( message.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076"))});
				MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, jsonData, message.channel);
			}
			return {success:true};
		} catch (error){
			return {success: false, message:"Zpracování JSON objektu selhalo:" + error};
		}
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if (!interaction.guild || !interaction.channel) return {success:false};
		configBatch.module.data = configBatch.module.data as ConfigEmbedsenderModule;

		switch(interaction.options.getSubcommand()){
			case "preset":
				const presetData: EmbedsenderEmbed | undefined = configBatch.module.data.embeds.find((embed: EmbedsenderEmbed) => embed.name === interaction.options.getString("name", true));
				if (presetData) {
					presetData.messages.forEach(async (embedMessage: MessageOptions) => {
						if (!interaction.guild) return {success:false};
						embedMessage = MessageHelper.ReplaceWildcardsInBuilder(embedMessage , {guild: interaction.guild, botUser: (await (await zakkBotClient.discordClient.guilds.fetch( interaction.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076"))});
						if (!interaction.channel) return;
						MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, embedMessage, interaction.channel);
					});
				}
				break;
			case "custom":
				const customData: string = interaction.options.getString("content", true);
				let jsonData: MessageOptions = {};
				try {
					jsonData = JSON.parse(customData);
				} catch (error){
					return {success:false, message: "JSON parsing failed"};
				}
				jsonData = MessageHelper.ReplaceWildcardsInBuilder((jsonData as MessageOptions) , {guild: interaction.guild, botUser: (await (await zakkBotClient.discordClient.guilds.fetch( interaction.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076"))});
				MessageHelper.Send({sendToChannel: "", sendToOrigin: true} , jsonData, interaction.channel);
				break;
			case "channel":
				const channelDefaultData: EmbedsenderEmbed | undefined = configBatch.module.data.embeds.find((embed: EmbedsenderEmbed): boolean => embed.name === interaction.channel?.id);
				if (channelDefaultData) {
					channelDefaultData.messages.forEach(async (embedMessage: MessageOptions): Promise<{ success: boolean; } | undefined> => {
						if (!interaction.guild) return {success:false};
						embedMessage = MessageHelper.ReplaceWildcardsInBuilder(embedMessage, {guild: interaction.guild, botUser: (await (await zakkBotClient.discordClient.guilds.fetch( interaction.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076"))});
						if (!interaction.channel) return;
						MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, embedMessage, interaction.channel);
					});
				} else {
					return {success:false, message: "There is no default message in this channel"};
				}
				break;
		}
		return {success:true};
	}
}

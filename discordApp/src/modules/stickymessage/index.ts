import { ModuleBase } from '../../bases/moduleBase';
import { ModuleInterface } from '../../bases/moduleInterface';
import { ModuleLoader } from '../../util/moduleLoader';
import { StickyMessageCommand } from './commands/stickymessage';
import { StickyMessageCreateFeature } from './features/messageCreate';

export class StickyMessageModule extends ModuleBase implements ModuleInterface {
	public moduleName: string = "stickymessage";

	async initCommands(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadCommand(this.moduleName, new StickyMessageCommand())
		]);
	}
	async initFeatures(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadFeature(new StickyMessageCreateFeature())
		]);
	}
}
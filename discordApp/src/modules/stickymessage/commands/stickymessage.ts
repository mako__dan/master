import {
	CommandInteraction, DMChannel, Guild, Message, MessageOptions, Snowflake, TextBasedChannels
} from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import {
	ConfigBatch, ConfigStickymessageModule, StickyMessage, StickyMessageDatabase
} from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { Logger } from '../../../util/logger';
import { ZakkBot } from '../../../zakkBot';

export class StickyMessageCommand extends CommandBase implements CommandInterface{
	name: string = 'stickymessage';
	description: string = 'Pošle sticky zprávu';
	help: string = '!automod applyroles';
	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if (message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return {success: false};

		configBatch.module.data = configBatch.module.data as ConfigStickymessageModule;
		const findStickyMesssage: StickyMessage | undefined = configBatch.module.data.messages.find((stickyMessage: StickyMessage): boolean => stickyMessage.channel === message.channel.id);

		if (!findStickyMesssage) return {success:false, message:"No message set."};

		const Data: StickyMessageDatabase | undefined = await ConfigHelper.getStickymessage(message.channel.id);
		if (Data) {
			this.removeMessage(message.channel, Data.stickyMessageId);
		}
		const newMessage: Message | void = await this.createMessage(message.channel, findStickyMesssage, message.guild, zakkBotClient);
		if (!newMessage) return {success:false, message:"Sending message failed"};
		ConfigHelper.setStickymessage(message.channel.id, {
			channelId: message.channel.id,
			stickyMessageId: newMessage.id,
			messagesCount: 0,
			maxMessages: 5,
		});
		return {success:true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType>  {
		if (!interaction.guildId || !interaction.channel) return {success:false};

		configBatch.module.data = configBatch.module.data as ConfigStickymessageModule;
		const findStickyMesssage: StickyMessage | undefined = configBatch.module.data.messages.find((message: StickyMessage): boolean => message.channel === interaction.channelId);

		if (!findStickyMesssage) return {success:false, message:"No message set."};

		const Data: StickyMessageDatabase | undefined = await ConfigHelper.getStickymessage(interaction.channel.id);
		if (Data) {
			this.removeMessage(interaction.channel, Data.stickyMessageId);
		}

		const newMessage: Message | void = await this.createMessage(interaction.channel, findStickyMesssage, interaction.guild, zakkBotClient);
		if (!newMessage) return {success:false, message: "Sending message failed"};

		ConfigHelper.setStickymessage(interaction.channel.id, {
			channelId: interaction.channel.id,
			stickyMessageId: newMessage.id,
			messagesCount: 0,
			maxMessages: 5,
		});
		return {success:true};
	}

	async createMessage(channel: TextBasedChannels, stickyMesssage: StickyMessage, guild: Guild | null, zakkBotClient: ZakkBot): Promise<void | Message> {
		let messageData: MessageOptions =  stickyMesssage.message;
		if (!guild) return;
		messageData = MessageHelper.ReplaceWildcardsInBuilder(messageData , {guild, botUser: (await (await zakkBotClient.discordClient.guilds.fetch( guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076"))});
		return await MessageHelper.Send({sendToChannel: "", sendToOrigin: true} , messageData, channel);
	}

	async removeMessage(channel: TextBasedChannels, messageId: Snowflake): Promise<void> {
		const messageToRemove: Message | void = await channel.messages.fetch(messageId).catch(Logger.error);
		if (messageToRemove) {
			messageToRemove.delete();
		}
	}

}
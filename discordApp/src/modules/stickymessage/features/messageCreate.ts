import { DMChannel, Message, MessageOptions, Snowflake } from 'discord.js';

import { FeatureBase } from '../../../bases/featureBase';
import { FeatureInterface } from '../../../bases/featureInterface';
import {
	ConfigModule, ConfigStickymessageModule, StickyMessage, StickyMessageDatabase
} from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';

export class StickyMessageCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "messageCreate/stickymessage";
	disabled: boolean = false;
	once: boolean = false;
	async run(zakkBotClient: ZakkBot, message: Message): Promise<void> {
		if (message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return;

		const moduleConfig: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, message.channel.guild.id, "stickymessage");
		if (!moduleConfig || !moduleConfig.enabled ) return;
		moduleConfig.data = moduleConfig.data as ConfigStickymessageModule;

		const findStickyMesssage: StickyMessage | undefined = moduleConfig.data.messages.find((stickyMessage: StickyMessage): boolean => stickyMessage.channel === message.channel.id);

		if (!findStickyMesssage) return;

		const Data: StickyMessageDatabase | undefined = await ConfigHelper.getStickymessage(message.channel.id);
		if (Data) {
			Data.messagesCount = Data.messagesCount+1;
			if (Data.messagesCount > Data.maxMessages){
				Data.messagesCount = 0;
				this.removeMessage(message, Data.stickyMessageId);
				const newMessage: Message | void = await this.createMessage(zakkBotClient, message, findStickyMesssage);
				if (!newMessage) return;
				Data.stickyMessageId = newMessage.id;
			}
			ConfigHelper.setStickymessage(message.channel.id, Data);
		} else {
			const newMessage: Message | void = await this.createMessage(zakkBotClient, message, findStickyMesssage);
			if (!newMessage) return;
			ConfigHelper.setStickymessage(message.channel.id, {
				channelId: message.channel.id,
				stickyMessageId: newMessage.id,
				messagesCount: 0,
				maxMessages: 5,
			});
		}
	}
	async createMessage(zakkBotClient: ZakkBot, messageOrigin: Message, findStickyMesssage: StickyMessage): Promise<Message | void> {
		let messageData: MessageOptions = findStickyMesssage.message;
		if (!messageOrigin.guild) return;
		messageData = MessageHelper.ReplaceWildcardsInBuilder(messageData , {guild: messageOrigin.guild, botUser: (await (await zakkBotClient.discordClient.guilds.fetch( messageOrigin.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076"))});

		return await MessageHelper.Send({sendToChannel: "", sendToOrigin: true} , messageData, messageOrigin.channel);
	}
	async removeMessage(messageOrigin: Message, messageId: Snowflake): Promise<void> {
		messageOrigin.channel.messages.fetch(messageId).then((message: Message): Promise<Message> => message.delete());
	}
}
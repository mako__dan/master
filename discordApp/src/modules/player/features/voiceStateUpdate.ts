import {
	CommandInteraction, GuildChannel, GuildMember, Message, MessageEmbed, Snowflake, StageChannel,
	ThreadChannel, VoiceChannel, VoiceState
} from 'discord.js';

import { entersState, joinVoiceChannel, VoiceConnectionStatus } from '@discordjs/voice';

import { FeatureBase } from '../../../bases/featureBase';
import { FeatureInterface } from '../../../bases/featureInterface';
import { ConfigModule, ConfigPlayerModule } from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { Logger } from '../../../util/logger';
import { MusicSubscription } from '../../../util/music/subscription';
import { Track } from '../../../util/music/track';
import { ZakkBot } from '../../../zakkBot';

export class PlayerVoiceStateUpdateFeature extends FeatureBase implements FeatureInterface {
	name: string = "voiceStateUpdate/player";
	disabled: boolean = false;
	once: boolean = false;
	async run(zakkBotClient: ZakkBot, oldState: VoiceState, newState: VoiceState): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, oldState.guild.id, "player");
		if (!moduleConfig || !moduleConfig.enabled || !(moduleConfig.data as ConfigPlayerModule).autoplay.startOnMemberJoin) return;
		moduleConfig.data = moduleConfig.data as ConfigPlayerModule;
		if (
			(newState.channelId === moduleConfig.data.autoplay.voiceRoom || oldState.channelId === moduleConfig.data.autoplay.voiceRoom) &&
			zakkBotClient.discordClient.user
		) {
			if (oldState.channel?.members.size === undefined && newState.channel?.members.size === 1) {
				const url: string = moduleConfig.data.autoplay.playlist;

				let subscription: MusicSubscription = zakkBotClient.playerSubscriptions[newState.guild.id];
				if (!subscription) {
					if (newState.channel && newState.channel.id) {
						const channel: VoiceChannel | StageChannel = newState.channel;
						subscription = new MusicSubscription(
							joinVoiceChannel({
								channelId: channel.id,
								guildId: channel.guild.id,
								adapterCreator: channel.guild.voiceAdapterCreator,
								selfDeaf: false
							}),
						);
						subscription.voiceConnection.on('error', (err: Error): void => Logger.log("", 'red', err));
					}
				}
				zakkBotClient.playerSubscriptions[newState.guild.id] = subscription;
				// Make sure the connection is ready before processing the user's request
				try {
					await entersState(subscription.voiceConnection, VoiceConnectionStatus.Ready, 20e3);
				} catch (error) {
					Logger.log("", 'error', error);
					return;
				}
				try {
					// Attempt to create a Track from the user's video URL
					const tracks: Track[] | void = await Track.from(url, await newState.guild.members.fetch(zakkBotClient.discordClient.user.id) as GuildMember, {
						onStart(track: Track, nextTrack: Track): void {
							if (subscription.skipFirst) {
								subscription.audioPlayer.stop();
								subscription.skipFirst = false;
							} else {
								if (newState.channel) {
									track.fulfillTrack().then(() => {
										const newMessage: MessageEmbed = new MessageEmbed()
											.setAuthor(`Now playing`, undefined, track.url)
											.setThumbnail(track.thumbnail)
											.setDescription(`[${track.title}](${track.url})`)
											.addField("Length", track.time)
											.addField("Requested by", `<@${track.enquedBy.id}>`);
										if (nextTrack)
											newMessage.addField("Next in queue", `[${nextTrack.title}](${nextTrack.url})`);
										const room: GuildChannel | ThreadChannel | undefined = newState.member?.guild.channels.cache.find((channelSelected: GuildChannel | ThreadChannel): boolean => channelSelected.isText());
										if (!room || !room.isText()) return;
										MessageHelper.Send({ sendToChannel: (moduleConfig.data as ConfigPlayerModule).autoplay.djRoom, sendToOrigin: false }, { embeds: [newMessage] }, room);
									});
								}
							}
						},
						onError(error: Error): void {
							// interaction.followUp({ content: `Error: ${error.message}`, ephemeral: true }).catch(console.warn);
						}
					});
					if (tracks) {
						tracks.forEach((track: Track): void => {
							subscription.enqueue(track);
						});
					}
				} catch (error) {
					Logger.log("", 'red', error);
				}

				// better shuffle method
				subscription.autoplayQueue = true;
				if (moduleConfig.data.autoplay.autoShuffle) {
					subscription.queue.push(subscription.queue[0]);
					subscription.queue = this.shuffle(subscription.queue);
					subscription.skipFirst = true;
				}
			}

			if (newState.channel?.members.size === undefined && oldState.channel?.members.size === 1) {
				if (zakkBotClient.playerSubscriptions[newState.guild.id]) {
					zakkBotClient.playerSubscriptions[newState.guild.id].stop();
					zakkBotClient.playerSubscriptions[newState.guild.id].voiceConnection.destroy();
					delete zakkBotClient.playerSubscriptions[newState.guild.id];
				}
			}
		}

	}

	public appendMultiplesongCallback(zakkBotClient: ZakkBot, guildId: Snowflake | null, callbackItem: Track | string[], replyType?: CommandInteraction | Message): void {
		if (!guildId) return;
		if (callbackItem instanceof Track) {
			zakkBotClient.playerSubscriptions[guildId].enqueue(callbackItem);
		} else {
			if (replyType instanceof CommandInteraction) {
				replyType.followUp(`added ${callbackItem[0]} songs from playlist ${callbackItem[1]}`);
			} else if (replyType instanceof Message) {
				replyType.reply(`added ${callbackItem[0]} songs from playlist ${callbackItem[1]}`);
			}
		}
	}

	shuffle(array: Track[]): Track[] {
		let currentIndex: number = array.length;
		let randomIndex: number = 0;

		while (currentIndex !== 1) {
			randomIndex = Math.floor(Math.random() * currentIndex + 1);
			currentIndex--;
			[array[currentIndex], array[randomIndex]] = [
				array[randomIndex], array[currentIndex]];
		}
		return array;
	}
}
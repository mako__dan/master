import { CommandInteraction, Message, MessageOptions } from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { MusicSubscription } from '../../../util/music/subscription';
import { Track } from '../../../util/music/track';
import { ZakkBot } from '../../../zakkBot';

export class ShuffleCommand extends CommandBase implements CommandInterface{
	name: string = 'shuffle';
	description: string = 'shuffle';
	help: string = 'shuffle';

	public static shuffleMessage: MessageOptions = {
		embeds: [{title:'Shuffled!'}]
	};

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!message.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[message.guildId];

		if (subscription) {
			subscription.queue = this.shuffle(subscription.queue);
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, ShuffleCommand.shuffleMessage, message.channel);
		}
		return {success: true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!interaction.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[interaction.guildId];

		if (subscription) {
			subscription.queue = this.shuffle(subscription.queue);
			await interaction.editReply(ShuffleCommand.shuffleMessage);
		}
		return {success: true};
	}

	shuffle(array: Track[]): Track[] {
		let currentIndex: number = array.length;
		let randomIndex: number = 0;

		while (currentIndex !== 1) {
			randomIndex = Math.floor(Math.random() * currentIndex+1);
			currentIndex--;
			[array[currentIndex], array[randomIndex]] = [
				array[randomIndex], array[currentIndex]];
			}
		return array;
	}
}
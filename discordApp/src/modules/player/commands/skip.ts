import { CommandInteraction, Message, MessageOptions } from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { MusicSubscription } from '../../../util/music/subscription';
import { ZakkBot } from '../../../zakkBot';

export class SkipCommand extends CommandBase implements CommandInterface{
	name: string = 'skip';
	description: string = 'skip';
	help: string = 'skip';

	public static notPlayingMessage: MessageOptions = {
		embeds: [{title:'Not playing in this server!'}]
	};

	public static skippedMessage: MessageOptions = {
		embeds: [{title:'Skipped!'}]
	};

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!message.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[message.guildId];

		if (subscription) {
			subscription.audioPlayer.stop();
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, SkipCommand.skippedMessage, message.channel);
		} else {
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, SkipCommand.notPlayingMessage, message.channel);
		}
		return {success: true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if(!interaction.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[interaction.guildId];

		if (subscription) {
			subscription.audioPlayer.stop();
			await interaction.editReply(SkipCommand.skippedMessage);
		} else {
			await interaction.editReply(SkipCommand.notPlayingMessage);
		}
		return {success: true};
	}
}
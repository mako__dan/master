import { CommandInteraction, Message, MessageOptions } from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { MusicSubscription } from '../../../util/music/subscription';
import { ZakkBot } from '../../../zakkBot';

export class ClearCommand extends CommandBase implements CommandInterface{
	name: string = 'clear';
	description: string = 'clear';
	help: string = 'clear';

	public static queueCrearedMessage: MessageOptions = {
		embeds: [{title:'Queue cleared'}]
	};

	public static notPlayingMessage: MessageOptions = {
		embeds: [{title:'Not playing in this server!'}]
	};

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!message.guild) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[message.guild.id];

		if (subscription) {
			subscription.queue = [subscription.queue[0]];
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, ClearCommand.queueCrearedMessage, message.channel);
		} else {
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, ClearCommand.notPlayingMessage, message.channel);
		}
		return {success: true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if(!interaction.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[interaction.guildId];

		if (subscription) {
			subscription.queue = [subscription.queue[0]];
			await interaction.editReply(ClearCommand.queueCrearedMessage);
		} else {
			await interaction.editReply(ClearCommand.notPlayingMessage);
		}
		return {success: true};
	}
}
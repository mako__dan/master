import { CommandInteraction, Message, MessageOptions } from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { MusicSubscription } from '../../../util/music/subscription';
import { ZakkBot } from '../../../zakkBot';

export class LeaveCommand extends CommandBase implements CommandInterface{
	name: string = 'leave';
	description: string = 'leave';
	help: string = 'leave';

	public static leftMessage: MessageOptions = {
		embeds: [{title:'Left channel!'}]
	};

	public static notPlayingMessage: MessageOptions = {
		embeds: [{title:'Not playing in this server!'}]
	};

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!message.guild) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[message.guild.id];

		if (subscription) {
			subscription.voiceConnection.destroy();
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, LeaveCommand.leftMessage, message.channel);
		} else {
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, LeaveCommand.notPlayingMessage, message.channel);
		}
		return {success: true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if(!interaction.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[interaction.guildId];

		if (subscription) {
			subscription.voiceConnection.destroy();
			await interaction.editReply(LeaveCommand.leftMessage);
		} else {
			await interaction.editReply(LeaveCommand.notPlayingMessage);
		}
		return {success: true};
	}
}
import {
	ApplicationCommandOption, ColorResolvable, CommandInteraction, Message, MessageEmbed,
	MessageOptions
} from 'discord.js';

import { AudioPlayerStatus } from '@discordjs/voice';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch, ConfigGuild } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { AppConfig } from '../../../util/config';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { LyricsGetter } from '../../../util/music/lyricsGetter';
import { MusicSubscription } from '../../../util/music/subscription';
import { ZakkBot } from '../../../zakkBot';

export class LyricsCommand extends CommandBase implements CommandInterface{
	name: string = 'lyrics';
	description: string = 'lyrics';
	help: string = 'lyrics';
	slashCommandParameters: ApplicationCommandOption[] = [
		{
			type: "STRING",
			name: "song",
			description: "song to play",
			required: false
		}
	];

	public static notPlayingMessage: MessageOptions = {
		embeds: [{title:'Not playing in this server!'}]
	};

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!message.guild) return {success: false};

		const serverConfig: ConfigGuild | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, message.guild.id, "moderation") as ConfigGuild | undefined;
		// Print out the current queue, including up to the next 5 tracks to be played.
		if(!serverConfig) return {success: false};

		if (args.length !== 0) {
			const options: { apiKey: string; title: string; } = {
				apiKey: AppConfig.lyricsToken,
				title: args.join(" ")
			};
			LyricsGetter.getLyrics(options)
			.then((lyrics: string | null): void => {
				if (lyrics) {
					const lyricsArray: string[] = lyrics.split("\n\n");
					const embed: MessageEmbed = new MessageEmbed()
					.setColor(serverConfig.themeColor as ColorResolvable);

					for (const row of lyricsArray){
						row === "" && row.length > 1000 ? embed.addField(`\u200b`, `\u200b`) : embed.addField(`\u200b`, `${row}`);
					}

					MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, { embeds: [embed] }, message.channel);
				}
			});
			return {success: true};
		}

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[message.guild.id];

		if (subscription) {
			const options: { apiKey: string; title: string; } = {
				apiKey: AppConfig.lyricsToken,
				title: subscription.queue[0].title.replace(/\([^()]*\)/g, '').trim()
			};
			LyricsGetter.getLyrics(options)
			.then((lyrics: string | null): void => {
				if (lyrics) {
					const lyricsArray: string[] = lyrics.split("\n\n");
					const embed: MessageEmbed = new MessageEmbed()
					.setColor(serverConfig.themeColor as ColorResolvable);

					if (subscription.audioPlayer.state.status === AudioPlayerStatus.Idle) {
						embed.setDescription(`Nothing is currently playing!`);
					} else {
						subscription.queue[0].fulfillTrack().then(() => {
							embed.setDescription(`Now playing - [${subscription.queue[0].title}](${subscription.queue[0].url}) | ${subscription.queue[0].time} Requested by <@${subscription.queue[0].enquedBy.id}>`);
							for (const row of lyricsArray){
								row === "" && row.length > 1000 ? embed.addField(`\u200b`, `\u200b`) : embed.addField(`\u200b`, `${row}`);
							}
						});
					}

					MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, { embeds: [embed] }, message.channel);
				}
			});
		} else {
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, LyricsCommand.notPlayingMessage, message.channel);
		}

		return {success: true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if(!interaction.guild) return {success: false};

		const serverConfig: ConfigGuild | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, interaction.guild.id, "moderation") as ConfigGuild | undefined;
		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[interaction.guild.id];
		// Print out the current queue, including up to the next 5 tracks to be played.
		if(!serverConfig) return {success: false};

		const songName: string = interaction.options.get('song')?.value as string;
		if (songName) {
			const options: { apiKey: string; title: string; } = {
				apiKey: AppConfig.lyricsToken,
				title: songName
			};
			LyricsGetter.getLyrics(options)
			.then((lyrics: string | null): void => {
				if (lyrics) {
					const lyricsArray: string[] = lyrics.split("\n\n");
					const embed: MessageEmbed = new MessageEmbed()
					.setColor(serverConfig.themeColor as ColorResolvable);

					for (const row of lyricsArray){
						row === "" && row.length > 1000 ? embed.addField(`\u200b`, `\u200b`) : embed.addField(`\u200b`, `${row}`);
					}

					interaction.editReply({embeds: [embed]});
				}
			});
		}

		if (subscription) {
			const options: { apiKey: string; title: string; } = {
				apiKey: 'CmWhAmduy3FGaEjoNcU0qiUiSZi9ZHf7seqnNRD2kWNJvX-E29W-f8Lu0iPsSg9w',
				title: subscription.queue[0].title.replace(/\([^()]*\)/g, '').trim()
			};
			LyricsGetter.getLyrics(options)
			.then((lyrics: string | null): void => {
				if (lyrics) {
					const lyricsArray: string[] = lyrics.split("\n\n");
					const embed: MessageEmbed = new MessageEmbed()
					.setColor(serverConfig.themeColor as ColorResolvable);

					if (subscription.audioPlayer.state.status === AudioPlayerStatus.Idle) {
						embed.setDescription(`Nothing is currently playing!`);
					} else {
						subscription.queue[0].fulfillTrack().then(() => {
							embed.setDescription(`Now playing - [${subscription.queue[0].title}](${subscription.queue[0].url}) | ${subscription.queue[0].time} Requested by <@${subscription.queue[0].enquedBy.id}>`);
							for (const row of lyricsArray){
								row === "" && row.length > 1000 ? embed.addField(`\u200b`, `\u200b`) : embed.addField(`\u200b`, `${row}`);
							}
						});
					}

					interaction.editReply({embeds: [embed]});
				}
			});
		} else {
			await interaction.editReply(LyricsCommand.notPlayingMessage);
		}

		return {success: true};
	}
}

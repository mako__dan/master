import {
	ApplicationCommandOption, CommandInteraction, GuildMember, Message, MessageEmbed,
	MessageOptions, Snowflake, StageChannel, VoiceChannel
} from 'discord.js';

import { entersState, joinVoiceChannel, VoiceConnectionStatus } from '@discordjs/voice';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch, ConfigPlayerModule } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { Logger } from '../../../util/logger';
import { MusicSubscription } from '../../../util/music/subscription';
import { Track } from '../../../util/music/track';
import { ZakkBot } from '../../../zakkBot';

export class PlayNowCommand extends CommandBase implements CommandInterface{
	name: string = 'playnow';
	description: string = 'playnow';
	help: string = 'playnow';
	slashCommandParameters: ApplicationCommandOption[] = [
		{
			type: "STRING",
			name: "song",
			description: "song to play",
			required: true
		}
	];

	public static joinVoiceFirstMessage: MessageOptions = {
		embeds: [{title:'Join a voice channel and then try that again!'}]
	};

	public static failedToJoinMessage: MessageOptions = {
		embeds: [{title:'Failed to join voice channel within 20 seconds, please try again later!'}]
	};

	public static failedToPlayMessage: MessageOptions = {
		embeds: [{title:'Failed to play track, please try again later!'}]
	};

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!message.guildId) return {success: false};

		const url: string = args.join(" ");

		if (!url) return {success: false, message: "Song name or url required"};

		let subscription: MusicSubscription = zakkBotClient.playerSubscriptions[message.guildId];

		const messageMember: GuildMember | undefined = await message.guild?.members.fetch(message.author.id);

		if (!subscription) {
			if (messageMember instanceof GuildMember && messageMember.voice.channel) {
				const channel: VoiceChannel | StageChannel = messageMember.voice.channel;
				subscription = new MusicSubscription(
					joinVoiceChannel({
						channelId: channel.id,
						guildId: channel.guild.id,
						adapterCreator: channel.guild.voiceAdapterCreator,
						selfDeaf: false
					}),
				);
				subscription.voiceConnection.on('error', (err: Error): void => Logger.log("error","warning", err));
			}
		}

		// If there is no subscription, tell the user they need to join a channel.
		if (!subscription) {
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, PlayNowCommand.joinVoiceFirstMessage, message.channel);
			return {success: false};
		}
		zakkBotClient.playerSubscriptions[message.guildId] = subscription;
		// Make sure the connection is ready before processing the user's request
		try {
			await entersState(subscription.voiceConnection, VoiceConnectionStatus.Ready, 20e3);
		} catch (error) {
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, PlayNowCommand.failedToJoinMessage, message.channel);
			return {success: false};
		}

		configBatch.module.data = configBatch.module.data as ConfigPlayerModule;

		try {
			// Attempt to create a Track from the user's video URL
			const tracks: Track[] | void = await Track.from(url, messageMember as GuildMember, {
				onStart(trackNow: Track, nextTrack: Track): void {
					trackNow.fulfillTrack().then(()=>{
						const newMessage: MessageEmbed = new MessageEmbed()
						.setAuthor(`Now playing`, undefined, trackNow.url)
						.setThumbnail(trackNow.thumbnail)
						.setDescription(`[${trackNow.title}](${trackNow.url})`)
						.addField("Length", trackNow.time)
						.addField("Requested by", `<@${trackNow.enquedBy.id}>`);
						if (nextTrack)
							newMessage.addField("Next in queue",  `[${nextTrack.title}](${nextTrack.url})`);
						if (!message.channel || message.channel.partial ) return;
						MessageHelper.Send({sendToChannel: (configBatch.module.data as ConfigPlayerModule).autoplay.djRoom, sendToOrigin: false}, { embeds: [newMessage] }, message.channel);
				});
					},
				onError(error: Error): void {
					MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, { embeds: [{title: `Error: ${error.message}`}] }, message.channel);
				}
			});
			// Enqueue the track and reply a success message to the user
			if (tracks) {
				tracks.forEach((track: Track): void => {
					subscription.enqueue(track);
				});
				if (tracks.length > 1) {
					MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, { embeds: [{title:`Enqueued playlist starting with **${tracks[0].title}**`}] }, message.channel);
				} else if (tracks[0]) {
					MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, { embeds: [{title:`Enqueued **${tracks[0].title}**`}] }, message.channel);
				}
			}
		} catch (error) {
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, PlayNowCommand.failedToPlayMessage, message.channel);
		}
		return {success: true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if(!interaction.guildId) return {success: false};

		const url: string = interaction.options.get('song')!.value! as string;

		let subscription: MusicSubscription = zakkBotClient.playerSubscriptions[interaction.guildId];

		if (!subscription) {
			if (interaction.member instanceof GuildMember && interaction.member.voice.channel) {
				const channel: VoiceChannel | StageChannel = interaction.member.voice.channel;
				subscription = new MusicSubscription(
					joinVoiceChannel({
						channelId: channel.id,
						guildId: channel.guild.id,
						adapterCreator: channel.guild.voiceAdapterCreator,
						selfDeaf: false
					}),
				);
				subscription.voiceConnection.on('error', (err: Error): void => Logger.log("", 'red', err));
			}
		}

		// If there is no subscription, tell the user they need to join a channel.
		if (!subscription) {
			await interaction.followUp('Join a voice channel and then try that again!');
			return {success: false};
		}
		zakkBotClient.playerSubscriptions[interaction.guildId] = subscription;
		// Make sure the connection is ready before processing the user's request
		try {
			await entersState(subscription.voiceConnection, VoiceConnectionStatus.Ready, 20e3);
		} catch (error) {
			await interaction.followUp('Failed to join voice channel within 20 seconds, please try again later!');
			return {success: false};
		}

		configBatch.module.data = configBatch.module.data as ConfigPlayerModule;

		try {
			// Attempt to create a Track from the user's video URL
			const tracks: Track[] | void = await Track.from(url, interaction.member as GuildMember, {
				onStart(trackNow: Track, nextTrack: Track): void {
					trackNow.fulfillTrack().then(()=>{
						const newMessage: MessageEmbed = new MessageEmbed()
						.setAuthor(`Now playing`, undefined, trackNow.url)
						.setThumbnail(trackNow.thumbnail)
						.setDescription(`[${trackNow.title}](${trackNow.url})`)
						.addField("Length", trackNow.time)
						.addField("Requested by", `<@${trackNow.enquedBy.id}>`);
						if (nextTrack)
							newMessage.addField("Next in queue",  `[${nextTrack.title}](${nextTrack.url})`);
						if (!interaction.channel || interaction.channel.partial ) return;
						MessageHelper.Send({sendToChannel: (configBatch.module.data as ConfigPlayerModule).autoplay.djRoom, sendToOrigin: false}, { embeds: [newMessage] }, interaction.channel);
					});
					},
				onError(error: Error): void {
					interaction.followUp({ content: `Error: ${error.message}`, ephemeral: true }).catch((err: string): void => Logger.log("", 'red', err));
				}
			});
			// Enqueue the track and reply a success message to the user
			if (tracks) {
				tracks.forEach((track: Track): void => {
					subscription.enqueue(track);
				});
				if (tracks.length > 1) {
					await interaction.followUp({ embeds: [{title:`Enqueued playlist starting with **${tracks[0].title}**`}] });
				} else if (tracks[0]) {
					await interaction.followUp({ embeds: [{title:`Enqueued **${tracks[0].title}**`}] });
				}
			}
		} catch (error) {
			await interaction.editReply(PlayNowCommand.failedToPlayMessage);
		}
		return {success: true};
	}
}

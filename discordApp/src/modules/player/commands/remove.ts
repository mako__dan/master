import { ApplicationCommandOption, CommandInteraction, Message, MessageOptions } from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { MusicSubscription } from '../../../util/music/subscription';
import { Track } from '../../../util/music/track';
import { ZakkBot } from '../../../zakkBot';

export class RemoveCommand extends CommandBase implements CommandInterface{
	name: string = 'remove';
	description: string = 'remove';
	help: string = 'remove';
	slashCommandParameters: ApplicationCommandOption[] = [
		{
			type: "NUMBER",
			name: "songnumber",
			description: "song to skip",
			required: true
		}
	];

	public static successMessage: MessageOptions = {
		embeds: [{title:'Succesfully removed song'}]
	};

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!message.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[message.guildId];
		const songId: number | null = Number(args[0]);
		if (subscription && songId) {
			if (subscription.queue[songId])

			subscription.queue = subscription.queue.filter((c: Track): boolean => {
				return subscription.queue.indexOf(c) === songId;
			});

			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, RemoveCommand.successMessage, message.channel);
		}
		return {success: true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!interaction.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[interaction.guildId];
		const songId: number | null = interaction.options.getNumber("songnumber");
		if (subscription && songId) {
			if (subscription.queue[songId])

			subscription.queue = subscription.queue.filter((c: Track): boolean => {
				return subscription.queue.indexOf(c) === songId;
			});

			await interaction.editReply(RemoveCommand.successMessage);
		}
		return {success: true};
	}
}
import { CommandInteraction, Message, MessageOptions } from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { MusicSubscription } from '../../../util/music/subscription';
import { ZakkBot } from '../../../zakkBot';

export class PauseCommand extends CommandBase implements CommandInterface{
	name: string = 'pause';
	description: string = 'pause';
	help: string = 'pause';

	public static pausedMessage: MessageOptions = {
		embeds: [{title:'Paused!'}]
	};

	public static notPlayingMessage: MessageOptions = {
		embeds: [{title:'Not playing in this server!'}]
	};

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!message.guild) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[message.guild.id];

		if (subscription) {
			subscription.audioPlayer.pause();
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, PauseCommand.pausedMessage, message.channel);
		} else {
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, PauseCommand.notPlayingMessage, message.channel);
		}
		return {success: true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if(!interaction.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[interaction.guildId];

		if (subscription) {
			subscription.audioPlayer.pause();
			await interaction.editReply(PauseCommand.pausedMessage);
		} else {
			await interaction.editReply(PauseCommand.notPlayingMessage);
		}
		return {success: true};
	}
}
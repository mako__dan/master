import {
	ColorResolvable, CommandInteraction, Message, MessageEmbed, MessageOptions
} from 'discord.js';

import { AudioPlayerStatus } from '@discordjs/voice';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch, ConfigGuild } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { MusicSubscription } from '../../../util/music/subscription';
import { Track } from '../../../util/music/track';
import { ZakkBot } from '../../../zakkBot';

export class QueueCommand extends CommandBase implements CommandInterface {
	name: string = 'queue';
	description: string = 'queue';
	help: string = 'queue';

	public static notPlayingMessage: MessageOptions = {
		embeds: [{ title: 'Not playing in this server!' }]
	};

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if (!message.guild) return { success: false };

		const serverConfig: ConfigGuild | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, message.guild.id, "moderation") as ConfigGuild | undefined;
		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[message.guild.id];
		// Print out the current queue, including up to the next 5 tracks to be played.
		if (!serverConfig) return { success: false };

		if (subscription) {
			const embed: MessageEmbed = new MessageEmbed()
				.setColor(serverConfig.themeColor as ColorResolvable);

			subscription.audioPlayer.state.status === AudioPlayerStatus.Idle
				? embed.setDescription(`Nothing is currently playing!`)
				: embed.addField(`Now playing`, `[${subscription.queue[0].title}](${subscription.queue[0].url}) | ${subscription.queue[0].time} Requested by <@${subscription.queue[0].enquedBy.id}>`);

			let displayLength: number = subscription.queue.length;
			if (displayLength > 25) {
				displayLength = 25;
			}
			for (let i: number = 1; i < displayLength; i++) {
				const song: Track = await subscription.queue[i];
				await song.fulfillTrack();
				embed.addField(`\u200b`, `${i + 1}) [${song.title}](${song.url}) | ${song.time} Requested by <@${song.enquedBy.id}>`);
			}

			MessageHelper.Send({ sendToChannel: "", sendToOrigin: true }, { embeds: [embed] }, message.channel);
		} else {
			MessageHelper.Send({ sendToChannel: "", sendToOrigin: true }, QueueCommand.notPlayingMessage, message.channel);
		}

		return { success: true };
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if (!interaction.guild) return { success: false };

		const serverConfig: ConfigGuild | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, interaction.guild.id, "moderation") as ConfigGuild | undefined;
		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[interaction.guild.id];
		// Print out the current queue, including up to the next 5 tracks to be played.
		if (!serverConfig) return { success: false };

		if (subscription) {
			const embed: MessageEmbed = new MessageEmbed()
				.setColor(serverConfig.themeColor as ColorResolvable);

			subscription.audioPlayer.state.status === AudioPlayerStatus.Idle
				? embed.setDescription(`Nothing is currently playing!`)
				: embed.addField(`Now playing`, `[${subscription.queue[0].title}](${subscription.queue[0].url}) | ${subscription.queue[0].time} Requested by <@${subscription.queue[0].enquedBy.id}>`);

			let displayLength: number = subscription.queue.length;
			if (displayLength > 25) {
				displayLength = 25;
			}
			for (let i: number = 1; i < displayLength; i++) {
				const song: Track = subscription.queue[i];
				await song.fulfillTrack();
				embed.addField(`\u200b`, `${i + 1}) [${song.title}](${subscription.queue[i].url}) | ${song.time} Requested by <@${song.enquedBy.id}>`);
			}

			await interaction.editReply({ embeds: [embed] });
		} else {
			await interaction.editReply(QueueCommand.notPlayingMessage);
		}

		return { success: true };
	}
}
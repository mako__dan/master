import { CommandInteraction, Message, MessageOptions } from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { MusicSubscription } from '../../../util/music/subscription';
import { ZakkBot } from '../../../zakkBot';

export class ResumeCommand extends CommandBase implements CommandInterface{
	name: string = 'resume';
	description: string = 'resume';
	help: string = 'resume';

	public static notPlayingMessage: MessageOptions = {
		embeds: [{title:'Not playing in this server!'}]
	};

	public static unpausedMessage: MessageOptions = {
		embeds: [{title:'Unpaused!'}]
	};

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!message.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[message.guildId];

		if (subscription) {
			subscription.audioPlayer.unpause();
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, ResumeCommand.unpausedMessage, message.channel);
		} else {
			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, ResumeCommand.notPlayingMessage, message.channel);
		}
		return {success: true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!interaction.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[interaction.guildId];

		if (subscription) {
			subscription.audioPlayer.unpause();
			await interaction.editReply(ResumeCommand.unpausedMessage);
		} else {
			await interaction.editReply(ResumeCommand.notPlayingMessage);
		}
		return {success: true};
	}
}
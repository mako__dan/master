import { CommandInteraction, Message, MessageOptions } from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { MusicSubscription } from '../../../util/music/subscription';
import { Track } from '../../../util/music/track';
import { ZakkBot } from '../../../zakkBot';

export class RemoveDupesCommand extends CommandBase implements CommandInterface{
	name: string = 'removedupes';
	description: string = 'removedupes';
	help: string = 'removedupes';

	public static successMessage: MessageOptions = {
		embeds: [{title:'Succesfully removed song'}]
	};

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!message.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[message.guildId];

		if (subscription) {
			subscription.queue = subscription.queue.filter((c: Track, index: number): boolean => {
				return subscription.queue.indexOf(c) === index;
			});

			MessageHelper.Send({sendToChannel: "", sendToOrigin: true}, RemoveDupesCommand.successMessage, message.channel);
		}
		return {success: true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if(!interaction.guildId) return {success: false};

		const subscription: MusicSubscription = zakkBotClient.playerSubscriptions[interaction.guildId];

		if (subscription) {
			subscription.queue = subscription.queue.filter((c: Track, index: number): boolean => {
				return subscription.queue.indexOf(c) === index;
			});

			await interaction.editReply(RemoveDupesCommand.successMessage);
		}
		return {success: true};
	}
}
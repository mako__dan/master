import { ModuleBase } from '../../bases/moduleBase';
import { ModuleInterface } from '../../bases/moduleInterface';
import { ModuleLoader } from '../../util/moduleLoader';
import { ClearCommand } from './commands/clear';
import { LeaveCommand } from './commands/leave';
import { LyricsCommand } from './commands/lyrics';
import { PauseCommand } from './commands/pause';
import { PlayCommand } from './commands/play';
import { PlayNowCommand } from './commands/playNow';
import { PlayTopCommand } from './commands/playTop';
import { QueueCommand } from './commands/queue';
import { RemoveCommand } from './commands/remove';
import { RemoveDupesCommand } from './commands/removedupes';
import { ResumeCommand } from './commands/resume';
import { ShuffleCommand } from './commands/shuffle';
import { SkipCommand } from './commands/skip';
import { PlayerVoiceStateUpdateFeature } from './features/voiceStateUpdate';

export class PlayerModule extends ModuleBase implements ModuleInterface {
	public moduleName: string = "player";

	async initCommands(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadCommand(this.moduleName, new PlayCommand()),
			ModuleLoader.loadCommand(this.moduleName, new QueueCommand()),
			ModuleLoader.loadCommand(this.moduleName, new SkipCommand()),
			ModuleLoader.loadCommand(this.moduleName, new LeaveCommand()),
			ModuleLoader.loadCommand(this.moduleName, new ResumeCommand()),
			ModuleLoader.loadCommand(this.moduleName, new PauseCommand()),
			ModuleLoader.loadCommand(this.moduleName, new PlayNowCommand()),
			ModuleLoader.loadCommand(this.moduleName, new PlayTopCommand()),
			ModuleLoader.loadCommand(this.moduleName, new ShuffleCommand()),
			ModuleLoader.loadCommand(this.moduleName, new ClearCommand()),
			ModuleLoader.loadCommand(this.moduleName, new RemoveCommand()),
			ModuleLoader.loadCommand(this.moduleName, new RemoveDupesCommand()),
			ModuleLoader.loadCommand(this.moduleName, new LyricsCommand()),
		]);
	}
	async initFeatures(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadFeature(new PlayerVoiceStateUpdateFeature())
		]);
	}
}
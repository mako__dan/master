import { DMChannel, GuildMember, Message, MessageOptions } from 'discord.js';

import { FeatureBase } from '../../../bases/featureBase';
import { FeatureInterface } from '../../../bases/featureInterface';
import { ConfigAutoreplyModule, ConfigAutoreplyReply, ConfigModule } from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';

export class AutoreplyMessageCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "messageCreate/autoreply";
	disabled: boolean = false;
	once: boolean = false;
	async run(zakkBotClient: ZakkBot, message: Message): Promise<void> {
		if (message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return;

		const moduleConfig: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, message.channel.guild.id, "autoreply");
		if (!moduleConfig || !moduleConfig.enabled || !(moduleConfig.data as ConfigAutoreplyModule) || !message.guild) return;
		moduleConfig.data = moduleConfig.data as ConfigAutoreplyModule;

		moduleConfig.data.replies.forEach(async (replyset: ConfigAutoreplyReply): Promise<void> => {
			if (replyset.triggers && replyset.triggers.includes(message.content)) {
				const memberData: GuildMember | undefined = await message.guild?.members.fetch(message.author.id);
				const randomizer: number = Math.floor(Math.random() * replyset.replies.length);
				let messageData: MessageOptions = replyset.replies[randomizer];
				if (!message.guild || !memberData) return;
				messageData = MessageHelper.ReplaceWildcardsInBuilder(messageData , {guild: message.guild, member: memberData, botUser: (await (await zakkBotClient.discordClient.guilds.fetch( message.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076"))});
				MessageHelper.SendRemovable({sendToChannel: "", sendToOrigin: true} , replyset.replies[randomizer], message.channel, replyset.autodelete);
			}
		});
	}
}
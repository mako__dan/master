import { ModuleBase } from '../../bases/moduleBase';
import { ModuleInterface } from '../../bases/moduleInterface';
import { ModuleLoader } from '../../util/moduleLoader';
import { ZakkBot } from '../../zakkBot';
import { AutoreplyMessageCreateFeature } from './features/messageCreate';

export class AutoreplyModule extends ModuleBase implements ModuleInterface{
	public moduleName: string = "autoreply";

	async initFeatures(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadFeature(new AutoreplyMessageCreateFeature())
		]);
	}
}
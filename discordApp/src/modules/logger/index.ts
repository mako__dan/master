// this is system module. Bot will not run without tihs module present

import { ModuleBase } from '../../bases/moduleBase';
import { ModuleInterface } from '../../bases/moduleInterface';

export class LoggerModule extends ModuleBase implements ModuleInterface {
	public moduleName: string = "logger";

	async initFeatures(): Promise<void[]> {
		return Promise.all([]);
	}
}
// this is system module. Bot will not run without tihs module present

import {
	Guild, GuildAuditLogs, GuildAuditLogsEntry, GuildChannel, GuildEmoji, Integration, Invite,
	Message, MessageOptions, Role, Snowflake, StageInstance, Sticker, TextChannel, ThreadChannel,
	User, Webhook
} from 'discord.js';

import { ConfigLoggerModule, ConfigModule } from '../../../types/configs';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';
import { LoggerModuleGlobal } from './global';

export class LoggerModuleEmojiHandler{

	static async logEmojiCreate(zakkBotClient: ZakkBot, emoji: GuildEmoji): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, emoji.guild.id);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.emojiCreate.enabled) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Created emote ${emoji.name}`,
					fields: [
						{name: "Data", value: `Name: ${emoji.name} \n`}
					],
					thumbnail: {
						url: `https://cdn.discordapp.com/emojis/${emoji.id}.png?size=4096`
					},
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await emoji.guild.fetchAuditLogs({
				limit: 1,
				type: 'EMOJI_CREATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof GuildEmoji && target.id === emoji.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.emojiCreate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, emoji.guild.channels.cache.first() as TextChannel);
		}
	}

	static async logEmojiDelete(zakkBotClient: ZakkBot, emoji: GuildEmoji): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, emoji.guild.id);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.emojiDelete.enabled) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Deleted emote ${emoji.name}`,
					fields: [
						{name: "Name", value: emoji.name ?? ""}
					],
					thumbnail: {
						url: `https://cdn.discordapp.com/emojis/${emoji.id}.png?size=4096`
					},
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await emoji.guild.fetchAuditLogs({
				limit: 1,
				type: 'EMOJI_DELETE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof GuildEmoji && target.id === emoji.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.emojiDelete.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, emoji.guild.channels.cache.first() as TextChannel);
		}
	}

	static async logEmojiUpdate(zakkBotClient: ZakkBot, oldEmoji: GuildEmoji, newEmoji: GuildEmoji): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, oldEmoji.guild.id);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.emojiUpdate.enabled) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Updated emote ${oldEmoji.name}`,
					fields: [
						{name: "Name", value: `${oldEmoji.name} -> ${newEmoji.name}\n`}
					],
					thumbnail: {
						url: `https://cdn.discordapp.com/emojis/${oldEmoji.id}.png?size=4096`
					},
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await newEmoji.guild.fetchAuditLogs({
				limit: 1,
				type: 'EMOJI_UPDATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();
			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof GuildEmoji && target.id === newEmoji.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.emojiUpdate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, newEmoji.guild.channels.cache.first() as TextChannel);
		}
	}
}
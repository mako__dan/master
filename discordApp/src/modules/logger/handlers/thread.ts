// this is system module. Bot will not run without tihs module present

import {
	Guild, GuildAuditLogs, GuildAuditLogsEntry, GuildChannel, GuildEmoji, Integration, Invite,
	Message, MessageOptions, Role, Snowflake, StageInstance, Sticker, TextChannel, ThreadChannel,
	User, Webhook
} from 'discord.js';

import { ConfigLoggerModule, ConfigModule } from '../../../types/configs';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';
import { LoggerModuleGlobal } from './global';

export class LoggerModuleThreadHandler{

	static async logThreadCreate(zakkBotClient: ZakkBot, thread: ThreadChannel): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, thread.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.threadCreate.enabled) {
			let dataString: string = `Name: ${thread.name} \n`;
			dataString += `Type: Thread \n`;
			dataString += `Locked: ${thread.locked} \n`;
			dataString += `Duration: ${thread.autoArchiveDuration} \n`;

			// TODO overrides

			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Created thread <#${thread.id}>`,
					fields: [
						{name: "Data", value: dataString}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await thread.guild.fetchAuditLogs({
				limit: 1,
				type: 'THREAD_CREATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof ThreadChannel && target.id === thread.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.threadCreate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, thread.parent as TextChannel);
		}
	}

	static async logThreadDelete(zakkBotClient: ZakkBot, thread: ThreadChannel): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, thread.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.threadDelete.enabled) {
			let dataString: string = `Name: ${thread.name} \n`;
			dataString += `Type: Thread \n`;
			dataString += `Locked: ${thread.locked} \n`;
			dataString += `Duration: ${thread.autoArchiveDuration} \n`;

			// TODO overrides

			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Deleted thread <#${thread.id}>`,
					fields: [
						{name: "Data", value: dataString}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await thread.guild.fetchAuditLogs({
				limit: 1,
				type: 'THREAD_DELETE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof ThreadChannel && target.id === thread.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.threadDelete.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, thread.parent as TextChannel);
		}
	}

	static async logThreadUpdate(zakkBotClient: ZakkBot, oldThread: ThreadChannel, newThread: ThreadChannel): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, oldThread.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.threadUpdate.enabled) {
			let dataString: string = "";
			if (oldThread.name !== newThread.name) dataString = `Updated name: ${oldThread.name} =>  ${newThread.name}\n`;
			if (oldThread.locked !== newThread.locked) dataString += `Updated locked status: ${oldThread.locked} =>  ${newThread.locked}\n`;
			if (oldThread.autoArchiveDuration !== newThread.autoArchiveDuration) dataString += `Updated duration: ${oldThread.autoArchiveDuration} =>  ${newThread.autoArchiveDuration}\n`;

			// TODO overrides

			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Updated thread <#${newThread.id}>`,
					fields: [
						{name: "Data", value: dataString}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await newThread.guild.fetchAuditLogs({
				limit: 1,
				type: 'THREAD_UPDATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();
			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof ThreadChannel && target.id === newThread.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.threadUpdate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, newThread.parent as TextChannel);
		}
	}
}
// this is system module. Bot will not run without tihs module present

import {
	Guild, GuildAuditLogs, GuildAuditLogsEntry, GuildChannel, GuildEmoji, Integration, Invite,
	Message, MessageOptions, PermissionString, Role, Snowflake, StageInstance, Sticker, TextChannel,
	ThreadChannel, User, Webhook
} from 'discord.js';

import { ConfigLoggerModule, ConfigModule } from '../../../types/configs';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';
import { LoggerModuleGlobal } from './global';

export class LoggerModuleRoleHandler{

	static async logRoleCreate(zakkBotClient: ZakkBot, role: Role): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, role.guild.id);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.roleCreate.enabled) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Created role ${role.name}`,
					fields: [
						{name: "Data", value: `Name: ${role.name} \n`}
					],
					thumbnail: {
						url: `https://cdn.discordapp.com/emojis/${role.id}.png?size=4096`
					},
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await role.guild.fetchAuditLogs({
				limit: 1,
				type: 'ROLE_CREATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof Role && target.id === role.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.roleCreate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, role.guild.channels.cache.first() as TextChannel);
		}
	}

	static async logRoleDelete(zakkBotClient: ZakkBot, role: Role): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, role.guild.id);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.roleDelete.enabled) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Deleted role ${role.name}`,
					fields: [
						{name: "Name", value: role.name ?? ""}
					],
					thumbnail: {
						url: `https://cdn.discordapp.com/emojis/${role.id}.png?size=4096`
					},
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await role.guild.fetchAuditLogs({
				limit: 1,
				type: 'ROLE_DELETE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof Role && target.id === role.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.roleDelete.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, role.guild.channels.cache.first() as TextChannel);
		}
	}

	static async logRoleUpdate(zakkBotClient: ZakkBot, oldRole: Role, newRole: Role): Promise<void> {
		// return if change is not valid to display
		if (oldRole.color === newRole.color && oldRole.hexColor === newRole.hexColor && oldRole.hoist === newRole.hoist && oldRole.name === newRole.name && oldRole.tags === newRole.tags && oldRole.permissions.bitfield === newRole.permissions.bitfield) return;

		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, oldRole.guild.id);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.roleUpdate.enabled) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;

			const oldRoleArray: PermissionString[] = oldRole.permissions.toArray();
			const newRoleArray: PermissionString[] = newRole.permissions.toArray();
			const roleAdded: PermissionString[] = newRoleArray.filter((x: PermissionString): boolean => !oldRoleArray.includes(x));
			const roleRemoved: PermissionString[] = oldRoleArray.filter((x: PermissionString): boolean => !newRoleArray.includes(x));

			let changesString: string = "";
			roleAdded.forEach((addedRole: PermissionString): void => {
				changesString += `+ ${addedRole} \n`;
			});
			roleRemoved.forEach((removedRole: PermissionString): void => {
				changesString +=`- ${removedRole} \n`;
			});

			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Updated role ${oldRole.name}`,
					fields: [],
					thumbnail: {
						url: `https://cdn.discordapp.com/emojis/${oldRole.id}.png?size=4096`
					},
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};
			if (embed.embeds && embed.embeds[0].fields) {
				if (oldRole.name !== newRole.name) embed.embeds[0].fields.push({name: "Name", value: `${oldRole.name} -> ${newRole.name}`, inline: false});
				if (oldRole.color !== newRole.color) embed.embeds[0].fields.push({name: "Color", value: `${oldRole.color} -> ${newRole.color}`, inline: false});
				if (oldRole.hexColor !== newRole.hexColor) embed.embeds[0].fields.push({name: "Color#2", value: `${oldRole.hexColor} -> ${newRole.hexColor}`, inline: false});
				if (oldRole.hoist !== newRole.hoist) embed.embeds[0].fields.push({name: "Hoist", value: `${oldRole.hoist} -> ${newRole.hoist}`, inline: false});
				if (changesString) embed.embeds[0].fields.push({name: "Permission changes", value: `${changesString}`, inline: false});
			}

			const fetchedLogs: GuildAuditLogs = await newRole.guild.fetchAuditLogs({
				limit: 1,
				type: 'ROLE_UPDATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();
			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof Role && target.id === newRole.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}
			MessageHelper.Send({sendToChannel: moduleConfig.data.events.roleUpdate.channel, sendToOrigin: false}, embed, newRole.guild.channels.cache.first() as TextChannel);
		}
	}
}
// this is system module. Bot will not run without tihs module present

import {
	CategoryChannel, Guild, GuildAuditLogs, GuildAuditLogsEntry, GuildChannel, GuildEmoji,
	Integration, Invite, Message, MessageOptions, NewsChannel, Role, Snowflake, StageChannel,
	StageInstance, Sticker, StoreChannel, TextChannel, ThreadChannel, User, VoiceChannel, Webhook
} from 'discord.js';

import { ConfigLoggerModule, ConfigModule } from '../../../types/configs';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';
import { LoggerModuleGlobal } from './global';

export class LoggerModuleChannelHandler{

	static async logChannelCreate(zakkBotClient: ZakkBot, channel: GuildChannel): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, channel.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.channelCreate.enabled) {
			let dataString: string = `Name: ${channel.name} \n`;
			if (channel instanceof TextChannel ||channel instanceof NewsChannel) {
				dataString += `Topic: ${channel.topic} \n`;
				dataString += `Type: Text \n`;
				dataString += `NSFW: ${channel.nsfw} \n`;
			}
			if (channel instanceof VoiceChannel) {
				dataString += `Type: Voice \n`;
				dataString += `Limit: ${channel.userLimit} \n`;
				dataString += `Bitrate: ${channel.bitrate} \n`;
			}
			if (channel instanceof StageChannel) {
				dataString += `Topic: ${channel.topic} \n`;
				dataString += `Type: Stage \n`;
				dataString += `Limit: ${channel.userLimit} \n`;
				dataString += `Bitrate: ${channel.bitrate} \n`;
			}
			if (channel instanceof StoreChannel) {
				dataString += `Type: Store \n`;
				dataString += `NSFW: ${channel.nsfw} \n`;
			}
			if (channel instanceof CategoryChannel) {
				dataString += `Type: Category \n`;
			}

			// TODO overrides

			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Created channel <#${channel.id}>`,
					fields: [
						{name: "Data", value: dataString}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await channel.guild.fetchAuditLogs({
				limit: 1,
				type: 'CHANNEL_CREATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof GuildChannel && target.id === channel.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.channelCreate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, channel as TextChannel);
		}
	}

	static async logChannelDelete(zakkBotClient: ZakkBot, channel: GuildChannel): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, channel.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.channelDelete.enabled) {
			let dataString: string = `Name: ${channel.name} \n`;
			if (channel instanceof TextChannel ||channel instanceof NewsChannel) {
				dataString += `Topic: ${channel.topic} \n`;
				dataString += `Type: Text \n`;
				dataString += `NSFW: ${channel.nsfw} \n`;
			}
			if (channel instanceof VoiceChannel) {
				dataString += `Type: Voice \n`;
				dataString += `Limit: ${channel.userLimit} \n`;
				dataString += `Bitrate: ${channel.bitrate} \n`;
			}
			if (channel instanceof StageChannel) {
				dataString += `Topic: ${channel.topic} \n`;
				dataString += `Type: Stage \n`;
				dataString += `Limit: ${channel.userLimit} \n`;
				dataString += `Bitrate: ${channel.bitrate} \n`;
			}
			if (channel instanceof StoreChannel) {
				dataString += `Type: Store \n`;
				dataString += `NSFW: ${channel.nsfw} \n`;
			}
			if (channel instanceof CategoryChannel) {
				dataString += `Type: Category \n`;
			}

			// TODO overrides

			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Deleted channel <#${channel.id}>`,
					fields: [
						{name: "Data", value: dataString}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await channel.guild.fetchAuditLogs({
				limit: 1,
				type: 'CHANNEL_DELETE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof GuildChannel && target.id === channel.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.channelDelete.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, channel as TextChannel);
		}
	}

	static async logChannelUpdate(zakkBotClient: ZakkBot, oldChannel: GuildChannel, newChannel: GuildChannel): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, oldChannel.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.channelUpdate.enabled) {
			let dataString: string = "";
			if (oldChannel.name !== newChannel.name) dataString = `Updated name: ${oldChannel.name} =>  ${newChannel.name}\n`;
			if ((oldChannel instanceof TextChannel || oldChannel instanceof NewsChannel) && (newChannel instanceof TextChannel ||newChannel instanceof NewsChannel)) {
				if (oldChannel.topic !== newChannel.topic) dataString += `Updated topic: ${oldChannel.topic} =>  ${newChannel.topic}\n`;
				if (oldChannel.nsfw !== newChannel.nsfw) dataString += `Updated nsfw: ${oldChannel.nsfw} =>  ${newChannel.nsfw}\n`;
			}
			if (oldChannel instanceof VoiceChannel && newChannel instanceof VoiceChannel) {
				if (oldChannel.userLimit !== newChannel.userLimit) dataString += `Updated limit: ${oldChannel.userLimit} =>  ${newChannel.userLimit}\n`;
				if (oldChannel.bitrate !== newChannel.bitrate) dataString += `Updated bitrate: ${oldChannel.bitrate} =>  ${newChannel.bitrate}\n`;
			}
			if (oldChannel instanceof StageChannel && newChannel instanceof StageChannel) {
				if (oldChannel.topic !== newChannel.topic) dataString += `Updated topic: ${oldChannel.topic} =>  ${newChannel.topic}\n`;
				if (oldChannel.userLimit !== newChannel.userLimit) dataString += `Updated limit: ${oldChannel.userLimit} =>  ${newChannel.userLimit}\n`;
				if (oldChannel.bitrate !== newChannel.bitrate) dataString += `Updated bitrate: ${oldChannel.bitrate} =>  ${newChannel.bitrate}\n`;
			}
			if (oldChannel instanceof StoreChannel && newChannel instanceof StoreChannel) {
				if (oldChannel.nsfw !== newChannel.nsfw) dataString += `Updated nsfw: ${oldChannel.nsfw} =>  ${newChannel.nsfw}\n`;
			}

			// TODO overrides

			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Updated channel <#${newChannel.id}>`,
					fields: [
						{name: "Data", value: dataString}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await newChannel.guild.fetchAuditLogs({
				limit: 1,
				type: 'CHANNEL_UPDATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();
			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof GuildChannel && target.id === newChannel.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.channelUpdate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, newChannel as TextChannel);
		}
	}
}
// this is system module. Bot will not run without tihs module present

import {
	Guild, GuildAuditLogs, GuildAuditLogsEntry, GuildChannel, GuildEmoji, Integration, Invite,
	Message, MessageOptions, PermissionString, Role, Snowflake, StageInstance, Sticker, TextChannel,
	ThreadChannel, User, Webhook
} from 'discord.js';

import { ConfigLoggerModule, ConfigModule } from '../../../types/configs';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';
import { LoggerModuleGlobal } from './global';

export class LoggerModuleInviteHandler{

	static async logInviteCreate(zakkBotClient: ZakkBot, invite: Invite): Promise<void> {
		if (!invite.guild) return;
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, invite.guild.id);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.inviteCreate.enabled) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: invite.inviter?.username ?? "Unknown",
						iconURL: invite.inviter?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Created invite ${invite.code}`,
					fields: [
						{name: "Data", value: `Code: ${invite.code} \nCode: ${invite.expiresAt?.toISOString()} \nCreated by: ${invite.inviter?.username} \nCreated by: ${invite.maxAge} \nCreated by: ${invite.maxUses} \nUsed: ${invite.uses}`}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.inviteCreate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, zakkBotClient.discordClient.guilds.cache.get(invite.guild.id)?.channels.cache.first() as TextChannel);
		}
	}

	static async logInviteDelete(zakkBotClient: ZakkBot, invite: Invite): Promise<void> {
		if (!invite.guild) return;
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, invite.guild.id);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.inviteDelete.enabled) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Deleted invite ${invite.code}`,
					fields: [
						{name: "Data", value: `Code: ${invite.code} \nCode: ${invite.expiresAt?.toISOString()} \nCreated by: ${invite.inviter} \nCreated by: ${invite.maxAge} \nCreated by: ${invite.maxUses} \nUsed: ${invite.uses}`}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs | undefined = await zakkBotClient.discordClient.guilds.cache.get(invite.guild.id)?.fetchAuditLogs({
				limit: 1,
				type: 'INVITE_DELETE',
			});
			if (fetchedLogs) {
				const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

				if (deletionLog) {
					const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
					const executorAvatar: string | null | undefined = executor?.avatarURL();
					if (target instanceof Invite && target.code === invite.code && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
						embed.embeds[0].author.iconURL = executorAvatar;
						embed.embeds[0].author.name = executor?.tag;
					}
				}
			}
			MessageHelper.Send({sendToChannel: moduleConfig.data.events.inviteDelete.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, zakkBotClient.discordClient.guilds.cache.get(invite.guild.id)?.channels.cache.first() as TextChannel);
		}
	}
}
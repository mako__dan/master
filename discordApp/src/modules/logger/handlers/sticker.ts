// this is system module. Bot will not run without tihs module present

import {
	Guild, GuildAuditLogs, GuildAuditLogsEntry, GuildChannel, GuildEmoji, Integration, Invite,
	Message, MessageOptions, Role, Snowflake, StageInstance, Sticker, TextChannel, ThreadChannel,
	User, Webhook
} from 'discord.js';

import { ConfigLoggerModule, ConfigModule } from '../../../types/configs';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';
import { LoggerModuleGlobal } from './global';

export class LoggerModuleStickerHandler{

	static async logStickerCreate(zakkBotClient: ZakkBot, sticker: Sticker): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, sticker.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.stickerCreate.enabled && sticker.guild) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Created sticker ${sticker.name}`,
					fields: [
						{name: "Data", value: `Name: ${sticker.name} \nDescription: ${sticker.description}`}
					],
					thumbnail: {
						url: sticker.url
					},
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await sticker.guild.fetchAuditLogs({
				limit: 1,
				type: 'STICKER_CREATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof Sticker && target.id === sticker.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.stickerCreate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, sticker.guild.channels.cache.first() as TextChannel);
		}
	}

	static async logStickerDelete(zakkBotClient: ZakkBot, sticker: Sticker): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, sticker.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.stickerDelete.enabled && sticker.guild) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Deleted sticker ${sticker.name}`,
					fields: [
						{name: "Data", value: `Name: ${sticker.name} \nDescription: ${sticker.description}`}
					],
					thumbnail: {
						url: sticker.url
					},
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await sticker.guild.fetchAuditLogs({
				limit: 1,
				type: 'STICKER_DELETE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof Sticker && target.id === sticker.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.stickerDelete.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, sticker.guild.channels.cache.first() as TextChannel);
		}
	}

	static async logStickerUpdate(zakkBotClient: ZakkBot, oldSticker: Sticker, newSticker: Sticker): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, oldSticker.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.stickerUpdate.enabled && oldSticker.guild && newSticker.guild) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Updated sticker ${oldSticker.name}`,
					fields: [
						{name: "Name", value: `${oldSticker.name} -> ${newSticker.name}\n`},
						{name: "Description", value: `${oldSticker.description} -> ${newSticker.description}`}
					],
					thumbnail: {
						url: newSticker.url
					},
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await newSticker.guild.fetchAuditLogs({
				limit: 1,
				type: 'STICKER_UPDATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();
			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof Sticker && target.id === newSticker.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.stickerUpdate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, newSticker.guild.channels.cache.first() as TextChannel);
		}
	}
}
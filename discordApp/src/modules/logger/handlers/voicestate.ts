// this is system module. Bot will not run without tihs module present

import {
	ClientUser, Guild, GuildAuditLogs, GuildAuditLogsEntry, GuildChannel, GuildEmoji, GuildMember,
	Integration, Invite, Message, MessageOptions, Role, Snowflake, StageInstance, Sticker,
	TextChannel, ThreadChannel, User, VoiceChannel, VoiceState, Webhook
} from 'discord.js';

import { ConfigLoggerModule, ConfigModule } from '../../../types/configs';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';
import { LoggerModuleGlobal } from './global';

export class LoggerModuleVoicestateHandler{

	static async logVoicestateUpdate(zakkBotClient: ZakkBot, oldVoiceState: VoiceState, newVoiceState: VoiceState): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, oldVoiceState.guild.id);
		const member: GuildMember | undefined = newVoiceState.guild.members.cache.find((x: GuildMember) => x.id === newVoiceState.id);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.voiceStateUpdate.enabled && member) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			if (oldVoiceState.channelId === null) {
				this.logMessage(zakkBotClient, moduleConfig.data, "joined", member, newVoiceState);
				return;
			}
			if (newVoiceState.channelId === null) {
				this.logMessage(zakkBotClient, moduleConfig.data, "leaved", member, newVoiceState);
				return;
			}
			if (oldVoiceState.channelId !== oldVoiceState.channelId) {
				this.logMessage(zakkBotClient, moduleConfig.data, "changed voice channel", member, newVoiceState);
				return;
			}
			if (oldVoiceState.serverDeaf === false && newVoiceState.serverDeaf === true) {
				this.logMessage(zakkBotClient, moduleConfig.data, "Has been defeaned", member, newVoiceState, true);
				return;
			}
			if (oldVoiceState.serverDeaf === true && newVoiceState.serverDeaf === false) {
				this.logMessage(zakkBotClient, moduleConfig.data, "Has been nudefeaned", member, newVoiceState, true);
				return;
			}
			if (oldVoiceState.serverMute === false && newVoiceState.serverMute === true) {
				this.logMessage(zakkBotClient, moduleConfig.data, "Has been muted", member, newVoiceState, true);
				return;
			}
			if (oldVoiceState.selfVideo === true && newVoiceState.selfVideo === false) {
				this.logMessage(zakkBotClient, moduleConfig.data, "Has been unmuted", member, newVoiceState, true);
				return;
			}
			if (oldVoiceState.streaming === false && newVoiceState.streaming === true) {
				this.logMessage(zakkBotClient, moduleConfig.data, "Started streaming", member, newVoiceState);
				return;
			}
			if (oldVoiceState.streaming === true && newVoiceState.streaming === false) {
				this.logMessage(zakkBotClient, moduleConfig.data, "Stopped streaming", member, newVoiceState);
				return;
			}
			if (oldVoiceState.suppress === false && newVoiceState.suppress === true) {
				this.logMessage(zakkBotClient, moduleConfig.data, "Suppressed", member, newVoiceState);
				return;
			}
			if (oldVoiceState.suppress === true && newVoiceState.suppress === false) {
				this.logMessage(zakkBotClient, moduleConfig.data, "Unsupressed", member, newVoiceState);
				return;
			}
			if (oldVoiceState.requestToSpeakTimestamp === null && newVoiceState.requestToSpeakTimestamp !== null) {
				this.logMessage(zakkBotClient, moduleConfig.data, "Requested to speak", member, newVoiceState);
				return;
			}
			if (oldVoiceState.requestToSpeakTimestamp !== null && newVoiceState.requestToSpeakTimestamp === null) {
				this.logMessage(zakkBotClient, moduleConfig.data, "Removed request to speak", member, newVoiceState);
				return;
			}
		}
	}

	static async logMessage(zakkBotClient: ZakkBot, moduleData: ConfigLoggerModule, displayString: string, member: GuildMember, newVoiceState: VoiceState, checkAuditLog?: boolean): Promise<void> {
		const embed: MessageOptions = {
			embeds: [{
				author: {
					name: member.displayName,
					iconURL: member.user.avatarURL() ?? ""
				},
				description: `Voice state updated for <@${member.id}>`,
				fields: [
					{name: "Data", value: displayString}
				],
				footer: {
					iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
					text: zakkBotClient.discordClient.user?.username
				}
			}]
		};

		if (true) {
			const fetchedLogs: GuildAuditLogs = await newVoiceState.guild.fetchAuditLogs({
				limit: 1
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();
			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof ClientUser && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}
			}
		}
		MessageHelper.Send({sendToChannel: moduleData.events.voiceStateUpdate.channel ?? moduleData.defaultChannel, sendToOrigin: false}, embed, newVoiceState.guild.channels.cache.first() as TextChannel);
	}
}
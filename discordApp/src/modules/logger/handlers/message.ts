// this is system module. Bot will not run without tihs module present

import {
	Guild, GuildAuditLogs, GuildAuditLogsEntry, GuildChannel, GuildEmoji, Integration, Invite,
	Message, MessageOptions, Role, Snowflake, StageInstance, Sticker, ThreadChannel, User, Webhook
} from 'discord.js';
import moment from 'moment';

import { ConfigLoggerModule, ConfigModule } from '../../../types/configs';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';
import { LoggerModuleGlobal } from './global';

export class LoggerModuleMessageHandler{

	static async logMessageDelete(zakkBotClient: ZakkBot, message: Message): Promise<void> {
		if (!message.guild) return;
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, message.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.messageDelete.enabled) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						iconURL: message.author.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						name: message.author.username
					},
					description: `Message deleted in <#${message.channelId}>`,
					fields: [
						{name: "Content", value: message.content},
						{name: "Date", value: moment(message.createdTimestamp).toISOString()}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await message.guild.fetchAuditLogs({
				limit: 1,
				type: 'MESSAGE_DELETE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof Message && target.id === message.author.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.messageDelete.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, message.channel);
		}
	}

	static async logMessageUpdate(zakkBotClient: ZakkBot, oldMessage: Message, newMessage: Message): Promise<void> {
		if (!oldMessage.guild) return;
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, oldMessage.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.messageUpdate.enabled) {
			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						iconURL: oldMessage.author.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						name: oldMessage.author.username
					},
					description: `Message deleted in <#${oldMessage.channelId}>`,
					fields: [
						{name: "New content", value: newMessage.content},
						{name: "Old content", value: oldMessage.content},
						{name: "Date", value: moment(oldMessage.createdTimestamp).toISOString()},
						{name: "Link", value: `[Go to message](https://discord.com/channels/${oldMessage.guildId}/${oldMessage.channelId}/${oldMessage.id})`}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.messageUpdate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, oldMessage.channel);
		}
	}
}
// this is system module. Bot will not run without tihs module present

import { Snowflake } from 'discord.js';

import { ConfigModule } from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';

export class LoggerModuleGlobal{
	static async getConfig(botId: Snowflake | undefined, guildId: Snowflake | null): Promise<ConfigModule | undefined> {
		if (!botId || !guildId) return;
		const config: ConfigModule | undefined = await ConfigHelper.getModuleOptions(botId, guildId, "logger");
		if (config && config.enabled)
			return config;
		return undefined;
	}
}
// this is system module. Bot will not run without tihs module present

import {
	Guild, GuildAuditLogs, GuildAuditLogsEntry, GuildChannel, GuildEmoji, Integration, Invite,
	Message, MessageOptions, Role, Snowflake, StageInstance, Sticker, TextChannel, ThreadChannel,
	User, Webhook
} from 'discord.js';

import { ConfigLoggerModule, ConfigModule } from '../../../types/configs';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';
import { LoggerModuleGlobal } from './global';

export class LoggerModuleGuildHandler{

	static async logGuildUpdate(zakkBotClient: ZakkBot, oldGuild: Guild, newGuild: Guild): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, oldGuild.id);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.guildUpdate.enabled) {
			let dataString: string = "";
			if (oldGuild.name !== newGuild.name) dataString = `Updated name: ${oldGuild.name} =>  ${newGuild.name}\n`;
			if (oldGuild.nameAcronym !== newGuild.nameAcronym) dataString += `Updated acronym: ${oldGuild.nameAcronym} =>  ${newGuild.nameAcronym}\n`;
			if (oldGuild.description !== newGuild.description) dataString += `Updated description: ${oldGuild.description} =>  ${newGuild.description}\n`;
			if (oldGuild.rulesChannel?.name !== newGuild.rulesChannel?.name) dataString += `Updated rulesChannel: ${oldGuild.rulesChannel?.name} =>  ${newGuild.rulesChannel?.name}\n`;
			if (oldGuild.systemChannel?.name !== newGuild.systemChannel?.name) dataString += `Updated systemChannel: ${oldGuild.systemChannel?.name} =>  ${newGuild.systemChannel?.name}\n`;
			if (oldGuild.afkChannel !== newGuild.afkChannel) dataString += `Updated AFK: ${oldGuild.afkChannel?.name} =>  ${newGuild.afkChannel?.name}\n`;
			if (oldGuild.afkTimeout !== newGuild.afkTimeout) dataString += `Updated AFK timeout: ${oldGuild.afkTimeout} =>  ${newGuild.afkTimeout}\n`;
			if (oldGuild.banner !== newGuild.banner) dataString += `Updated banner: ${oldGuild.banner} =>  ${newGuild.banner}\n`;
			if (oldGuild.discoverySplash !== newGuild.discoverySplash) dataString += `Updated discovery: ${oldGuild.discoverySplash} =>  ${newGuild.discoverySplash}\n`;
			if (oldGuild.icon !== newGuild.icon) dataString += `Updated limit: ${oldGuild.icon} =>  ${newGuild.icon}\n`;
			if (oldGuild.mfaLevel !== newGuild.mfaLevel) dataString += `Updated MFA: ${oldGuild.mfaLevel} =>  ${newGuild.mfaLevel}\n`;
			if (oldGuild.nsfwLevel !== newGuild.nsfwLevel) dataString += `Updated nsfw: ${oldGuild.nsfwLevel} =>  ${newGuild.nsfwLevel}\n`;
			if (oldGuild.ownerId !== newGuild.ownerId) dataString += `Updated owner: ${oldGuild.ownerId} =>  ${newGuild.ownerId}\n`;
			if (oldGuild.premiumTier !== newGuild.premiumTier) dataString += `Updated premium: ${oldGuild.premiumTier} =>  ${newGuild.premiumTier}\n`;
			if (oldGuild.verificationLevel !== newGuild.verificationLevel) dataString += `Updated verification level: ${oldGuild.verificationLevel} =>  ${newGuild.verificationLevel}\n`;
			if (oldGuild.verified !== newGuild.verified) dataString += `Updated verified: ${oldGuild.verified} =>  ${newGuild.verified}\n`;
			if (oldGuild.partnered !== newGuild.partnered) dataString += `Updated partnered: ${oldGuild.partnered} =>  ${newGuild.partnered}\n`;

			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Updated channel <#${newGuild.id}>`,
					fields: [
						{name: "Data", value: dataString}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await newGuild.fetchAuditLogs({
				limit: 1,
				type: 'GUILD_UPDATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();
			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof Guild && target.id === newGuild.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.guildUpdate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, newGuild.channels.cache.first() as TextChannel);
		}
	}
}
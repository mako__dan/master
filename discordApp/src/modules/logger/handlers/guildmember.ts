// this is system module. Bot will not run without tihs module present

import {
	Guild, GuildAuditLogs, GuildAuditLogsEntry, GuildChannel, GuildEmoji, GuildMember, Integration,
	Invite, Message, MessageOptions, Role, Snowflake, StageInstance, Sticker, TextChannel,
	ThreadChannel, User, Webhook
} from 'discord.js';

import { ConfigLoggerModule, ConfigModule } from '../../../types/configs';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';
import { LoggerModuleGlobal } from './global';

export class LoggerModuleGuildmemberHandler{

	static async logGuildmemberUpdate(zakkBotClient: ZakkBot, oldMember: GuildMember, newMember: GuildMember): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, oldMember.guild.id);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.guildMemberUpdate.enabled) {
			let dataString: string = "";
			if (oldMember.displayName !== newMember.displayName) dataString = `Updated name: ${oldMember.displayName} =>  ${newMember.displayName}\n`;
			if (oldMember.nickname !== newMember.nickname) dataString += `Updated nickname: ${oldMember.nickname} =>  ${newMember.nickname}\n`;
			if (oldMember.pending !== newMember.pending) dataString += `Updated pending: ${oldMember.pending} =>  ${newMember.pending}\n`;
			if (oldMember.roles.cache.toJSON() !== newMember.roles.cache.toJSON()) dataString += `Updated roles: ${oldMember.roles.cache.toJSON().toString()} =>  ${newMember.roles.cache.toJSON().toString()}\n`;
			if (oldMember.roles.valueOf() !== newMember.roles.valueOf()) dataString += `Updated roles: ${oldMember.roles.valueOf()} =>  ${newMember.roles.valueOf()}\n`;


			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Updated channel <#${newMember.id}>`,
					fields: [
						{name: "Data", value: dataString}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await newMember.guild.fetchAuditLogs({
				limit: 1,
				type: 'MEMBER_UPDATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();
			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof GuildChannel && target.id === newMember.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}

			MessageHelper.Send({sendToChannel: moduleConfig.data.events.guildMemberUpdate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, newMember.guild.channels.cache.first() as TextChannel);
		}
	}
}
// this is system module. Bot will not run without tihs module present

import {
	Guild, GuildAuditLogs, GuildAuditLogsEntry, GuildChannel, GuildEmoji, Integration, Invite,
	Message, MessageOptions, Role, Snowflake, StageChannel, StageInstance, Sticker, TextChannel,
	ThreadChannel, User, Webhook
} from 'discord.js';

import { ConfigLoggerModule, ConfigModule } from '../../../types/configs';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';
import { LoggerModuleGlobal } from './global';

export class LoggerModuleStageHandler{

	static async logStageCreate(zakkBotClient: ZakkBot, stage: StageInstance): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, stage.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.stageInstanceCreate.enabled && stage.channel) {
			let dataString: string = `Topic: ${stage.topic} \n`;
			dataString += `Type: Stage instance \n`;

			// TODO overrides

			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Created stage instance <#${stage.id}>`,
					fields: [
						{name: "Data", value: dataString}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await stage.channel.guild.fetchAuditLogs({
				limit: 1,
				type: 'STAGE_INSTANCE_CREATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof StageChannel && target.id === stage.id && executorAvatar && embed.embeds &&  embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}
			const channel: GuildChannel | ThreadChannel | undefined = stage.channel;
			if (channel)
				MessageHelper.Send({sendToChannel: moduleConfig.data.events.stageInstanceCreate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, channel as TextChannel);
		}
	}

	static async logStageDelete(zakkBotClient: ZakkBot, stage: StageInstance): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, stage.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.stageInstanceDelete.enabled && stage.channel) {
			let dataString: string = `Topic: ${stage.topic} \n`;
			dataString += `Type: Stage instance \n`;

			// TODO overrides

			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Deleted stage instance <#${stage.id}>`,
					fields: [
						{name: "Data", value: dataString}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await stage.channel.guild.fetchAuditLogs({
				limit: 1,
				type: 'STAGE_INSTANCE_DELETE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();

			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof StageChannel && target.id === stage.id && executorAvatar && embed.embeds && embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}
			const channel: GuildChannel | ThreadChannel | undefined = stage.channel;
			if (channel)
				MessageHelper.Send({sendToChannel: moduleConfig.data.events.stageInstanceDelete.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, channel as TextChannel);
		}
	}

	static async logStageUpdate(zakkBotClient: ZakkBot, oldStage: StageInstance, newStage: StageInstance): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await LoggerModuleGlobal.getConfig(zakkBotClient.discordClient.user?.id, oldStage.guildId);
		if (moduleConfig && moduleConfig.enabled && (moduleConfig.data as ConfigLoggerModule).events.stageInstanceUpdate.enabled && newStage.channel && oldStage.channel) {
			let dataString: string = "";
			if (oldStage.topic !== newStage.topic) dataString += `Topic: ${oldStage.topic} => ${newStage.topic} \n`;

			// TODO overrides

			moduleConfig.data = moduleConfig.data as ConfigLoggerModule;
			const embed: MessageOptions = {
				embeds: [{
					author: {
						name: "Unknown",
						iconURL: "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png"
					},
					description: `Updated stage instance <#${newStage.id}>`,
					fields: [
						{name: "Data", value: dataString}
					],
					footer: {
						iconURL: zakkBotClient.discordClient.user?.avatarURL() ?? "https://discord.com/assets/6f26ddd1bf59740c536d2274bb834a05.png",
						text: zakkBotClient.discordClient.user?.username
					}
				}]
			};

			const fetchedLogs: GuildAuditLogs = await oldStage.channel.guild.fetchAuditLogs({
				limit: 1,
				type: 'STAGE_INSTANCE_UPDATE',
			});
			const deletionLog: GuildAuditLogsEntry | undefined = fetchedLogs.entries.first();
			if (deletionLog) {
				const { executor, target }: {executor: User | null , target: Guild | GuildChannel | User | Role | GuildEmoji | Invite | Webhook | Message | Integration | StageInstance | Sticker | ThreadChannel | { id: Snowflake } | null} = deletionLog;
				const executorAvatar: string | null | undefined = executor?.avatarURL();
				if (target instanceof StageChannel && target.id === newStage.id && executorAvatar && embed.embeds &&  embed.embeds[0].author?.iconURL) {
					embed.embeds[0].author.iconURL = executorAvatar;
					embed.embeds[0].author.name = executor?.tag;
				}

			}
			const channel: GuildChannel | ThreadChannel | undefined = oldStage.channel;
			if (channel)
				MessageHelper.Send({sendToChannel: moduleConfig.data.events.stageInstanceUpdate.channel ?? moduleConfig.data.defaultChannel, sendToOrigin: false}, embed, channel as TextChannel);
		}
	}
}
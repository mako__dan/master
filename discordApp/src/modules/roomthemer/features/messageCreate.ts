import { DMChannel, Message } from 'discord.js';

import { FeatureBase } from '../../../bases/featureBase';
import { FeatureInterface } from '../../../bases/featureInterface';
import { ConfigModule, ConfigRoomthemerModule } from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { ZakkBot } from '../../../zakkBot';

export class RoomThemerMessageCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "messageCreate/roomthemer";
	disabled: boolean = false;
	once:boolean = false;
	async run(zakkBotClient: ZakkBot, message: Message): Promise<void> {
		if (message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return;

		const moduleConfig: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, message.channel.guild.id, "roomthemer");
		if (!moduleConfig || !moduleConfig.enabled || !((moduleConfig.data as ConfigRoomthemerModule).channels.includes(message.channel.id))) return;
		moduleConfig.data = moduleConfig.data as ConfigRoomthemerModule;

		if (message.attachments.size > 0 || message.content.includes("http://") || message.content.includes("https://")){
			if (moduleConfig.data.autoThread.enabled) {
				// if (moduleConfig.data.autoThread.customThreads[message.channel.id]){
				// 	await message.startThread(moduleConfig.data.autoThread.customThreads[message.channel.id]);
				// } else {
					await message.startThread(moduleConfig.data.autoThread.defaultThread);
				// }
			}
		} else {
			if (!message.system) {
				message.delete();
			}
		}
	}
}
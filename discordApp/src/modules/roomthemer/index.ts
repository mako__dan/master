import { ModuleBase } from '../../bases/moduleBase';
import { ModuleInterface } from '../../bases/moduleInterface';
import { ModuleLoader } from '../../util/moduleLoader';
import { RoomThemerMessageCreateFeature } from './features/messageCreate';

export class RoomthemerModule extends ModuleBase implements ModuleInterface {
	public moduleName: string = "roomthemer";

	async initFeatures(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadFeature(new RoomThemerMessageCreateFeature())
		]);
	}
}
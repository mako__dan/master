import { Guild, GuildMember, Role, Snowflake } from 'discord.js';

import { ModuleBase } from '../../bases/moduleBase';
import { ModuleInterface } from '../../bases/moduleInterface';
import {
	ConfigModerationModule, ConfigModule, PenalisationData, PenalisationEnum
} from '../../types/configs';
import { ConfigHelper } from '../../util/helpers/configHelper';
import { ModuleLoader } from '../../util/moduleLoader';
import { ZakkBot } from '../../zakkBot';
import { BanCommand } from './commands/ban';
import { ClearCommand } from './commands/clear';
import { MuteCommand } from './commands/mute';
import { SbanCommand } from './commands/sban';
import { TempbanCommand } from './commands/tempban';
import { TempmuteCommand } from './commands/tempmute';
import { UnbanCommand } from './commands/unban';
import { UnmuteCommand } from './commands/unmute';
import { SentryMemberAddFeature } from './sentry/memberAdd';

export class ModerationModule extends ModuleBase implements ModuleInterface {
	public moduleName: string = "moderation";

	async initCommands(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadCommand(this.moduleName, new BanCommand()),
			ModuleLoader.loadCommand(this.moduleName, new SbanCommand()),
			ModuleLoader.loadCommand(this.moduleName, new TempbanCommand()),
			ModuleLoader.loadCommand(this.moduleName, new UnbanCommand()),

			ModuleLoader.loadCommand(this.moduleName, new MuteCommand()),
			ModuleLoader.loadCommand(this.moduleName, new TempmuteCommand()),
			ModuleLoader.loadCommand(this.moduleName, new UnmuteCommand()),

			ModuleLoader.loadCommand(this.moduleName, new BanCommand()),

			ModuleLoader.loadCommand(this.moduleName, new ClearCommand())
		]);
	}

	async initCustom(zakkBotClient: ZakkBot): Promise<void[]> {
		return Promise.all([
			this.createThreadTimeouts(zakkBotClient)
		]);
	}

	async initFeatures(): Promise<void[]> {
		return Promise.all([
			ModuleLoader.loadFeature(new SentryMemberAddFeature())
		]);
	}

	async createThreadTimeouts(zakkBotClient: ZakkBot): Promise<void> {
		const penalizationsData: PenalisationData[] | undefined = await ConfigHelper.listPenalisations();
		if (penalizationsData) {
			for(const penalization of penalizationsData) {
				let remainingTime: number = penalization.endsIn - Date.now();
				if (remainingTime < 0) {
					remainingTime = 10;
				}
				this.removePenalisation(zakkBotClient, penalization, remainingTime);
			}
		}
	}

	async removePenalisation(zakkBotClient: ZakkBot, penalisation: PenalisationData, remainingTime: number): Promise<void> {
		setTimeout(async (): Promise<void> => {



			const guildOfMember: Guild = await zakkBotClient.discordClient.guilds.fetch(penalisation.guildId);
			const memberData: GuildMember = await guildOfMember.members.fetch(penalisation.guildMemberId);

			if (penalisation.penalisationType === PenalisationEnum.MUTE){
				const moduleConfig: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, penalisation.guildId, "moderation");
				if (!moduleConfig) return;
				moduleConfig.data = moduleConfig.data as ConfigModerationModule;
				const muteRoles: string[] = moduleConfig.data.muteroles;
				muteRoles.forEach(async (roleId: Snowflake): Promise<void> => {
					const role: Role | null = await guildOfMember.roles.fetch(roleId);
					if (role) memberData.roles.remove(role);
				});
			}
			if (penalisation.penalisationType === PenalisationEnum.BAN){
				guildOfMember.members.unban(penalisation.guildMemberId);
			}
			ConfigHelper.deletePenalisation(penalisation);




		}, remainingTime);
	}
}
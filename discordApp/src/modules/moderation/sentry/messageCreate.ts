import { Guild, GuildMember, Message, Role, Snowflake } from 'discord.js';

import { FeatureBase } from '../../../bases/featureBase';
import { FeatureInterface } from '../../../bases/featureInterface';
import { ConfigModerationModule, ConfigModule } from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { RoleHelper } from '../../../util/helpers/roleHelper';
import { Logger } from '../../../util/logger';
import { ZakkBot } from '../../../zakkBot';

type MessagesData = {
	sentryMode: boolean,
	messageHistory: Map<Snowflake, string>;
};

export class SentryMessageCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "messageCreate";
	disabled: boolean =  false;
	once: boolean = false;

	static recentMessages: Record<Snowflake, MessagesData> = {};

	async run(zakkBotClient: ZakkBot, message: Message): Promise<void> {
		if (!message.guild) return;

		const moduleConfig: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, message.guild.id, "moderation");
		if (!moduleConfig || !moduleConfig.enabled || !(moduleConfig.data as ConfigModerationModule)) return;
		moduleConfig.data = moduleConfig.data as ConfigModerationModule;
		if (!SentryMessageCreateFeature.recentMessages[message.guild.id]) {
			SentryMessageCreateFeature.recentMessages[message.guild.id] = {sentryMode: false, messageHistory: new Map<Snowflake, string>()};
		}

		SentryMessageCreateFeature.recentMessages[message.guild.id].messageHistory.set(message.author.id, message.id);
		setTimeout(() => {
			if (message.guild?.id) {
				SentryMessageCreateFeature.recentMessages[message.guild.id].messageHistory.delete(message.id);
				if (SentryMessageCreateFeature.recentMessages[message.guild.id].messageHistory.size < (moduleConfig.data as ConfigModerationModule).sentry.limit) {
					SentryMessageCreateFeature.recentMessages[message.guild.id].sentryMode = false;
				}
			}
		}, moduleConfig.data.sentry.time);
		if (SentryMessageCreateFeature.recentMessages[message.guild.id].sentryMode === true) {
			this.penalize(message.id, moduleConfig.data, message.guild);
		}
		if (SentryMessageCreateFeature.recentMessages[message.guild.id].messageHistory.size >= moduleConfig.data.sentry.limit && !SentryMessageCreateFeature.recentMessages[message.guild.id].sentryMode) {
			SentryMessageCreateFeature.recentMessages[message.guild.id].sentryMode = true;
			for (const member of SentryMessageCreateFeature.recentMessages[message.guild.id].messageHistory) {
				this.penalize(member[0], moduleConfig.data as ConfigModerationModule, message.guild);
			}
		}
	}

	async penalize(id: Snowflake, moduleConfig: ConfigModerationModule, guild: Guild): Promise<void> {
		const member: GuildMember = await guild.members.fetch(id);
		switch(moduleConfig.sentry.action) {
			case "mute":
				RoleHelper.applyRoles(member, moduleConfig.muteroles);
				break;
			case "sban":
				RoleHelper.applyRoles(member, moduleConfig.sbanroles);
				break;
		}
	}
}
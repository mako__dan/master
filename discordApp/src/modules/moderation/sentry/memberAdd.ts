import { Guild, GuildMember, Role, Snowflake } from 'discord.js';

import { FeatureBase } from '../../../bases/featureBase';
import { FeatureInterface } from '../../../bases/featureInterface';
import { ConfigModerationModule, ConfigModule } from '../../../types/configs';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { Logger } from '../../../util/logger';
import { ZakkBot } from '../../../zakkBot';

type GuildJoinData = {
	sentryMode: boolean,
	joinedmembers: Map<string, string>;
};

export class SentryMemberAddFeature extends FeatureBase implements FeatureInterface {
	name: string = "guildMemberAdd/moderation";
	disabled: boolean = false;
	once: boolean = false;

	static recentlyJoined: Record<Snowflake, GuildJoinData> = {};

	async run(zakkBotClient: ZakkBot, memberData: GuildMember): Promise<void> {
		const moduleConfig: ConfigModule | undefined = await ConfigHelper.getModuleOptions(zakkBotClient.clientConfig.id, memberData.guild.id, "moderation");
		if (!moduleConfig || !moduleConfig.enabled || !(moduleConfig.data as ConfigModerationModule)) return;
		moduleConfig.data = moduleConfig.data as ConfigModerationModule;
		if (!SentryMemberAddFeature.recentlyJoined[memberData.guild.id]) {
			SentryMemberAddFeature.recentlyJoined[memberData.guild.id] = {sentryMode: false, joinedmembers: new Map<string, string>()};
		}

		SentryMemberAddFeature.recentlyJoined[memberData.guild.id].joinedmembers.set(memberData.id, memberData.id);
		setTimeout(() => {
			SentryMemberAddFeature.recentlyJoined[memberData.guild.id].joinedmembers.delete(memberData.id);
			if (SentryMemberAddFeature.recentlyJoined[memberData.guild.id].joinedmembers.size < (moduleConfig.data as ConfigModerationModule).sentry.limit) {
				SentryMemberAddFeature.recentlyJoined[memberData.guild.id].sentryMode = false;
			}
		}, moduleConfig.data.sentry.time);
		if (SentryMemberAddFeature.recentlyJoined[memberData.guild.id].sentryMode === true) {
			this.penalize(memberData.id, moduleConfig.data, memberData.guild);
		}
		if (SentryMemberAddFeature.recentlyJoined[memberData.guild.id].joinedmembers.size >= moduleConfig.data.sentry.limit && !SentryMemberAddFeature.recentlyJoined[memberData.guild.id].sentryMode) {
			SentryMemberAddFeature.recentlyJoined[memberData.guild.id].sentryMode = true;
			SentryMemberAddFeature.recentlyJoined[memberData.guild.id].joinedmembers.forEach((member: Snowflake) => {
				this.penalize(member, moduleConfig.data as ConfigModerationModule, memberData.guild);
			});
		}
	}

	async penalize(id: Snowflake, moduleConfig: ConfigModerationModule, guild: Guild): Promise<void> {
		const member: GuildMember = await guild.members.fetch(id);
		switch(moduleConfig.sentry.action) {
			case "mute":
				moduleConfig.muteroles.forEach((roleId: Snowflake): void => {
					const role: Role | undefined = member.guild.roles.cache.find((roleToFind: Role): boolean => roleToFind.id === roleId);
					if (role) member.roles.add(role).catch(Logger.error);
				});
				break;
			case "sban":
				moduleConfig.sbanroles.forEach((roleId: Snowflake): void => {
					const role: Role | undefined = member.guild.roles.cache.find((roleToFind: Role): boolean => roleToFind.id === roleId);
					if (role) member.roles.add(role).catch(Logger.error);
				});
				break;
		}
	}
}
import {
	ApplicationCommandOption, Collection, CommandInteraction, DMChannel, GuildMember, Message,
	NewsChannel, PartialDMChannel, Role, TextChannel, ThreadChannel
} from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { Logger } from '../../../util/logger';

export class ClearCommand extends CommandBase implements CommandInterface{
	name: string = 'clearmessages';
	description: string = 'Vyčistí chat';
	help: string = 'ban';
	slashCommandParameters: ApplicationCommandOption[] = [
		{
			type: "NUMBER",
			name: "count",
			description: "Počet zpráv ke smazání (zakkBotClient.clientConfig.id pro všechny)",
			required: true
		},
		{
			type: "USER",
			name: "member",
			description: "Vyfiltruje pouze deného membera",
			required: false
		},
		{
			type: "ROLE",
			name: "role",
			description: "Vyfiltruje pouze denou roli",
			required: false
		},
	];

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch ): Promise<ExecuteResponseType> {
		if (message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return {success: false};

		if (!(args.length === 1 || args.length === 2)) {
			return {success: false, message: "musíš zadat počet zpráv"};
		}

		if (!Number(args[0])){
			return {success: false, message: "Počet zpráv musí být menší než 100"};
		}
		const countOption: number = Number(args[0]);
		if (countOption > 100 || countOption <= 0){
			return {success: false, message: "Počet zpráv musí být menší než 100"};
		}

		let roleOption: Role | undefined = message.mentions.roles.first();
		let memberOption: GuildMember | undefined = message.mentions.members?.first();

		if (!roleOption && !memberOption) {
			if (!isNaN(Number(args[1]))){
				if (message.channel.guild.members.cache.get(args[1])) {
					memberOption = message.channel.guild.members.cache.get(args[1]);
				}
				if (message.channel.guild.roles.cache.get(args[1])) {
					roleOption = message.channel.guild.roles.cache.get(args[1]);
				}
			}
		}

		if (memberOption){
			let lastMessageId: string | undefined;
			const messagesToDelete: Message[] = [];
			let run: boolean = true;
			while (messagesToDelete.length !== countOption && run){
				const messages: Collection<string, Message> = await message.channel.messages.fetch(lastMessageId ? {limit: countOption < 50 ? countOption+20 : 100, before: lastMessageId} : {limit:countOption < 50 ? countOption+20 : 100});
				for (const messagen of messages) {
					if(messagen[1].createdTimestamp < Number(new Date()) - 1123200000) {
						run = false;
						break;
					}
					if (messagesToDelete.length > countOption){
						run = false;
						break;
					}

					if(messagen[1].author.id === memberOption.id) {
						messagesToDelete.push(messagen[1]);
					}
					lastMessageId = messagen[1]?.id;
				}
			}
			await message.channel.bulkDelete(messagesToDelete).catch(Logger.error);
		} else if (roleOption) {
			let lastMessageId: string | undefined;
			const messagesToDelete: Message[] = [];
			let run: boolean = true;
			while (messagesToDelete.length !== countOption && run){
				const messages: Collection<string, Message> = await message.channel.messages.fetch(lastMessageId ? {limit: countOption < 50 ? countOption+20 : 100, before: lastMessageId} : {limit:countOption < 50 ? countOption+20 : 100});
				for (const messagen of messages) {
					if(messagen[1].createdTimestamp < Number(new Date()) - 1123200000) {
						run = false;
						break;
					}
					if (messagesToDelete.length > countOption){
						run = false;
						break;
					}
					if(message.channel.guild.members.cache.get(messagen[1].author.id)?.roles.cache.has(roleOption.id)) {
						messagesToDelete.push(messagen[1]);
					}
					lastMessageId = messagen[1]?.id;
				}
			}
			await message.channel.bulkDelete(messagesToDelete).catch(Logger.error);
		} else {
			const messages: Collection<string, Message> = await message.channel.messages.fetch({limit: countOption});
			const firstMessage: PartialDMChannel | DMChannel | TextChannel | NewsChannel | ThreadChannel | undefined = messages.first()?.channel;
			if (firstMessage?.isText() && !firstMessage.partial && !(firstMessage instanceof DMChannel)) {
				firstMessage.bulkDelete(messages).catch(Logger.error);
			}
		}
		return {success: true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch): Promise<ExecuteResponseType> {
		if (!interaction.guild || !interaction.channel || interaction.channel.partial === true || interaction.channel instanceof DMChannel) return {success:false};

		const countOption: number = interaction.options.getNumber("count", true);
		const memberOption: GuildMember | null = interaction.options.getMember("member", false) as GuildMember | null;
		const roleOption: Role | null = interaction.options.getRole("role", false) as Role | null;

		if (countOption > 100 || countOption <= 0){
			return {success: false, message: "Počet zpráv musí být menší než 100"};
		}

		if (memberOption){
			let lastMessageId: string | undefined;
			const messagesToDelete: Message[] = [];
			let run: boolean = true;
			while (messagesToDelete.length !== countOption && run){
				const messages: Collection<string, Message> = await interaction.channel.messages.fetch(lastMessageId ? {limit: countOption < 50 ? countOption+20 : 100, before: lastMessageId} : {limit:countOption < 50 ? countOption+20 : 100});
				for (const message of messages) {
					if(message[1].createdTimestamp < Number(new Date()) - 1123200000) {
						run = false;
						break;
					}
					if (messagesToDelete.length > countOption){
						run = false;
						break;
					}

					if(message[1].author.id === memberOption.id) {
						messagesToDelete.push(message[1]);
					}
					lastMessageId = message[1]?.id;
				}
			}
			await interaction.channel.bulkDelete(messagesToDelete).catch(Logger.error);
		} else if (roleOption) {
			let lastMessageId: string | undefined;
			const messagesToDelete: Message[] = [];
			let run: boolean = true;
			while (messagesToDelete.length !== countOption && run){
				const messages: Collection<string, Message> = await interaction.channel.messages.fetch(lastMessageId ? {limit: countOption < 50 ? countOption+20 : 100, before: lastMessageId} : {limit:countOption < 50 ? countOption+20 : 100});
				for (const message of messages) {
					if(message[1].createdTimestamp < Number(new Date()) - 1123200000) {
						run = false;
						break;
					}
					if (messagesToDelete.length > countOption){
						run = false;
						break;
					}
					if(interaction.channel.guild.members.cache.get(message[1].author.id)?.roles.cache.has(roleOption.id)) {
						messagesToDelete.push(message[1]);
					}
					lastMessageId = message[1]?.id;
				}
			}
			await interaction.channel.bulkDelete(messagesToDelete).catch(Logger.error);
		} else {
			const messages: Collection<string, Message> = await interaction.channel.messages.fetch({limit: countOption});
			const firstMessage: PartialDMChannel | DMChannel | TextChannel | NewsChannel | ThreadChannel | undefined = messages.first()?.channel;
			if (firstMessage?.isText() && !firstMessage.partial && !(firstMessage instanceof DMChannel)) {
				firstMessage.bulkDelete(messages).catch(Logger.error);
			}
		}
		return {success: true};
	}
}
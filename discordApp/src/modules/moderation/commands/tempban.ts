import {
	ApplicationCommandOption, CommandInteraction, DMChannel, Guild, GuildMember, Message,
	MessageOptions, Snowflake, User
} from 'discord.js';
import ms from 'ms';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import {
	ConfigBatch, ConfigModerationModule, PenalisationData, PenalisationEnum
} from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';

export class TempbanCommand extends CommandBase implements CommandInterface{
	name: string = 'tempban';
	description: string = 'Umlčí člena';
	help: string = 'tempban';
	slashCommandParameters: ApplicationCommandOption[] = [
		{
			type: "USER",
			name: "member",
			description: "člen k dočasnému zabanování",
			required: true
		},
		{
			type: "STRING",
			name: "length",
			description: "délka umlčení",
			required: true
		},
		{
			type: "STRING",
			name: "reason",
			description: "reason",
			required: false
		}
	];

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if (!message.guild || message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return {success: false};

		const user: User | undefined = message.mentions.users.first();
		let userToBan: Snowflake | undefined =  user?.id;
		if (!userToBan) {
			if (!isNaN(Number(args[0]))){
				userToBan = args[0];
			} else {
				return {success:false, message:"Missing member reference."};
			}
		}

		if (!args[1]) return {success:false, message:"Missing time"};
		const time: number = ms(args[1]);
		if (!time) return {success:false, message:"Invalid time format"};
		args.shift();
		message.channel.guild.members.ban(userToBan, {reason: args.join(" ")});
		this.waitForUnban(userToBan, time, message.channel.guild);

		const penaltyAuthor: GuildMember = await message.guild.members.fetch(message.author.id);
		const penaltyMember: GuildMember = await message.guild.members.fetch(userToBan);
		configBatch.module.data = configBatch.module.data as ConfigModerationModule;
		const embedMessage: MessageOptions = MessageHelper.ReplaceWildcardsInBuilder(configBatch.module.data.penalizations.tempban.message , {
			channel: message.channel,
			guild: message.guild,
			member: penaltyMember,
			botUser: (await (await zakkBotClient.discordClient.guilds.fetch( message.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076")),
			author: penaltyAuthor,
			penalty: {time: (Date.now() + time).toString(), reason: args.length > 0 ? args.join(" "): "Undefined"}
		});
		MessageHelper.Send(configBatch.command, embedMessage, message.channel);

		ConfigHelper.addInfraction({
			id:  penaltyMember.guild.id + "/" + penaltyMember.id,
			guildId: penaltyMember.guild.id,
			memberId: penaltyMember.id,
			penalisationType: PenalisationEnum.BAN,
			penalized: Date.now(),
			endsIn: Date.now()+time,
			reason: args.length > 0 ? args.join(" "): "Undefined"
		});
		return {success:true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if (!interaction.guild || !interaction.channel) return {success:false};

		const memberOption: GuildMember = interaction.options.getMember("member", true) as GuildMember;
		const timeOption: string = interaction.options.getString("length", true);
		const reason: string | null = interaction.options.getString("reason", false);

		const time: number = ms(timeOption);
		if (!time) return {success:false, message:"Invalid time format"};

		memberOption.ban({reason: reason ?? ""});
		this.waitForUnban(memberOption.id, time, interaction.guild);

		const penaltyAuthor: GuildMember = await interaction.guild.members.fetch(interaction.user.id);
		configBatch.module.data = configBatch.module.data as ConfigModerationModule;
		const embedMessage: MessageOptions = MessageHelper.ReplaceWildcardsInBuilder(configBatch.module.data.penalizations.tempban.message , {
			channel: !interaction.channel.partial ? interaction.channel : undefined,
			guild: interaction.guild,
			member: memberOption,
			botUser: (await (await zakkBotClient.discordClient.guilds.fetch( interaction.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076")),
			author: penaltyAuthor,
			penalty: {time: (Date.now() + time).toString(), reason: reason ? reason : "Undefined"}

		});
		MessageHelper.Send(configBatch.command, embedMessage, interaction.channel);

		ConfigHelper.addInfraction({
			id:  memberOption.guild.id + "/" + memberOption.id,
			guildId: memberOption.guild.id,
			memberId: memberOption.id,
			penalisationType: PenalisationEnum.BAN,
			penalized: Date.now(),
			endsIn: Date.now()+time,
			reason: reason ? reason : "Undefined"
		});
		return {success: true};
	}

	async waitForUnban(user: Snowflake, time: number, guild: Guild): Promise<void> {
		let check: PenalisationData | undefined = await ConfigHelper.getPenalisation(guild.id, user, PenalisationEnum.BAN);
		check = JSON.parse(JSON.stringify(check));
		if (check) {
			check.endsIn = Date.now() + time;
			ConfigHelper.updatePenalisation(check);
		} else {
			check = {
				guildId: guild.id,
				guildMemberId: user,
				penalisationType: PenalisationEnum.BAN,
				endsIn: Date.now() + time
			};
			ConfigHelper.addPenalisation(check);
		}
		setTimeout((): void => {
			guild.members.unban(user);
			if (check) {
				ConfigHelper.deletePenalisation(check);
			}
		}, time);
	}
}
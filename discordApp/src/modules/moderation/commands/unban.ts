import {
	ApplicationCommandOption, CommandInteraction, DMChannel, GuildMember, Message, MessageOptions,
	Snowflake, User
} from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch, ConfigModerationModule, PenalisationEnum } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';

export class UnbanCommand extends CommandBase implements CommandInterface{
	name: string = 'unban';
	description: string = 'Smaže ban člena';
	help: string = 'unban';
	slashCommandParameters: ApplicationCommandOption[] = [
		{
			type: "STRING",
			name: "memberid",
			description: "člen k odbanování",
			required: true
		}
	];

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if (!message.guild || message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return {success: false};

		const user: User | undefined = message.mentions.users.first();
		let userToBan: Snowflake | undefined =  user?.id;
		if (!userToBan) {
			if (!isNaN(Number(args[0]))){
				userToBan = args[0];
			} else {
				return {success:false, message:"Missing member reference."};
			}
		}
		message.channel.guild.members.unban(userToBan);

		const penaltyAuthor: GuildMember = await message.guild.members.fetch(message.author.id);
		const penaltyMember: GuildMember = await message.guild.members.fetch(userToBan);
		configBatch.module.data = configBatch.module.data as ConfigModerationModule;
		const embedMessage: MessageOptions = MessageHelper.ReplaceWildcardsInBuilder(configBatch.module.data.penalizations.mute.message , {
			channel: message.channel,
			guild: message.guild,
			member: penaltyMember,
			botUser: (await (await zakkBotClient.discordClient.guilds.fetch( message.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076")),
			author: penaltyAuthor
		});
		MessageHelper.Send(configBatch.command, embedMessage, message.channel);

		ConfigHelper.addInfraction({
			id:  penaltyMember.guild.id + "/" + penaltyMember.id,
			guildId: penaltyMember.guild.id,
			memberId: penaltyMember.id,
			penalisationType: PenalisationEnum.BAN,
			penalized: 0,
			endsIn: Date.now(),
			reason: ""
		});
		return {success:true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if (!interaction.guild || !interaction.channel) return {success:false};

		const memberOption: string = interaction.options.getString("memberid", true);

		interaction.guild.members.unban(memberOption);

		const penaltyAuthor: GuildMember = await interaction.guild.members.fetch(interaction.user.id);
		configBatch.module.data = configBatch.module.data as ConfigModerationModule;
		const embedMessage: MessageOptions = MessageHelper.ReplaceWildcardsInBuilder(configBatch.module.data.penalizations.mute.message , {
			channel: !interaction.channel.partial ? interaction.channel : undefined,
			guild: interaction.guild,
			member: await interaction.guild.members.fetch(memberOption)?? undefined,
			botUser: (await (await zakkBotClient.discordClient.guilds.fetch( interaction.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076")),
			author: penaltyAuthor

		});
		MessageHelper.Send(configBatch.command, embedMessage, interaction.channel);

		ConfigHelper.addInfraction({
			id:  interaction.guild.id + "/" + memberOption,
			guildId: interaction.guild.id,
			memberId: memberOption,
			penalisationType: PenalisationEnum.MUTE,
			penalized: 0,
			endsIn: Date.now(),
			reason: ""
		});
		return {success:true};
	}
}
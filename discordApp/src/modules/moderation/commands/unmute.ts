import {
	ApplicationCommandOption, CommandInteraction, DMChannel, GuildMember, Message, MessageOptions,
	Role, Snowflake, User
} from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch, ConfigModerationModule, PenalisationEnum } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { ZakkBot } from '../../../zakkBot';

export class UnmuteCommand extends CommandBase implements CommandInterface{
	name: string = 'unmute';
	description: string = 'Umožní členovi opět psát';
	help: string = 'unmute';
	slashCommandParameters: ApplicationCommandOption[] = [
		{
			type: "USER",
			name: "member",
			description: "člen k odumlčení",
			required: true
		}
	];

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if (!message.guild || message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return {success: false};

		configBatch.module.data = configBatch.module.data as ConfigModerationModule;

		const user: User | undefined = message.mentions.users.first();
		let userToMute: Snowflake | undefined =  user?.id;
		if (!userToMute) {
			if (!isNaN(Number(args[0]))){
				userToMute = args[0];
			} else {
				return {success:false, message:"Missing member reference."};
			}
		}

		const penaltyMember: GuildMember =  await message.channel.guild.members.fetch(userToMute);

		const penaltyAuthor: GuildMember = await message.guild.members.fetch(message.author.id);
		configBatch.module.data = configBatch.module.data as ConfigModerationModule;
		const embedMessage: MessageOptions = MessageHelper.ReplaceWildcardsInBuilder(configBatch.module.data.penalizations.mute.message , {
			channel: message.channel,
			guild: message.guild,
			member: penaltyMember,
			botUser: (await (await zakkBotClient.discordClient.guilds.fetch( message.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076")),
			author: penaltyAuthor
		});
		MessageHelper.Send(configBatch.command, embedMessage, message.channel);

		this.applyRoles(penaltyMember, configBatch.module.data);

		ConfigHelper.addInfraction({
			id:  penaltyMember.guild.id + "/" + penaltyMember.id,
			guildId: penaltyMember.guild.id,
			memberId: penaltyMember.id,
			penalisationType: PenalisationEnum.BAN,
			penalized: 0,
			endsIn: Date.now(),
			reason: ""
		});
		return {success:true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if (!interaction.guild || !interaction.channel) return {success:false};

		const memberOption: GuildMember = interaction.options.getMember("member", true) as GuildMember;

		configBatch.module.data = configBatch.module.data as ConfigModerationModule;

		this.applyRoles(memberOption, configBatch.module.data);

		const penaltyAuthor: GuildMember = await interaction.guild.members.fetch(interaction.user.id);
		configBatch.module.data = configBatch.module.data as ConfigModerationModule;
		const embedMessage: MessageOptions = MessageHelper.ReplaceWildcardsInBuilder(configBatch.module.data.penalizations.mute.message , {
			channel: !interaction.channel.partial ? interaction.channel : undefined,
			guild: interaction.guild,
			member: await interaction.guild.members.fetch(memberOption)?? undefined,
			botUser: (await (await zakkBotClient.discordClient.guilds.fetch( interaction.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076")),
			author: penaltyAuthor

		});
		MessageHelper.Send(configBatch.command, embedMessage, interaction.channel);

		ConfigHelper.addInfraction({
			id:  memberOption.guild.id + "/" + memberOption.id,
			guildId: memberOption.guild.id,
			memberId: memberOption.id,
			penalisationType: PenalisationEnum.MUTE,
			penalized: 0,
			endsIn: Date.now(),
			reason: ""
		});
		return {success:true};
	}

	async applyRoles(user: GuildMember, moduleConfig: ConfigModerationModule): Promise<void> {
		moduleConfig.muteroles.forEach((roleId: Snowflake): void => {
			const role: Role | undefined = user.guild.roles.cache.find((roleToFind: Role): boolean => roleToFind.id === roleId);
			if (role) user.roles.remove(role);
		});
	}
}
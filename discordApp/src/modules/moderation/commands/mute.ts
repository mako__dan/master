import {
	ApplicationCommandOption, CommandInteraction, DMChannel, GuildMember, Message, MessageOptions,
	Role, Snowflake, User
} from 'discord.js';

import { CommandBase } from '../../../bases/commandBase';
import { CommandInterface } from '../../../bases/commandInterface';
import { ConfigBatch, ConfigModerationModule, PenalisationEnum } from '../../../types/configs';
import { ExecuteResponseType } from '../../../types/types';
import { ConfigHelper } from '../../../util/helpers/configHelper';
import { MessageHelper } from '../../../util/helpers/messageHelper';
import { Logger } from '../../../util/logger';
import { ZakkBot } from '../../../zakkBot';

export class MuteCommand extends CommandBase implements CommandInterface{
	name: string = 'mute';
	description: string = 'Umlčí člena';
	help: string = 'mute';
	slashCommandParameters: ApplicationCommandOption[] = [
		{
			type: "USER",
			name: "member",
			description: "člen k umlčení",
			required: true
		},
		{
			type: "STRING",
			name: "reason",
			description: "reason",
			required: false
		}
	];

	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		if (!message.guild || message.system || message.author.bot || message.channel instanceof DMChannel || message.channel.partial) return {success: false};

		configBatch.module.data = configBatch.module.data as ConfigModerationModule;

		const user: User | undefined = message.mentions.users.first();
		let userToMute: Snowflake | undefined =  user?.id;
		if (!userToMute) {
			if (!isNaN(Number(args[0]))){
				userToMute = args[0];
			} else {
				return {success:false, message:"Missing member reference."};
			}
		}
		const member: GuildMember =  await message.channel.guild.members.fetch(userToMute);
		this.applyRoles(member, configBatch.module.data);

		const penaltyAuthor: GuildMember = await message.guild.members.fetch(message.author.id);
		const penaltyMember: GuildMember = await message.guild.members.fetch(userToMute);
		configBatch.module.data = configBatch.module.data as ConfigModerationModule;
		args.shift();
		const embedMessage: MessageOptions = MessageHelper.ReplaceWildcardsInBuilder(configBatch.module.data.penalizations.mute.message , {
			channel: message.channel,
			guild: message.guild,
			member: penaltyMember,
			botUser: (await (await zakkBotClient.discordClient.guilds.fetch( message.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076")),
			author: penaltyAuthor,
			penalty: {reason: args.length > 0 ? args.join(" "): "Undefined"}
		});
		MessageHelper.Send(configBatch.command, embedMessage, message.channel);

		ConfigHelper.addInfraction({
			id:  penaltyMember.guild.id + "/" + penaltyMember.id,
			guildId: penaltyMember.guild.id,
			memberId: penaltyMember.id,
			penalisationType: PenalisationEnum.MUTE,
			penalized: Date.now(),
			endsIn: 0,
			reason: args.length > 0 ? args.join(" "): "Undefined"
		});
		return {success:true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType> {
		if (!interaction.guild || !interaction.channel) return {success:false};

		const memberOption: GuildMember = interaction.options.getMember("member", true) as GuildMember;

		configBatch.module.data = configBatch.module.data as ConfigModerationModule;

		this.applyRoles(memberOption, configBatch.module.data);

		const reason: string | null | null = interaction.options.getString("reason", false);
		const penaltyAuthor: GuildMember = await interaction.guild.members.fetch(interaction.user.id);
		configBatch.module.data = configBatch.module.data as ConfigModerationModule;
		const embedMessage: MessageOptions = MessageHelper.ReplaceWildcardsInBuilder(configBatch.module.data.penalizations.mute.message , {
			channel: !interaction.channel.partial ? interaction.channel : undefined,
			guild: interaction.guild,
			member: memberOption,
			botUser: (await (await zakkBotClient.discordClient.guilds.fetch( interaction.guild.id)).members.fetch(zakkBotClient.discordClient.user?.id ?? "795374069805613076")),
			author: penaltyAuthor,
			penalty: {reason: reason ? reason : "Undefined"}

		});
		MessageHelper.Send(configBatch.command, embedMessage, interaction.channel);

		ConfigHelper.addInfraction({
			id:  memberOption.guild.id + "/" + memberOption.id,
			guildId: memberOption.guild.id,
			memberId: memberOption.id,
			penalisationType: PenalisationEnum.MUTE,
			penalized: Date.now(),
			endsIn: 0,
			reason: reason ? reason : "Undefined"
		});
		return {success:true};
	}

	async applyRoles(user: GuildMember, moduleConfig: ConfigModerationModule): Promise<void> {
		moduleConfig.muteroles.forEach((roleId: Snowflake): void => {
			const role: Role | undefined = user.guild.roles.cache.find((roleToFind: Role): boolean => roleToFind.id === roleId);
			if (role) user.roles.add(role).catch(Logger.error);
		});
	}
}
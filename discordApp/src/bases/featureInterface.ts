import {
	Collection, Guild, GuildBan, GuildChannel, GuildEmoji, GuildMember, Interaction, Invite,
	Message, MessageReaction, Presence, Role, Snowflake, StageInstance, Sticker, TextBasedChannels,
	User, VoiceState
} from 'discord.js';

import { ZakkBot } from '../zakkBot';

export interface FeatureInterface{
	name: string;
	disabled: boolean;
	once: boolean;

	run(zakkBotClient: ZakkBot, ...args: (
		Date |
		Guild |
		GuildChannel |
		GuildEmoji |
		GuildMember |
		Interaction |
		GuildBan |
		Message  |
		MessageReaction |
		Presence |
		TextBasedChannels |
		User |
		VoiceState |
		Sticker |
		StageInstance |
		Role |
		Invite |
		Collection<Snowflake, Message>
		)[] ): Promise<void>;
}
import { ApplicationCommandOption, CommandInteraction, Message } from 'discord.js';

import { ConfigBatch } from '../types/configs';
import { ExecuteResponseType } from '../types/types';
import { ZakkBot } from '../zakkBot';

export interface CommandInterface{
	name: string;
	description: string;
	help: string;
	slashCommandParameters?: ApplicationCommandOption[];

	execute(message: Message, args: string[], text: string, commandConfig: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType>;
	executeInteraction(interaction: CommandInteraction, commandConfig: ConfigBatch, zakkBotClient: ZakkBot): Promise<ExecuteResponseType>;
}
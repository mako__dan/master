import { CommandInteraction, Message } from 'discord.js';

import { ConfigBatch } from '../types/configs';
import { ExecuteResponseType } from '../types/types';
import { ZakkBot } from '../zakkBot';

export class CommandBase{
	async execute(message: Message, args: string[], text: string, configBatch: ConfigBatch, zakkBotClient: ZakkBot  ): Promise<ExecuteResponseType> {
		return {success:true};
	}

	async executeInteraction(interaction: CommandInteraction, configBatch: ConfigBatch, zakkBotClient: ZakkBot ): Promise<ExecuteResponseType> {
		return {success:true};
	}
}
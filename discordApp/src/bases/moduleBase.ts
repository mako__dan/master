/* tslint:disable:no-empty */
import { ZakkBot } from '../zakkBot';

export class ModuleBase{
	async init(zakkBotClient: ZakkBot): Promise<void[][]>{
		return Promise.all([
			this.initCommands(),
			this.initFeatures(),
			this.initInteractions(),
			this.initCustom(zakkBotClient)
		]);
	}
	async initCommands(): Promise<void[]> {
		return Promise.all([]);
	}
	async initFeatures(): Promise<void[]> {
		return Promise.all([]);
	}
	async initInteractions(): Promise<void[]> {
		return Promise.all([]);
	}
	async initCustom(zakkBotClient: ZakkBot): Promise<void[]> {
		return Promise.all([]);
	}
}
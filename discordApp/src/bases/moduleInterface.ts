import { ZakkBot } from '../zakkBot';

export interface ModuleInterface{
	moduleName: string;

	init(zakkBotClient: ZakkBot): Promise<void[][]>;
	initCustom(zakkBotClient: ZakkBot): Promise<void[]>;
	initCommands(): Promise<void[]>;
	initFeatures(): Promise<void[]>;
	initInteractions(): Promise<void[]>;
}
import { Sticker } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleStickerHandler } from '../modules/logger/handlers/sticker';
import { ZakkBot } from '../zakkBot';

export class StickerCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "stickerCreate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, sticker: Sticker): Promise<void> {
		LoggerModuleStickerHandler.logStickerCreate(zakkBotClient, sticker);
	}
}
import { Message } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleMessageHandler } from '../modules/logger/handlers/message';
import { ZakkBot } from '../zakkBot';

export class MessageUpdateFeature extends FeatureBase implements FeatureInterface{
	name: string = "messageUpdate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, oldMessage: Message, newMessage: Message): Promise<void> {
		// Ignore direct messages
		if (!oldMessage.guild) return;

		LoggerModuleMessageHandler.logMessageUpdate(zakkBotClient, oldMessage, newMessage);
	}
}
import { Message } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { CommandHandler } from '../util/handlers/commandHandler';
import { ZakkBot } from '../zakkBot';

export class MessageCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "messageCreate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, message: Message): Promise<void> {
		// Ignore direct messages
		if (!message.guild) return;

		CommandHandler.handleCommand(message, zakkBotClient);
	}
}
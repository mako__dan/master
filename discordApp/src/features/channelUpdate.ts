import { GuildChannel } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleChannelHandler } from '../modules/logger/handlers/channel';
import { ZakkBot } from '../zakkBot';

export class ChannelUpdateFeature extends FeatureBase implements FeatureInterface{
	name: string = "channelUpdate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, oldChannel: GuildChannel, newChannel: GuildChannel): Promise<void> {
		LoggerModuleChannelHandler.logChannelUpdate(zakkBotClient, oldChannel, newChannel);
	}
}
import { Invite } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleInviteHandler } from '../modules/logger/handlers/invite';
import { ZakkBot } from '../zakkBot';

export class InviteCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "inviteCreate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, invite: Invite): Promise<void> {
		LoggerModuleInviteHandler.logInviteCreate(zakkBotClient, invite);
	}
}
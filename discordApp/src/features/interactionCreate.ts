import { CommandInteraction } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { InteractionHandler } from '../util/handlers/interactionHandler';
import { ZakkBot } from '../zakkBot';

export class InteractionCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "interactionCreate";
	disabled: boolean = false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, interaction: CommandInteraction): Promise<void> {
		InteractionHandler.handleInteraction(zakkBotClient, interaction);
	}
}
import { Role } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleRoleHandler } from '../modules/logger/handlers/role';
import { ZakkBot } from '../zakkBot';

export class RoleCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "roleCreate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, role: Role): Promise<void> {
		LoggerModuleRoleHandler.logRoleCreate(zakkBotClient, role);
	}
}

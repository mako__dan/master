import { GuildEmoji } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleEmojiHandler } from '../modules/logger/handlers/emoji';
import { ZakkBot } from '../zakkBot';

export class EmojiDeleteFeature extends FeatureBase implements FeatureInterface{
	name: string = "emojiDelete";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, emoji: GuildEmoji): Promise<void> {
		LoggerModuleEmojiHandler.logEmojiDelete(zakkBotClient, emoji);
	}
}
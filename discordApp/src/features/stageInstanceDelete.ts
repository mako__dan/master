import { StageInstance } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleStageHandler } from '../modules/logger/handlers/stage';
import { ZakkBot } from '../zakkBot';

export class StageInstanceDeleteFeature extends FeatureBase implements FeatureInterface{
	name: string = "stageInstanceDelete";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, stageInstance: StageInstance): Promise<void> {
		LoggerModuleStageHandler.logStageDelete(zakkBotClient, stageInstance);
	}
}
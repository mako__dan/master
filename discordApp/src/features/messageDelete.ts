import { Message } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleMessageHandler } from '../modules/logger/handlers/message';
import { ZakkBot } from '../zakkBot';

export class MessageDeleteFeature extends FeatureBase implements FeatureInterface{
	name: string = "messageDelete";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, message: Message): Promise<void> {
		// Ignore direct messages
		if (!message.guild) return;

		LoggerModuleMessageHandler.logMessageDelete(zakkBotClient, message);
	}
}
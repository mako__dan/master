import { Collection, Message, Snowflake } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleMessageHandler } from '../modules/logger/handlers/message';
import { ZakkBot } from '../zakkBot';

export class MessageDeleteBulkFeature extends FeatureBase implements FeatureInterface{
	name: string = "messageDeleteBulk";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, messages: Collection<Snowflake, Message>): Promise<void> {
		messages.forEach((message: Message) => {
			if (!message.guild) return;
			LoggerModuleMessageHandler.logMessageDelete(zakkBotClient, message);
		});
	}
}
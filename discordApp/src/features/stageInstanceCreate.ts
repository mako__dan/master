import { StageInstance } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleStageHandler } from '../modules/logger/handlers/stage';
import { ZakkBot } from '../zakkBot';

export class StageInstanceCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "stageInstanceCreate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, stageInstance: StageInstance): Promise<void> {
		LoggerModuleStageHandler.logStageCreate(zakkBotClient, stageInstance);
	}
}
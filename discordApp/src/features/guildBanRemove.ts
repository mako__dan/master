import { GuildBan } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleGuildbanHandler } from '../modules/logger/handlers/guildban';
import { ZakkBot } from '../zakkBot';

export class GuildBanRemoveFeature extends FeatureBase implements FeatureInterface{
	name: string = "guildBanRemove";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, ban: GuildBan): Promise<void> {
		LoggerModuleGuildbanHandler.logGuildbanDelete(zakkBotClient, ban);
	}
}
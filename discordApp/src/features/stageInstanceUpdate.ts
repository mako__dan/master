import { StageInstance } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleStageHandler } from '../modules/logger/handlers/stage';
import { ZakkBot } from '../zakkBot';

export class StageInstanceUpdateFeature extends FeatureBase implements FeatureInterface{
	name: string = "stageInstanceUpdate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, oldStageInstance: StageInstance, newStageInstance: StageInstance): Promise<void> {
		LoggerModuleStageHandler.logStageUpdate(zakkBotClient, oldStageInstance, newStageInstance);
	}
}
import { Guild } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { ModuleBase } from '../bases/moduleBase';
import { CommandModel } from '../schemas/command';
import { GuildModel } from '../schemas/guild';
import { ModuleModel } from '../schemas/module';
import { ConfigCommandObject, ConfigModule, ConfigTypes } from '../types/configs';
import { ZakkBot } from '../zakkBot';

export class GuildCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "guildCreate";
	disabled: boolean = false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, guild: Guild): Promise<void> {
		const defaultConfig: {modules: ConfigModule[], commands:ConfigCommandObject[]} = {
			modules: [
				{
					id: zakkBotClient.clientConfig.id + "/" + guild.id + "/verify",
					enabled: false,
					data: {
						channel: "",
						emotes: {
							yes: "✅",
							yesDisplay: "✅",
							undecided: "❓",
							undecidedDisplay: "❓",
							no: "❎",
							noDisplay: "❎",
							forceYes: "🟢",
							forceYesDisplay: "🟢",
							forceNo: "🔴",
							forceNoDisplay: "🔴",
							reset: "🔁",
							resetDisplay: "🔁"
						},
						verifiedRole: "",
						points: {
							yes: 1,
							undecided: 0,
							no: -1
						},
						groups: [],
						neededPoints: 2,
						authority: [],
						respondRoleWhitelist: [],
						respondRoleBlacklist: []
					}
				}, // verify
				{
					id: zakkBotClient.clientConfig.id + "/" + guild.id + "/statusRole",
					enabled: false,
					data: {
						groups: []
					}
				}, // statusRole
				{
					id: zakkBotClient.clientConfig.id + "/" + guild.id + "/welcomer",
					enabled: true,
					data: {
						sendToChannel: "",
						joinMessage: {
							embeds: [{
								title: "**Welome ** {user}",
								color: "#00FF00",
								fields: [{
									name: "Account created:",
									value: "{user.created}"
								}, {
									name: "Members:",
									value: "{server.members}"
								}, {
									name: "Ping",
									value: "{user.mention}"
								}],
								author: {
									name: "{bot}",
									url: "",
									iconURL: "{user.mention}"
								},
								thumbnail: {
									url: "{user.avatar}"
								},
								footer: {
									text: "{datetime}"
								}
							}]
						},
						leaveMessage: {
							embeds: [{
								title: "**Goodbye ** {user}",
								color: "#FF0000",
								fields: [{
									name: "Members:",
									value: "{server.members}"
								}, {
									name: "Ping",
									value: "{user.mention}"
								}],
								author: {
									name: "{bot}",
									url: "",
									iconURL: "{user.mention}"
								},
								thumbnail: {
									url: "{user.avatar}"
								},
								footer: {
									text: "{datetime}"
								}
							}]
						}
					},
				}, // welcomer
				{
					id: zakkBotClient.clientConfig.id + "/" + guild.id + "/autorole",
					enabled: false,
					data: {
						roles: []
					}
				}, // autorole
				{
					id: zakkBotClient.clientConfig.id + "/" + guild.id + "/roomthemer",
					enabled: false,
					data: {
						channels: [],
						autoThread: {
							enabled: true,
							defaultThread: {
								name: "Add comment to this post",
								autoArchiveDuration: 1440
							},
							customThreads: {}
						}
					},
				}, // roomthemer
				{
					id: zakkBotClient.clientConfig.id + "/" + guild.id + "/stickymessage",
					enabled: false,
					data: {
						messages: []
					},
				}, // stickymessage
				{
					id: zakkBotClient.clientConfig.id + "/" + guild.id + "/autoreply",
					enabled: false,
					data: {
						replies: []
					},
				}, // autoreply
				{
					id: zakkBotClient.clientConfig.id + "/" + guild.id + "/moderation",
					enabled: true,
					data: {
						sbanroles: [],
						muteroles: [],
						penalizations: {
							ban: {
								channel: "",
								message: {
									embeds: [{
										title: "{user.nickname} has been banned!",
										description: "{penalty.reason}",
										color: "#FF0000",
										author: {
											name: "{bot.nickname}",
											iconURL: "{bot.avatar}"
										},
										thumbnail: {
											url: ""
										},
										footer: {
											text: "Banned by {author.nickname}",
											iconURL: "{author.avatar}"
										}
									}]
								}
							},
							tempban: {
								channel: "",
								message: {
									embeds: [{
										title: "{user.nickname} has been banned until {penalty.time}!",
										description: "{penalty.reason}",
										color: "#FF0000",
										author: {
											name: "{bot.nickname}",
											iconURL: "{bot.avatar}"
										},
										thumbnail: {
											url: ""
										},
										footer: {
											text: "Banned by {author.nickname}",
											iconURL: "{author.avatar}"
										}
									}]
								}
							},
							mute: {
								channel: "",
								message: {
									embeds: [{
										title: "{user.nickname} has been muted",
										description: "{penalty.reason}",
										color: "#FFA500",
										author: {
											name: "{bot.nickname}",
											iconURL: "{bot.avatar}"
										},
										thumbnail: {
											url: ""
										},
										footer: {
											text: "Muted by {author.nickname}",
											iconURL: "{author.avatar}"
										}
									}]
								}
							},
							tempmute: {
								channel: "",
								message: {
									embeds: [{
										title: "{user.nickname} has been muted until {penalty.time}!",
										description: "{penalty.reason}",
										color: "#FFA500",
										author: {
											name: "{bot.nickname}",
											iconURL: "{bot.avatar}"
										},
										thumbnail: {
											url: ""
										},
										footer: {
											text: "Muted by {author.nickname}",
											iconURL: "{author.avatar}"
										}
									}]
								}
							},
							sban: {
								channel: "",
								message: {
									embeds: [{
										title: "{user.nickname} has been banned!",
										description: "{penalty.reason}",
										color: "#FFA500",
										author: {
											name: "{bot.nickname}",
											iconURL: "{bot.avatar}"
										},
										thumbnail: {
											url: ""
										},
										footer: {
											text: "Banned by {author.nickname}",
											iconURL: "{author.avatar}"
										}
									}]
								}
							},
							unban: {
								channel: "",
								message: {
									embeds: [{
										title: "{user.nickname} has been unbanned!",
										description: "",
										color: "#00FF00",
										author: {
											name: "{bot.nickname}",
											iconURL: "{bot.avatar}"
										},
										thumbnail: {
											url: ""
										},
										footer: {
											text: "Unbanned by {author.nickname}",
											iconURL: "{author.avatar}"
										}
									}]
								}
							},
							unmute: {
								channel: "",
								message: {
									embeds: [{
										title: "{user.nickname} has been unmuted!",
										description: "",
										color: "#00FF00",
										author: {
											name: "{bot.nickname}",
											iconURL: "{bot.avatar}"
										},
										thumbnail: {
											url: ""
										},
										footer: {
											text: "Unmuted by {author.nickname}",
											iconURL: "{author.avatar}"
										}
									}]
								}
							}
						},
						sentry: {
							enabled: false,
							action: "sban",
							time: 5000,
							limit: 5
						}
					},

				}, // modetarion
				{
					id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player",
					enabled: true,
					data: {
						autoplay: {
							enabled: false,
							returnEmptyQueue: true,
							startOnMemberJoin: true,
							autoclearWhenPlayCommand: true,
							autoShuffle: true,
							voiceRoom: "",
							djRoom: "",
							playlist: ""
						}
					},
				}, // player
				{
					id: zakkBotClient.clientConfig.id + "/" + guild.id + "/embedsender",
					enabled: true,
					data: {
						embeds: []
					},
				}, // embedSender
				{
					id: zakkBotClient.clientConfig.id + "/" + guild.id + "/logger",
					enabled: true,
					data: {
						events: {
							channelCreate: {
								enabled: false,
								channel: ""
							},
							channelDelete: {
								enabled: false,
								channel: ""
							},
							channelUpdate: {
								enabled: false,
								channel: ""
							},
							emojiCreate: {
								enabled: false,
								channel: ""
							},
							emojiDelete: {
								enabled: false,
								channel: ""
							},
							emojiUpdate: {
								enabled: false,
								channel: ""
							},
							guildBanAdd: {
								enabled: false,
								channel: ""
							},
							guildBanRemove: {
								enabled: false,
								channel: ""
							},
							guildMemberAdd: {
								enabled: false,
								channel: ""
							},
							guildMemberRemove: {
								enabled: false,
								channel: ""
							},
							guildMemberUpdate: {
								enabled: false,
								channel: ""
							},
							guildUpdate: {
								enabled: false,
								channel: ""
							},
							messageDelete: {
								enabled: false,
								channel: ""
							},
							messageUpdate: {
								enabled: false,
								channel: ""
							},
							roleCreate: {
								enabled: false,
								channel: ""
							},
							roleDelete: {
								enabled: false,
								channel: ""
							},
							roleUpdate: {
								enabled: false,
								channel: ""
							},
							voiceStateUpdate: {
								enabled: false,
								channel: ""
							},
							inviteCreate: {
								enabled: false,
								channel: ""
							},
							inviteDelete: {
								enabled: false,
								channel: ""
							},
							stageInstanceCreate: {
								enabled: false,
								channel: ""
							},
							stageInstanceDelete: {
								enabled: false,
								channel: ""
							},
							stageInstanceUpdate: {
								enabled: false,
								channel: ""
							},
							stickerCreate: {
								enabled: false,
								channel: ""
							},
							stickerDelete: {
								enabled: false,
								channel: ""
							},
							stickerUpdate: {
								enabled: false,
								channel: ""
							},
							threadCreate: {
								enabled: false,
								channel: ""
							},
							threadDelete: {
								enabled: false,
								channel: ""
							},
							threadUpdate: {
								enabled: false,
								channel: ""
							}
						},
						defaultChannel: ""
					}
				} // logger
			],
			commands: [
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/embedsender/sendembed",
				enabled: true,
				prefixedPhrases: [
					"sendembed",
					"se",
					"embed"
				],
				slashCommand: {
					name: "sendembed",
					description: "Sends embed message as a bot"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: true,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/moderation/ban",
				enabled: true,
				prefixedPhrases: [
					"ban"
				],
				slashCommand: {
					name: "ban",
					description: "Bans member"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: true,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/moderation/unban",
				enabled: true,
				prefixedPhrases: [
					"unban"
				],
				slashCommand: {
					name: "unban",
					description: "Unbans a member"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: true,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/moderation/tempban",
				enabled: true,
				prefixedPhrases: [
					"tempban"
				],
				slashCommand: {
					name: "tempban",
					description: "Temporarily bans member"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: true,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/moderation/sban",
				enabled: true,
				prefixedPhrases: [
					"sban"
				],
				slashCommand: {
					name: "sban",
					description: "Softly bans a member"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: true,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/moderation/mute",
				enabled: true,
				prefixedPhrases: [
					"mute"
				],
				slashCommand: {
					name: "mute",
					description: "Mutes member"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: true,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/moderation/unmute",
				enabled: true,
				prefixedPhrases: [
					"unmute"
				],
				slashCommand: {
					name: "unmute",
					description: "Unmutes member"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: true,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/moderation/tempmute",
				enabled: true,
				prefixedPhrases: [
					"tempmute"
				],
				slashCommand: {
					name: "tempmute",
					description: "Temporarily mutes member"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: true,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/playtop",
				enabled: true,
				prefixedPhrases: [
					"playtop"
				],
				slashCommand: {
					name: "playtop",
					description: "Puts the song to the top of queue"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/queue",
				enabled: true,
				prefixedPhrases: [
					"queue"
				],
				slashCommand: {
					name: "queue",
					description: "Shows the current queue"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/leave",
				enabled: true,
				prefixedPhrases: [
					"leave"
				],
				slashCommand: {
					name: "leave",
					description: "Disconnects the bot from voice channel"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/pause",
				enabled: true,
				prefixedPhrases: [
					"pause"
				],
				slashCommand: {
					name: "pause",
					description: "Pause the music"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/resume",
				enabled: true,
				prefixedPhrases: [
					"resume"
				],
				slashCommand: {
					name: "resume",
					description: "resumes the music"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/autorole/applyroles",
				enabled: true,
				prefixedPhrases: [
					"applyroles",
					"ar"
				],
				slashCommand: {
					name: "applyroles",
					description: "Apply role to member"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: true,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/playnow",
				enabled: true,
				prefixedPhrases: [
					"playnow"
				],
				slashCommand: {
					name: "playnow",
					description: "Plays song immedietly"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/play",
				enabled: true,
				prefixedPhrases: [
					"play"
				],
				slashCommand: {
					name: "play",
					description: "Plays song"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/skip",
				enabled: true,
				prefixedPhrases: [
					"skip"
				],
				slashCommand: {
					name: "skip",
					description: "Skips song to the nex in queue"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/stickymessage/stickymessage",
				enabled: true,
				prefixedPhrases: [
					"stickymessage",
					"sticky",
					"sm"
				],
				slashCommand: {
					name: "stickymessage",
					description: "Glues the message to the end of channel"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: true,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/shuffle",
				enabled: true,
				prefixedPhrases: [
					"shuffle"
				],
				slashCommand: {
					name: "shuffle",
					description: "Shuffles queue"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/clear",
				enabled: true,
				prefixedPhrases: [
					"clear"
				],
				slashCommand: {
					name: "clear",
					description: "Clears queue"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/remove",
				enabled: true,
				prefixedPhrases: [
					"remove"
				],
				slashCommand: {
					name: "remove",
					description: "Removes song from queue"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/removedupes",
				enabled: true,
				prefixedPhrases: [
					"removedupes"
				],
				slashCommand: {
					name: "removedupes",
					description: "Removes duplicit songs (by name)"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/player/lyrics",
				enabled: true,
				prefixedPhrases: [
					"lyrics"
				],
				slashCommand: {
					name: "lyrics",
					description: "Tries to find lyrics"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: false,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				},
				{
				id: zakkBotClient.clientConfig.id + "/" + guild.id + "/moderation/clearmessages",
				enabled: true,
				prefixedPhrases: [
					"clearmessages"
				],
				slashCommand: {
					name: "clearmessages",
					description: "Clears messages"
				},
				phrases: [],
				respondChannelWhitelist: [],
				respondChannelBlacklist: [],
				respondRoleWhitelist: [],
				respondRoleBlacklist: [],
				removeSelf: false,
				removeTrigger: true,
				removeInvalidTrigger: true,
				removeInvalidSelf: 10,
				sendToChannel: "",
				sendToOrigin: true
				}
			]
		};

		GuildModel.create({
			id: zakkBotClient.clientConfig.id + "/" + guild.id,
			name: guild.name,
			commander: guild.channels.cache.first()?.id ?? "",
			removeUnknown: true,
			themeColor: "#123456",
			prefix: "€",
			argsDivider: " ",
			removeInvalidTrigger: true,
			removeInvalidSelf: 10,
			guildId: guild.id
		});
		defaultConfig.modules.forEach((module: ConfigModule): void => {
			ModuleModel.create(module);
		});
		defaultConfig.commands.forEach((command: ConfigCommandObject) => {
			CommandModel.create(command);
		});
	}
}
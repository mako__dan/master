import { VoiceState } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleVoicestateHandler } from '../modules/logger/handlers/voicestate';
import { ZakkBot } from '../zakkBot';

export class VoiceStateUpdateFeature extends FeatureBase implements FeatureInterface{
	name: string = "voiceStateUpdate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, oldVoiceState: VoiceState, newVoiceState: VoiceState): Promise<void> {
		LoggerModuleVoicestateHandler.logVoicestateUpdate(zakkBotClient, oldVoiceState, newVoiceState);
	}
}
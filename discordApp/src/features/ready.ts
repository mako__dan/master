import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { ZakkBot } from '../zakkBot';

export class ReadyFeature extends FeatureBase implements FeatureInterface{
	name: string = "ready";
	disabled: boolean =  false;
	once: boolean = true;
	async run(zakkBotClient: ZakkBot): Promise<void> {
		this.manageStatus(zakkBotClient);
	}
	async manageStatus(zakkBotClient: ZakkBot): Promise<void> {
		if (zakkBotClient.clientConfig) {
			const length: number = zakkBotClient.clientConfig.activities.length;
			for (let i: number = 0;i < length;i++){
				zakkBotClient.discordClient?.user?.setActivity(zakkBotClient.clientConfig.activities[i].message, {type: zakkBotClient.clientConfig.activities[i].type});
				await new Promise((resolve: (value: PromiseLike<void>) => void): void => {
					setTimeout(resolve, zakkBotClient.clientConfig?.activities[i].time);
				});
				if (i === length -1){
					i = -1;
				}
			}
		}
	}
}
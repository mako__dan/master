import { ThreadChannel } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleThreadHandler } from '../modules/logger/handlers/thread';
import { ZakkBot } from '../zakkBot';

export class ThreadUpdateFeature extends FeatureBase implements FeatureInterface{
	name: string = "threadUpdate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, oldThreadChannel: ThreadChannel, newThreadChannel: ThreadChannel): Promise<void> {
		LoggerModuleThreadHandler.logThreadUpdate(zakkBotClient, oldThreadChannel, newThreadChannel);
	}
}
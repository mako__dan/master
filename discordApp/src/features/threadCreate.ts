import { ThreadChannel } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleThreadHandler } from '../modules/logger/handlers/thread';
import { ZakkBot } from '../zakkBot';

export class ThreadCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "threadCreate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, threadChannel: ThreadChannel): Promise<void> {
		LoggerModuleThreadHandler.logThreadCreate(zakkBotClient, threadChannel);
	}
}
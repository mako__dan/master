import { Role } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleRoleHandler } from '../modules/logger/handlers/role';
import { ZakkBot } from '../zakkBot';

export class RoleUpdateFeature extends FeatureBase implements FeatureInterface{
	name: string = "roleUpdate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, oldRole: Role, newRole: Role): Promise<void> {
		LoggerModuleRoleHandler.logRoleUpdate(zakkBotClient, oldRole, newRole);
	}
}

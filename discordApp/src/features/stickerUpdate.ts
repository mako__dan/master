import { Sticker } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleStickerHandler } from '../modules/logger/handlers/sticker';
import { ZakkBot } from '../zakkBot';

export class StickerUpdateFeature extends FeatureBase implements FeatureInterface{
	name: string = "stickerUpdate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, oldSticker: Sticker, newSticker: Sticker): Promise<void> {
		LoggerModuleStickerHandler.logStickerUpdate(zakkBotClient, oldSticker, newSticker);
	}
}
import { Guild } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleGuildHandler } from '../modules/logger/handlers/guild';
import { ZakkBot } from '../zakkBot';

export class GuildUpdateFeature extends FeatureBase implements FeatureInterface{
	name: string = "guildUpdate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, oldGuild: Guild, newGuild: Guild): Promise<void> {
		LoggerModuleGuildHandler.logGuildUpdate(zakkBotClient, oldGuild, newGuild);
	}
}
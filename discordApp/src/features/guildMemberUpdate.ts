import { GuildMember } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleGuildmemberHandler } from '../modules/logger/handlers/guildmember';
import { ZakkBot } from '../zakkBot';

export class GuildMemberUpdateFeature extends FeatureBase implements FeatureInterface{
	name: string = "guildMemberUpdate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, oldMember: GuildMember, newMember: GuildMember): Promise<void> {
		LoggerModuleGuildmemberHandler.logGuildmemberUpdate(zakkBotClient, oldMember, newMember);
	}
}
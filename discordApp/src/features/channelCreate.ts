import { GuildChannel } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleChannelHandler } from '../modules/logger/handlers/channel';
import { ZakkBot } from '../zakkBot';

export class ChannelCreateFeature extends FeatureBase implements FeatureInterface{
	name: string = "channelCreate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, channel: GuildChannel): Promise<void> {
		LoggerModuleChannelHandler.logChannelCreate(zakkBotClient, channel);
	}
}
import { Message, MessageReaction, TextChannel, User } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { ZakkBot } from '../zakkBot';

export class RawFeature extends FeatureBase implements FeatureInterface{
	name: string = "raw";
	disabled: boolean =  false;
	once: boolean = false;

	// tslint:disable-next-line:no-any
	async run(zakkBotClient: ZakkBot, packet: any): Promise<void> {
		switch(packet.t){
			case 'MESSAGE_REACTION_ADD': this.emitMessageReactionChange(zakkBotClient, packet);
			case 'MESSAGE_REACTION_REMOVE': this.emitMessageReactionChange(zakkBotClient, packet);
		}
	}

	// tslint:disable-next-line:no-any
	async emitMessageReactionChange(zakkBotClient: ZakkBot, packet: any): Promise<void> {
		const channel: TextChannel = await zakkBotClient.discordClient.channels.fetch(packet.d.channel_id) as TextChannel;
		if (channel.messages.cache.has(packet.d.message_id)) return;
		channel.messages.fetch(packet.d.message_id).then((message: Message): void => {
			const emoji: string = packet.d.emoji.id ? `${packet.d.emoji.name}:${packet.d.emoji.id}` : packet.d.emoji.name;
			const reaction: MessageReaction | undefined = message.reactions.cache.get(emoji);
			if (reaction) {
				const ReactionMember: User | undefined = zakkBotClient.discordClient.users.cache.get(packet.d.user_id);
				if (ReactionMember)
					reaction.users.cache.set(packet.d.user_id, ReactionMember);
			}
			// Check which type of event it is before emitting
			if (packet.t === 'MESSAGE_REACTION_ADD') {
				zakkBotClient.discordClient.emit('customMessageReactionAdd', reaction, zakkBotClient.discordClient.users.cache.get(packet.d.user_id));
			}
			if (packet.t === 'MESSAGE_REACTION_REMOVE') {
				zakkBotClient.discordClient.emit('customMessageReactionRemove', reaction, zakkBotClient.discordClient.users.cache.get(packet.d.user_id));
			}
		});
	}
}
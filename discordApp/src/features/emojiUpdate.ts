import { GuildEmoji } from 'discord.js';

import { FeatureBase } from '../bases/featureBase';
import { FeatureInterface } from '../bases/featureInterface';
import { LoggerModuleEmojiHandler } from '../modules/logger/handlers/emoji';
import { ZakkBot } from '../zakkBot';

export class EmojiUpdateFeature extends FeatureBase implements FeatureInterface{
	name: string = "emojiUpdate";
	disabled: boolean =  false;
	once: boolean = false;

	async run(zakkBotClient: ZakkBot, oldEmoji: GuildEmoji, newEmoji: GuildEmoji): Promise<void> {
		LoggerModuleEmojiHandler.logEmojiUpdate(zakkBotClient, oldEmoji, newEmoji);
	}
}
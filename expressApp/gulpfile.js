'use strict'

const gulp = require('gulp')
const gulpTS = require('gulp-typescript')
const gulpTSLint = require('gulp-tslint').default
const gulpSourcemaps = require('gulp-sourcemaps')
const gulpNodemon = require('gulp-nodemon')
const tsLint = require('tslint')
const del = require('del')
const path = require('path')

const project = gulpTS.createProject('tsconfig.json')
const typeCheck = tsLint.Linter.createProgram('tsconfig.json')

gulp.task('lint', () => {
    return gulp.src([
        './src/**/*ts',
    ])
        .pipe(gulpTSLint({
            configuration: 'tslint.json',
            formatter: 'verbose'
        }))
        .pipe(gulpTSLint.report({emitError: false}))
})

gulp.task('compile', () => {

    gulp.src('./src/**/*.json')
        .pipe(gulp.dest('./build/'))
    const tsCompile = gulp.src('./src/**/*.ts')
        .pipe(gulpSourcemaps.init())
        .pipe(project())
    return tsCompile.js
        .pipe(gulpSourcemaps.write({
            sourceRoot: file => path.relative(path.join(file.cwd, file.path),
            file.base)
        }))
        .pipe(gulp.dest('build/'))
})

gulp.task('watch', () => {
    gulp.watch('./src/**/*.ts', gulp.series('lint'))
})

gulp.task('start', () => {
    return gulpNodemon({
        script: './build/index.js',
        watch: './build/index.js'
    })
})

gulp.task('serve', () => {
    return gulpNodemon({
        "watch": ["src"],
        "ext": "ts,json",
        "ignore": ["src/**/*.spec.ts"],
        "exec": "ts-node ./src/index.ts"      // or "npx ts-node src/index.ts"
      })
});

exports.dev = gulp.series('lint', gulp.parallel('watch', 'serve'));
exports.build = gulp.series('lint', 'compile');
exports.server = gulp.series('serve');
exports.start = gulp.series('start');

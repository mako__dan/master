import { Schema, model, Model} from 'mongoose';
import { ClientConfig } from '../types/configs';

export const ClientShema: Schema<ClientConfig, Model<ClientConfig>, {}> = new Schema<ClientConfig>({
	token: {type: String, index: true, unique: true},
	activities: Schema.Types.Mixed,
	mainClusterId: Number,
	logicId: Number,
	availability: String,
	reservedFor: Array
});

export const ClientModel: Model<ClientConfig> = model("Client", ClientShema);
import { Schema, model, Model} from 'mongoose';
import { ConfigModule } from '../types/configs';

// tslint:disable:no-any
export const CommandShema: Schema<ConfigModule, Model<ConfigModule>, {}>  = new Schema<ConfigModule>({
	id: {type: String, index: true, unique: true},
	enabled: Boolean,
	data: Schema.Types.Mixed,
});

export const ModuleModel: Model<ConfigModule>  = model("Module", CommandShema);
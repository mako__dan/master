import { Schema, model, Model} from 'mongoose';
import { ConfigCommandObject } from '../types/configs';

export const CommandShema: Schema<ConfigCommandObject, Model<ConfigCommandObject>, {}> = new Schema<ConfigCommandObject>({
	id: {type: String, index: true, unique: true},

	enabled: Boolean,
	prefixedPhrases: [String],
	slashCommand: {
		name: String,
		description: String,
	},
	phrases: [],
	respondChannelWhitelist: [String],
	respondChannelBlacklist: [String],
	respondRoleWhitelist: [String],
	respondRoleBlacklist: [String],
	removeSelf: Boolean,
	removeTrigger: Boolean,
	removeInvalidTrigger: Boolean,
	removeInvalidSelf: Number,
	sendToChannel: String,
	sendToOrigin: Boolean
});

export const CommandModel: Model<ConfigCommandObject> = model("Command", CommandShema);
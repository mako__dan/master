import { Schema, model, Model} from 'mongoose';
import { PenalisationData } from '../types/configs';

// tslint:disable:no-any
export const PenalisationShema: Schema<PenalisationData, Model<PenalisationData>, {}> = new Schema<PenalisationData>({
	guildId: String,
	guildMemberId: String,
	penalisationType: String,
	endsIn: Number
});

export const PenalisationModel: Model<PenalisationData> = model("Penalisation", PenalisationShema);
import { Schema, model, Model } from 'mongoose';

// tslint:disable:no-any
export const userSchema: Schema<any, Model<any, any, any>, {}> = new Schema({
	discordId: {
		type: String,
		required: true,
		unique: true
	},
	discordTag: {
		type: String,
		required: true,
	},
	avatar: {
		type: String,
		required: true
	},
	guilds: {
		type: Array,
		required: true
	},
	isAdministrator: {
		type: Boolean,
		required: false
	}
});

export const userModel: Model<any, any, any> = model('user', userSchema);
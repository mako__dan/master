import passport from 'passport';
import { VerifyCallback } from 'passport-oauth2';
import passportDiscord from 'passport-discord';
import { userModel } from '../models/dbUser';
import { User } from '../types/custom';
import { AppConfig } from '../util/config';

export default {
	register(): void {
		passport.serializeUser((user: User, done: VerifyCallback): void => {
			done(null, user.discordId);
		});
		passport.deserializeUser(async (discordId: string, done: VerifyCallback): Promise<void> => {
			const user: User = await userModel.findOne({ discordId }) as Express.User;
			return user ? done(null, user) : done (null);
		});
		passport.use(
			new passportDiscord({
				clientID: AppConfig.clientId,
				clientSecret: AppConfig.clientSecret,
				callbackURL: AppConfig.clientCallback,
				scope: ['identify', 'email', 'guilds']
			}, async (accessToken: string, refreshToken: string, profile: passportDiscord.Profile, done: VerifyCallback): Promise<void> => {
				const {id, username, discriminator, avatar, guilds}: passportDiscord.Profile = profile;
				const findUser: User | undefined = await userModel.findOneAndUpdate({ discordId: id}, {
					discordTag: `${username}#${discriminator}`,
					avatar,
					guilds,
					isAdministrator: false
				}, {new: true}) as User | undefined;
				if (findUser) {
					return done(null, findUser);
				}
				const newUser: User = {
					discordId: id,
					discordTag: `${username}#${discriminator}`,
					avatar,
					guilds,
				};
				await userModel.create(newUser);
				return done(null, newUser);
			})
		);
	}
};

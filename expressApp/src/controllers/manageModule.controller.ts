import { Response, NextFunction } from 'express';
import { Request } from './../types/custom';
import { ConfigModule } from '../types/configs';
import { ModuleModel } from '../models/module';
import { SocketManager } from '../util/socket';

export class ManageModuleController {

	static check = (req: Request, res: Response, next: NextFunction): void => {
		res.send({status: 200, msg: "Command manager is up and running"});
	}

	static async get(req: Request, res: Response, next: NextFunction): Promise<void> {
		if (req.query?.guildid && req.query.modulename && req.query.botid) {
			res.send(await ModuleModel.findOne({id: req.query.botid + "/" + req.query.guildid + "/" + req.query.modulename}));
		} else
			res.status(409).send({status: 409, msg: 'Missing data, commandName, moduleName, guildid are required'});
	}

	static async list(req: Request, res: Response, next: NextFunction): Promise<void> {
		if (req.query?.guildid && req.query.botid) {
			res.send(await ModuleModel.find({id: RegExp(req.query.botid + "/" + req.query.guildid + "/")}));
		} else
			res.status(409).send({status: 409, msg: 'Missing data, commandName, moduleName, guildid are required'});
	}

	static async set(req: Request, res: Response, next: NextFunction): Promise<void> {
		if (req.query) {
			const updateResult: ConfigModule | null  = await ModuleModel.findOneAndUpdate({_id: req.query._id}, req.body);
			if (updateResult){
				SocketManager.sendData("reloadGuildData", {guildId: req.query?.guildid}, (response: string): void => {
					res.status(200).send({status: 200, msg: 'Successfully updated'});
				});
			} else {
				res.status(200).send({status: 400, msg: 'Error while updating'});
			}
		} else
			res.status(409).send({status: 409, msg: 'Missing data'});
	}

	static async toggle(req: Request, res: Response, next: NextFunction): Promise<void> {
		if (req.query?.guildid && req.query.systemid) {
			const queryResult: ConfigModule | null = await ModuleModel.findById(req.query.systemid);
			if (queryResult) {
				queryResult.enabled = !queryResult.enabled;
				const updateResult: ConfigModule | null = await ModuleModel.findByIdAndUpdate(req.query.systemid, {enabled: queryResult.enabled});
				if (updateResult){
					SocketManager.sendData("reloadGuildData", {guildId: req.query?.guildid}, (response: string): void => {
						res.status(200).send({status: 200, msg: 'Successfully updated'});
					});
				} else
					res.status(400).send({status: 400, msg: 'Error while updating'});
			} else
				res.status(400).send({status: 400, msg: 'Error while updating'});
		} else
			res.status(409).send({status: 409, msg: 'Missing data'});
	}
}
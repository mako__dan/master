import { Response, NextFunction } from 'express';
import { Request } from './../types/custom';
import { ClientModel } from '../models/client';
import { Snowflake } from 'discord.js';
import { ClientConfig } from '../types/configs';

export class ManageClientController {

	static check = (req: Request, res: Response, next: NextFunction): void => {
		res.send({status: 200, msg: "Client manager is up and running"});
	}


	static async get(req: Request, res: Response, next: NextFunction): Promise<void> {
		if (req.query?.clientid) {
			const data: ClientConfig | null = await ClientModel.findById(req.query?.clientid);
			if (data) {
				if (data.availability === "PUBLIC") {
					data.token = "";
					res.status(200).send(data);
					return;
				} else {
					const foundGuild: string | undefined = data.reservedFor.find((guildId: Snowflake): boolean => guildId === req.query?.guildid);
					if (foundGuild) {
						data.token = "";
						res.status(200).send(data);
					} else
						res.status(409).send({status: 409, msg: 'Missing data, commandName, moduleName, guildId are required'});
				}
			} else
				res.status(409).send({status: 409, msg: 'Missing data, commandName, moduleName, guildId are required'});
		} else
			res.status(409).send({status: 409, msg: 'Missing data, commandName, moduleName, guildId are required'});
	}

	static async list(req: Request, res: Response, next: NextFunction): Promise<void> {
		const allClients: ClientConfig[] = await ClientModel.find();
		const avalilableClients: ClientConfig[] = [];
		for(const client of allClients) {
			if (client.availability === "PUBLIC") {
				client.token = "";
				avalilableClients.push(client);
			} else {
				const foundGuild: Snowflake | undefined = client.reservedFor.find((guildId: Snowflake): boolean => guildId === req.query?.guildid);
				if (foundGuild) {
					client.token = "";
					avalilableClients.push(client);
				}
			}
		}
		res.status(200).send(avalilableClients);
	}

	// static async set(req: Request, res: Response, next: NextFunction): Promise<void> {
	// 	if (req.body) {
	// 		const updateResult: ConfigCommandObject | null = await CommandModel.findOneAndUpdate({_id: req.body._id}, req.body);
	// 		if (updateResult){
	// 			res.status(200).send({status: 200, msg: 'Successfully updated'});
	// 		} else {
	// 			res.status(200).send({status: 400, msg: 'Error while updating'});
	// 		}
	// 	} else
	// 		res.status(409).send({status: 409, msg: 'Missing data'});
	// }

}
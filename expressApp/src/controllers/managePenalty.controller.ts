import { Response, NextFunction } from 'express';
import { Request } from './../types/custom';
import { CommandModel } from '../models/command';
import { ConfigCommandObject } from '../types/configs';

export class PenaltyController {

	// static check = (req: Request, res: Response, next: NextFunction): void => {
	// 	res.send({status: 200, msg: "Command manager is up and running"});
	// }


	// static async get(req: Request, res: Response, next: NextFunction): Promise<void> {
	// 	if (req.params?.guildId && req.params.moduleName && req.params.commandName) {
	// 		res.send(await CommandModel.findOne({id: "0/" + req.params.guildId + "/" + req.params.moduleName + "/" + req.params.commandName}));
	// 	} else
	// 		res.status(409).send({status: 409, msg: 'Missing data, commandName, moduleName, guildId are required'});
	// }

	// static async list(req: Request, res: Response, next: NextFunction): Promise<void> {
	// 	if (req.params?.guildId && req.params.moduleName) {
	// 		res.send(await CommandModel.find({id: RegExp("0/" + req.params.guildId + "/" + req.params.moduleName + "/")}));
	// 	} else
	// 		res.status(409).send({status: 409, msg: 'Missing data, commandName, moduleName, guildId are required'});
	// }

	// static async set(req: Request, res: Response, next: NextFunction): Promise<void> {
	// 	if (req.params) {
	// 		const updateResult: ConfigCommandObject | null = await CommandModel.findOneAndUpdate({_id: req.params._id}, req.params);
	// 		if (updateResult){
	// 			res.status(200).send({status: 200, msg: 'Successfully updated'});
	// 		} else {
	// 			res.status(200).send({status: 400, msg: 'Error while updating'});
	// 		}
	// 	} else
	// 		res.status(409).send({status: 409, msg: 'Missing data'});
	// }

	// static async toggle(req: Request, res: Response, next: NextFunction): Promise<void> {
	// 	if (req.params?.guildId && req.params.systemId) {
	// 		const queryResult: ConfigCommandObject | null = await CommandModel.findById(req.params.systemId);
	// 		if (queryResult) {
	// 			queryResult.enabled = !queryResult.enabled;
	// 			const updateResult: ConfigCommandObject | null = await CommandModel.findByIdAndUpdate(req.params.systemId, queryResult);
	// 			if (updateResult){
	// 				res.status(200).send({status: 200, msg: 'Successfully updated'});
	// 			} else
	// 				res.status(400).send({status: 400, msg: 'Error while updating'});
	// 		} else
	// 			res.status(400).send({status: 400, msg: 'Error while updating'});
	// 	} else
	// 		res.status(409).send({status: 409, msg: 'Missing data'});
	// }
}
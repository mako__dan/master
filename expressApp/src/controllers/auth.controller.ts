import { Response, NextFunction } from 'express';
import { Request } from './../types/custom';
import passport from 'passport';
import { GuildInfo } from 'passport-discord';

export class AuthController {

	static check = (req: Request, res: Response, next: NextFunction): void => {
		res.send({status: 200, msg: "Discord auth service is up and running"});
	}

	static signin = (req: Request, res: Response, next: NextFunction): void => {
		passport.authenticate('discord')(req, res, next);
	}

	static signout = (req: Request, res: Response, next: NextFunction): void => {
		req.logout();
		res.status(200).send("200");
	}

	static signinResponse = (req: Request, res: Response, next: NextFunction): void => {
		res.status(200).send(`
		<body>
			<script>
			window.opener.postMessage(`+ JSON.stringify(req.user) +`, '*');
			window.close();
			</script>
		</body>
		`);
	}

	static hasPermission = (req: Request, res: Response, next: NextFunction): void => {
		if (req.user) {
			if (req.user.guilds) {
				if (req.query?.guildid) {
					const permTest: GuildInfo | undefined = req.user.guilds.find((guildtocheck: GuildInfo): boolean => guildtocheck.id === req.query?.guildid);
					if (permTest) {
						if (permTest.permissions & 0x8) {
							next();
						} else
							res.status(401).send({status: 401, msg: 'You are not administrator of this guild'});
					} else
						res.status(417).send({status: 417, msg: 'guild not found'});
				} else
					res.status(417).send({status: 417, msg: 'Missing guildId'});
			} else
				res.status(417).send({status: 417, msg: 'User have no guilds'});
		} else
			res.status(401).send({status: 401, msg: 'Unauthorized'});
	}

	static isLogged = (req: Request, res: Response, next: NextFunction): void => {
		if (req.user) {
			next();
		} else
			res.status(401).send({status: 401, msg: 'Unauthorized'});
	}

	static loggedData = (req: Request, res: Response, next: NextFunction): void => {
		res.send(req.user);
	}
}
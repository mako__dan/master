import { Response, NextFunction } from 'express';
import { SocketManager } from '../util/socket';
import { Request } from './../types/custom';

export class RefreshController {

	static check = (req: Request, res: Response, next: NextFunction): void => {
		res.send({status: 200, msg: "Config refresher service is up and running"});
	}

	static refreshCommands = (req: Request, res: Response, next: NextFunction): void => {
		const start: number = Date.now();
		SocketManager.sendData("refreshPrefix", {guildId: "847599807178080316"}, (): void => {
			res.send({status: 200, latency: Date.now() - start});
		});
	}

	static refreshSlashCommands = (req: Request, res: Response, next: NextFunction): void => {
		const start: number = Date.now();
		SocketManager.sendData("refreshPrefix", {guildId: "847599807178080316"}, (): void => {
			res.send({status: 200, latency: Date.now() - start});
		});

	}

}
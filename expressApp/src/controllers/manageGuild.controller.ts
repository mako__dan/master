import { Response, NextFunction } from 'express';
import { Request } from './../types/custom';
import { ConfigGuild } from '../types/configs';
import { GuildModel } from '../models/guild';
import { SocketManager } from '../util/socket';

export class ManageGuildController {

	static check = (req: Request, res: Response, next: NextFunction): void => {
		res.send({status: 200, msg: "Command manager is up and running"});
	}

	static async get(req: Request, res: Response, next: NextFunction): Promise<void> {
		if (req.query?.guildid && req.query.botid) {
			res.send(await GuildModel.findOne({id: req.query.botid + "/" + req.query.guildid}));
		} else
			res.status(409).send({status: 409, msg: 'Missing data, guildid is required'});
	}

	static async list(req: Request, res: Response, next: NextFunction): Promise<void> {
		res.send(req.user?.guilds);
	}

	static async set(req: Request, res: Response, next: NextFunction): Promise<void> {
		if (req.query) {
			const updateResult: ConfigGuild | null  = await  GuildModel.findOneAndUpdate({_id: req.query._id}, req.body);
			if (updateResult){
				SocketManager.sendData("reloadGuildData", {guildId: req.query?.guildid}, (response: string): void => {
					res.status(200).send({status: 200, msg: 'Successfully updated'});
				});
			} else {
				res.status(200).send({status: 400, msg: 'Error while updating'});
			}
		} else
			res.status(409).send({status: 409, msg: 'Missing data'});
	}
}
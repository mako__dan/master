import { Response, NextFunction } from 'express';
import { Request } from './../types/custom';
import { CommandModel } from '../models/command';
import { ConfigCommandObject } from '../types/configs';
import { SocketManager } from '../util/socket';

export class ManageCommandController {

	static check = (req: Request, res: Response, next: NextFunction): void => {
		res.send({status: 200, msg: "Command manager is up and running"});
	}


	static async get(req: Request, res: Response, next: NextFunction): Promise<void> {
		if (req.query?.guildid && req.query.modulename && req.query.commandname && req.query.botid) {
			res.send(await CommandModel.findOne({id: req.query.botid + "/" + req.query.guildid + "/" + req.query.modulename + "/" + req.query.commandname}));
		} else
			res.status(409).send({status: 409, msg: 'Missing data, commandname, modulename, guildid are required'});
	}

	static async list(req: Request, res: Response, next: NextFunction): Promise<void> {
		if (req.query?.guildid && req.query.modulename) {
			res.send(await CommandModel.find({id: RegExp( req.query.botid + "/" + req.query.guildid + "/" + req.query.modulename + "/")}));
		} else
			res.status(409).send({status: 409, msg: 'Missing data, commandname, modulename, guildid are required'});
	}

	static async set(req: Request, res: Response, next: NextFunction): Promise<void> {
		if (req.query) {
			const updateResult: ConfigCommandObject | null = await CommandModel.findOneAndUpdate({_id: req.query._id}, req.body);
			if (updateResult){
				SocketManager.sendData("reloadGuildData", {guildId: req.query?.guildid}, (response: string): void => {
					res.status(200).send({status: 200, msg: 'Successfully updated'});
				});
			} else {
				res.status(200).send({status: 400, msg: 'Error while updating'});
			}
		} else
			res.status(409).send({status: 409, msg: 'Missing data'});
	}

	static async toggle(req: Request, res: Response, next: NextFunction): Promise<void> {
		if (req.query?.guildid && req.query.systemid) {
			const queryResult: ConfigCommandObject | null = await CommandModel.findOne({id: req.query.systemid});
			if (queryResult) {
				queryResult.enabled = !queryResult.enabled;
				const updateResult: ConfigCommandObject | null = await CommandModel.findOneAndUpdate({id: req.query.systemid}, queryResult);
				if (updateResult){
					SocketManager.sendData("reloadGuildData", {guildId: req.query?.guildid}, (response: string): void => {
						res.status(200).send({status: 200, msg: 'Successfully updated'});
					});
				} else
					res.status(400).send({status: 400, msg: 'Error while updating'});
			} else
				res.status(400).send({status: 400, msg: 'Error while updating'});
		} else
			res.status(409).send({status: 409, msg: 'Missing data'});
	}
}
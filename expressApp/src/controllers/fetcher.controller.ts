import { Response, NextFunction } from 'express';
import { Request } from './../types/custom';
import { SocketManager } from '../util/socket';

export class FetcherController {

	static check = (req: Request, res: Response, next: NextFunction): void => {
		res.send({status: 200, msg: "Data fetcher is up and running"});
	}

	static async listChannels(req: Request, res: Response, next: NextFunction): Promise<void> {
		SocketManager.sendData("getChannels", {guildId: req.query?.guildid}, (response: string): void => {
			res.status(200).send(response);
		});
	}

	static async listRoles(req: Request, res: Response, next: NextFunction): Promise<void> {
		SocketManager.sendData("getRoles", {guildId: req.query?.guildid}, (response: string): void => {
			res.status(200).send(response);
		});
	}

	static async listEmotes(req: Request, res: Response, next: NextFunction): Promise<void> {
		SocketManager.sendData("getEmotes", {guildId: req.query?.guildid}, (response: string): void => {
			res.status(200).send(response);
		});
	}
}
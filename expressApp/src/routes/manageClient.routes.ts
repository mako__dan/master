import { Express, Router } from 'express';
import { AuthController } from '../controllers/auth.controller';
import { ManageClientController } from '../controllers/manageClient.controller';

export class ManageClientRouter {
	static async register(app: Express): Promise<void> {
		const router: Router = Router();

		router.get("/", ManageClientController.check);

		router.get("/get", AuthController.hasPermission, ManageClientController.get);

		router.get("/list", AuthController.hasPermission, ManageClientController.list);

		// router.post("/set", ManageClientController.set);

		app.use('/api/manage/client', router);
	}
}

import { Express, Router } from 'express';
import { AuthController } from '../controllers/auth.controller';
import { ManageModuleController } from '../controllers/manageModule.controller';

export class ManageModuleRouter {
	static async register(app: Express): Promise<void> {
		const router: Router = Router();

		router.get("/", ManageModuleController.check);

		router.get("/get", AuthController.hasPermission, ManageModuleController.get);

		router.get("/list", AuthController.hasPermission, ManageModuleController.list);

		router.post("/set", ManageModuleController.set);

		router.post("/toggle", AuthController.hasPermission, ManageModuleController.toggle);

		app.use('/api/manage/module', router);
	}
}

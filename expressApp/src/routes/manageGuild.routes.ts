import { Express, Router } from 'express';
import { AuthController } from '../controllers/auth.controller';
import { ManageGuildController } from '../controllers/manageGuild.controller';

export class ManageGuildRouter {
	static async register(app: Express): Promise<void> {
		const router: Router = Router();

		router.get("/", ManageGuildController.check);

		router.get("/get", AuthController.hasPermission, ManageGuildController.get);

		router.get("/list", AuthController.hasPermission, ManageGuildController.list);

		router.post("/set", ManageGuildController.set);

		app.use('/api/manage/guild', router);
	}
}

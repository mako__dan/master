import { Express, Router } from 'express';
import { AuthController } from '../controllers/auth.controller';
import { FetcherController } from '../controllers/fetcher.controller';

export class FetcherRouter {
	static async register(app: Express): Promise<void> {
		const router: Router = Router();

		router.get("/", FetcherController.check);

		router.get("/channels", AuthController.hasPermission, FetcherController.listChannels);

		router.get("/roles", AuthController.hasPermission, FetcherController.listRoles);

		router.get("/emotes", AuthController.hasPermission, FetcherController.listEmotes);

		app.use('/api/fetch', router);
	}
}

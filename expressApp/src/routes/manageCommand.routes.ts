import { Express, Router } from 'express';
import { AuthController } from '../controllers/auth.controller';
import { ManageCommandController } from '../controllers/manageCommand.controller';

export class ManageCommandRouter {
	static async register(app: Express): Promise<void> {
		const router: Router = Router();

		router.get("/", ManageCommandController.check);

		router.get("/get", AuthController.hasPermission, ManageCommandController.get);

		router.get("/list", AuthController.hasPermission, ManageCommandController.list);

		router.post("/set", ManageCommandController.set);

		router.post("/toggle", AuthController.hasPermission, ManageCommandController.toggle);

		app.use('/api/manage/command', router);
	}
}

import { Express, Router } from 'express';
import { RefreshController } from '../controllers/refresher.controller';
import { AuthController } from '../controllers/auth.controller';

export class RefreshRouter {
	static async register(app: Express): Promise<void> {
		const router: Router = Router();

		router.get("/", RefreshController.check);

		router.get("/commands", AuthController.hasPermission, RefreshController.refreshCommands);

		router.get("/slashcommands", AuthController.hasPermission, RefreshController.refreshSlashCommands);

		app.use('/api/refresh', router);
	}
}

import { Express, Router } from 'express';
import { AuthController } from '../controllers/auth.controller';

export class AuthRouter {
	static async register(app: Express): Promise<void> {
		const router: Router = Router();

		router.get("/", AuthController.check);

		router.get("/discord", AuthController.signin);

		router.get("/discord/redirect", AuthController.signin, AuthController.signinResponse);

		router.get("/logout", AuthController.signout);

		router.get("/discord/loggedData", AuthController.isLogged,  AuthController.loggedData);

		app.use('/api/auth', router);
	}
}

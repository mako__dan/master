/* tslint:disable */

import fs from 'fs';

export class Logger {
	static colors = require('colors/safe');
	static loadTime: number;
	static init(){
		this.colors.setTheme({
			error: 'brightRed',
			warning: 'brightYellow',
			success: 'brightGreen',
			util: 'blue',
			module: 'magenta',
			feature: 'cyan'
		});
		this.loadTime = Date.now()
	}

	static log(message: string | number, color: string = 'green', dataJson?: unknown | undefined): void {
		console.log(this.colors[color](message), dataJson ?? '');
	}

	static file(filename: string, data: any): void {
		fs.writeFileSync("./logs/" + filename + ".json", JSON.stringify(data, null, 2));
	}
}

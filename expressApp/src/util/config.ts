import { Error } from "mongoose";

export function Configure(): void {
	if (process.env.port && process.env.clientId && process.env.clientSecret && process.env.clientCallback && process.env.mongoConnection && process.env.appHost) {
		AppConfig.port = Number(process.env.port);
		AppConfig.clientId = process.env.clientId;
		AppConfig.clientSecret = process.env.clientSecret;
		AppConfig.clientCallback = process.env.clientCallback;
		AppConfig.mongoConnection = process.env.mongoConnection;
		AppConfig.appHost = process.env.appHost;
	} else {
		throw new Error("Configuration is missing");
	}
}

export const AppConfig: { port: number, clientId: string, clientSecret: string, clientCallback: string, mongoConnection: string, appHost: string } = {
	port: 0,
	clientId: "",
	clientSecret: "",
	clientCallback: "",
	mongoConnection: "",
	appHost: "",
};
import { Server, Socket } from "socket.io";
import { Logger } from "./logger";

export class SocketManager {
	static io: Server;
	static socket: Socket;

	static init(): void {
		this.io = new Server(8123);
		this.io.on("connect", (socket: Socket): void => {
			this.socket = socket;
			Logger.log(`connect ${socket.id}`, "warning");
			socket.on("disconnect", (): void => {
				Logger.log(`disconnect ${socket.id}`, "error");
			});
		});
	}

	static sendData(commandName: string, data: unknown, cb: (response: string) => void): void {
		if (this.socket){
			this.socket.emit(commandName,data, cb);
		} else {
			Logger.log("Not conected", "error");
		}
	}
}

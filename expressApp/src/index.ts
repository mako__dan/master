import { AuthRouter } from './routes/auth.routes';

import express from 'express';
import bodyParser from 'body-parser';
import passport from 'passport';
import discordStrategy from './strategies/discord';
import { connect, connection } from 'mongoose';
import session from 'express-session';
import Store from 'connect-mongo';
import { Logger } from './util/logger';
import { SocketManager } from './util/socket';
import { RefreshRouter } from './routes/refresher.routes';
import { ManageCommandRouter } from './routes/manageCommand.routes';
import cors from 'cors';
import { ManageClientRouter } from './routes/manageClient.routes';
import { FetcherRouter } from './routes/fetcher.routes';
import { ManageGuildRouter } from './routes/manageGuild.routes';
import { ManageModuleRouter } from './routes/manageModule.routes';
import * as dotenv from 'dotenv';
import { Configure, AppConfig } from './util/config';

dotenv.config();
Configure();

Logger.init();
connect(AppConfig.mongoConnection).then((): void => {
	SocketManager.init();
	run();
}).catch((err: string): void => {
	Logger.log('Unable to connect to MongoDB Database.\nError: ' + err, "error");
});

async function run(): Promise<void> {
	const app: express.Express = express();

	discordStrategy.register();

	const corsOptions:{ origin: string; optionsSuccessStatus: number; credentials: boolean } = {
		origin: AppConfig.appHost,
		credentials: true,
		optionsSuccessStatus: 200
	};

	app.use(cors(corsOptions));
	app.use(express.static(__dirname + '/public'));
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(session({
		secret: 'zakk',
		cookie: {
			maxAge: 60000*60*24
		},
		resave: false,
		saveUninitialized: false,
		store: new (Store(session))({mongooseConnection: connection})
	}));
	app.use(passport.initialize());
	app.use(passport.session());

	await AuthRouter.register(app);
	await RefreshRouter.register(app);
	await FetcherRouter.register(app);
	await ManageCommandRouter.register(app);
	await ManageModuleRouter.register(app);
	await ManageClientRouter.register(app);
	await ManageGuildRouter.register(app);

	app.get("/", (req: express.Request, res: express.Response): void => {
		res.json({ message: "ZakkBot backend is up and running without any problems :D" });
	});

	app.listen(AppConfig.port, (): void => {
		Logger.log(`App listening on port ${AppConfig.port}`, "success");
	});
}

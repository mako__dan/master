export type User = {
	discordId?: string
	discordTag?: string
	avatar?: string
	guilds?: GuildInfo[]
	isAdministrator?: boolean
};

export interface Request {
	user?: User;
	query?: {
		botid?: string
		guildid?: string
		modulename?: string
		commandname?: string
		_id?: string
		systemid: string
		clientid: string
	};
	body: JSON;
	logout(): func;
}

export type FEUser = {
	discordId: string
	discordTag: string
	avatar: string
	guilds: GuildInfo[]
	isAdministrator: boolean
};

export type GuildInfo = {
	owner: boolean;
	permissions: number;
	icon: string;
	id: string;
	name: string;
};
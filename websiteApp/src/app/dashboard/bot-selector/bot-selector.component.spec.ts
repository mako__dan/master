import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BotSelectorComponent } from './bot-selector.component';

describe('BotSelectorComponent', () => {
  let component: BotSelectorComponent;
  let fixture: ComponentFixture<BotSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BotSelectorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BotSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

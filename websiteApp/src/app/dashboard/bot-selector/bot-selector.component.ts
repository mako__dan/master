import { Component, OnInit } from '@angular/core';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SyncService } from 'src/app/_services/sync.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-bot-selector',
  templateUrl: './bot-selector.component.html'
})
export class BotSelectorComponent implements OnInit {
  availableBots: any;
  selectedGuildId: any;
  urlParams: any;

  constructor(
    private apiService: APIService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
		private toastr: ToastrService,
		private syncService: SyncService
    ) { }

  async ngOnInit(): Promise<void> {
    this.activatedRoute.params.subscribe(async (data) => {
      this.urlParams = JSON.parse(JSON.stringify(data));
      this.syncService.sendRouteToParent(data);
      this.selectedGuildId = data.guildId;
      this.apiService.listAvailableBots(data.guildId).subscribe((availableBots) => {
        this.availableBots = availableBots;
      }, () => {
        this.toastr.error('There are no available bots currently', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
      });
    });
  }

  botSelected(botId: string): void {
    this.urlParams.botId = botId;
    this.syncService.sendRouteToParent(this.urlParams);
    this.router.navigate(['/dashboard', botId, this.selectedGuildId]);

  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardNotFoundComponent } from './not-found/not-found.component';
import { CommandDetailComponent } from './command-detail/command-detail.component';
import { ModuleDetailComponent } from './module-detail/module-detail.component';
import { GuildDetailComponent } from './guild-detail/guild-detail.component';
import { BotSelectorComponent } from './bot-selector/bot-selector.component';
import { GuildSelectorComponent } from './guild-selector/guild-selector.component';
import { InviteComponent } from './invite/invite.component';
import { InviteCompleteComponent } from './invite-complete/invite-complete.component';

const routes: Routes = [
	{ path: '', component: GuildSelectorComponent }, //guild selector
	{ path: 'invitecomplete', component: InviteCompleteComponent }, //invite complete redirector
	{ path: ':guildId', component: BotSelectorComponent }, //bot selector
	{ path: ':botId/:guildId', component: GuildDetailComponent }, //guild detail
	{ path: ':botId/:guildId/invite', component: InviteComponent }, //guild selector
	{ path: ':botId/:guildId/:modulename', component: ModuleDetailComponent }, //module detail
	{ path: ':botId/:guildId/:modulename/command/:commandname', component: CommandDetailComponent }, //command detail
	{ path: '**', component: DashboardNotFoundComponent }, //command detail

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class DashboardRoutingModule { }

export const routedComponents = [];

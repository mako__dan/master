import { Component, OnInit, OnDestroy } from '@angular/core';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConfigCommandObject } from 'src/app/_types/types';
import { switchMap } from 'rxjs/operators'
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'src/app/_services/common.service';

@Component({
	selector: 'app-command-detail',
	templateUrl: './command-detail.component.html'
})
export class CommandDetailComponent implements OnInit, OnDestroy {
	selectedGuild: any;
	selectedBot: any;
	activatedRouteParams: any;

	subscriptionRoute: any;

	form: FormGroup = new FormGroup({});
	prefixedPhrases: any;
	phrases: any;

	availableRoles: any;
	availableRooms: any;

	initialcommandData: any;

	constructor(
		private apiService: APIService,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private toastr: ToastrService,
		private commonService: CommonService
	) { }

	async ngOnInit(): Promise<void> {
		this.initForm();
		this.subscriptionRoute = this.activatedRoute.params
		.pipe(
			switchMap((data) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(data));
				return this.apiService.fetchChannels(this.activatedRouteParams.guildId)
			}),
			switchMap((data: any) => {
				this.availableRooms = data.filter((channel: any) => channel.type !== "GUILD_VOICE").sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})
				return this.apiService.fetchRoles(this.activatedRouteParams.guildId)
			})
		)
		.subscribe(async (data: any) => {
			this.availableRoles = data.sort((b: any, a: any) => {return a.rawPosition - b.rawPosition});
			this.updateForm();
		}, () => {
			this.toastr.error('Failed to load guild channels or roles', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
		})
	}

	ngOnDestroy() {
		this.subscriptionRoute.unsubscribe();
	}

	initForm(){
		this.form = new FormGroup({
			enabled: new FormControl(false, Validators.required),
			phrases: new FormControl([], Validators.required),
			prefixedPhrases: new FormControl([], Validators.required),
			removeInvalidSelf: new FormControl(0, Validators.required),
			removeInvalidTrigger: new FormControl(true, Validators.required),
			removeSelf: new FormControl(false, Validators.required),
			removeTrigger: new FormControl(true, Validators.required),
			respondChannelBlacklist: new FormControl([], Validators.required),
			respondChannelWhitelist: new FormControl([], Validators.required),
			respondRoleBlacklist: new FormControl([], Validators.required),
			respondRoleWhitelist: new FormControl([], Validators.required),
			sendToChannel: new FormControl("", Validators.required),
			sendToOrigin: new FormControl(false, Validators.required),
			slashCommand: new FormGroup({
				name: new FormControl("", Validators.required),
				description: new FormControl("", Validators.required),
			}),
		});
	}

	async updateForm(){
		this.apiService.getCommand(
			this.activatedRouteParams.botId,
			this.activatedRouteParams.guildId,
			this.activatedRouteParams.modulename,
			this.activatedRouteParams.commandname
			).subscribe((commandData) => {
				if (commandData) {
					this.initialcommandData = commandData;
					this.fillForm(commandData as ConfigCommandObject);
				} else {
					this.router.navigate(['/dashboard']);
				}
			}, () => {
				this.router.navigate(['/dashboard']);
			});
	}

	fillForm(data: ConfigCommandObject) {
		data.respondChannelBlacklist = this.commonService.fillArray(this.initialcommandData.respondChannelBlacklist, this.availableRooms);
		data.respondChannelWhitelist = this.commonService.fillArray(this.initialcommandData.respondChannelWhitelist, this.availableRooms);
		data.respondRoleBlacklist = this.commonService.fillArray(this.initialcommandData.respondRoleBlacklist, this.availableRoles);
		data.respondRoleWhitelist = this.commonService.fillArray(this.initialcommandData.respondRoleWhitelist, this.availableRoles);


		this.form.patchValue(data);
		this.prefixedPhrases = data.prefixedPhrases;
		this.phrases = data.phrases;
	}
	
	async submitForm() {
		const toSave = this.form.value;
		toSave.respondChannelBlacklist = this.commonService.clearArray(toSave.respondChannelBlacklist);
		toSave.respondChannelWhitelist = this.commonService.clearArray(toSave.respondChannelWhitelist);
		toSave.respondRoleBlacklist = this.commonService.clearArray(toSave.respondRoleBlacklist);
		toSave.respondRoleWhitelist = this.commonService.clearArray(toSave.respondRoleWhitelist);
		toSave.phrases = this.commonService.clearArray(this.phrases);
		toSave.prefixedPhrases = this.commonService.clearArray(this.prefixedPhrases);
		this.apiService.updateCommand(
			this.activatedRouteParams.botId,
			this.activatedRouteParams.guildId,
			this.initialcommandData._id,
			toSave
			).subscribe(() => {
				this.toastr.success('Command updated', 'Success', {timeOut: 3000, closeButton: true, progressBar: true});
			}, () => {
				this.toastr.error('Failed to update command', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
			});
	}
}

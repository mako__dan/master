import { Component, OnInit } from '@angular/core';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-module-detail',
	templateUrl: './module-detail.component.html'
})
export class ModuleDetailComponent implements OnInit {
	activatedRouteParams: any;

	subscriptionRoute: any;

	form: FormGroup = new FormGroup({});

	moduleCommands: any;
	moduleData: any;
	moduleType: string = "";

	constructor(
		private apiService: APIService,
		private activatedRoute: ActivatedRoute,
		private toastr: ToastrService,
		private router: Router
	) { }

	async ngOnInit(): Promise<void> {
		this.initForm();
		this.subscriptionRoute = this.activatedRoute.params
		.pipe(
			switchMap((data) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(data));
				return this.apiService.getModule(this.activatedRouteParams.botId, this.activatedRouteParams.guildId, this.activatedRouteParams.modulename)
			}),
			switchMap((data: any) => {
				if (!data) {
					this.router.navigate(['/dashboard',this.activatedRouteParams.botId,this.activatedRouteParams.guildId,'invite'])
				}
				this.moduleData = data;
				this.moduleType = data.id.split("/")[2];
				return this.apiService.listCommands(this.activatedRouteParams.botId, this.activatedRouteParams.guildId, this.activatedRouteParams.modulename)
			})
		)
		.subscribe(async (data: any) => {
			this.moduleCommands = data;
			this.updateForm();
		}, () => {
			this.toastr.error('Failed to load module data', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
		})
	}

	ngOnDestroy() {
		this.subscriptionRoute.unsubscribe();
	}

	initForm(){

	}

	updateForm() {

	}

	toggleCommand(commandId: string) {
		this.apiService.toggleCommand(
			this.activatedRouteParams.botId,
			this.activatedRouteParams.guildId,
			commandId
			).subscribe(() => {
				this.toastr.success('Module config updated', 'Success', {timeOut: 3000, closeButton: true, progressBar: true});
			}, () => {
				this.toastr.error('Failed to update module config', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
			});
	}

	moduleDataReturned(data: any) {
		const ne = {data, _id: this.moduleData._id};

		this.apiService.updateModule(
			this.activatedRouteParams.botId,
			this.activatedRouteParams.guildId,
			ne._id,
			ne
			).subscribe(() => {
				this.toastr.success('Module config updated', 'Success', {timeOut: 3000, closeButton: true, progressBar: true});
			}, () => {
				this.toastr.error('Failed to update module config', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
			});
	}
}

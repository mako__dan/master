import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppData } from '../../_services/app.service';
import { GuardService } from 'src/app/_services/guard.service';
import { User, ClientConfig } from 'src/app/_types/types';
import { Router, ActivatedRoute } from '@angular/router';
import { APIService } from 'src/app/_services/api.service';
import { switchMap, skip } from 'rxjs/operators';
import { SyncService } from 'src/app/_services/sync.service';
import { empty } from 'rxjs';
import { moduleTypes } from '../../_static/moduleTypes';
import { Guild } from 'discord.js';

@Component({
	selector: 'dashboard-layout',
	templateUrl: './layout.component.html',
})

export class DashboardLayoutComponent {
	sidebarPinned: boolean = true;
	sidebarHover: boolean = true;

	loggedUser: User | undefined;
	selectedGuild: any;

	activatedRouteParams: any;

	availableBots: any;
	selectedBot: any;

	subscriptionRoute: any;

	moduleTypes = moduleTypes.filter((moduleType: any) => moduleType.public);

	constructor(
		private titleService: Title,
		public appData: AppData,
		private guardService: GuardService,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private apiService: APIService,
		private syncService: SyncService
	) {
		this.titleService.setTitle(this.appData.appConfig.hostnameTitle);
	}

	public async ngOnInit(): Promise<void> {
		await this.checkIsLogged();

		this.syncService.routeToParent
		.pipe(
			skip(1),
			switchMap((routeData: any) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(routeData));
				this.selectedBot = this.activatedRouteParams.botId;
				this.selectedGuild = this.loggedUser?.guilds.find((guild: Guild) => guild.id === this.activatedRouteParams.guildId);
				if (this.activatedRouteParams.guildId) {
					return this.apiService.listAvailableBots(this.activatedRouteParams.guildId);
				}
				return empty()

			})
		)
		.subscribe((availableBots: any) => {
			this.availableBots = availableBots;
			this.selectedBot = this.availableBots?.find((bot: any) => bot.id === this.selectedBot);
		})

		this.subscriptionRoute = this.activatedRoute.children[0].params
		.pipe(
			switchMap((routeData: any) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(routeData));
				this.selectedBot = this.activatedRouteParams.botId;
				this.selectedGuild = this.loggedUser?.guilds.find((guild: Guild) => guild.id === this.activatedRouteParams.guildId);
				if (this.activatedRouteParams.guildId) {
					return this.apiService.listAvailableBots(this.activatedRouteParams.guildId);
				}
				return empty()
			})
		)
		.subscribe((availableBots: any) => {
			this.availableBots = availableBots;
			this.selectedBot = this.availableBots?.find((bot: any) => bot.id === this.selectedBot);
		})
	}

	ngOnDestroy() {
		if (this.subscriptionRoute) this.subscriptionRoute.unsubscribe();
	}

	public async checkIsLogged() {
		await this.guardService.updateLoggedUser();
		const userRequestResult = await this.guardService.getLoggedUser()
		if(userRequestResult === null) {
			this.router.navigate(["/"]);
		} else {
			this.loggedUser = userRequestResult;
		}
	}

	public async guildReselected(): Promise<void> {
		this.apiService.listAvailableBots(this.selectedGuild.id).subscribe((availableBots) => {
			this.activatedRouteParams.guildId = this.selectedGuild.id;
			this.availableBots = availableBots;
			if (this.selectedBot && !this.availableBots.find((bot: ClientConfig) => bot.id === this.selectedBot.id)) {
				this.selectedBot = null;
			}
			if(this.availableBots.length === 1) {
				this.selectedBot = this.availableBots[0];
				this.botReselected();
			} else {
				this.redirect();
			}
		});
	}

	public async botReselected(): Promise<void> {
		this.activatedRouteParams.botId = this.selectedBot.id;
		this.redirect();
	}

	public redirect() {
		if (this.activatedRouteParams.guildId){
			if (this.activatedRouteParams.botId){
				if (this.activatedRouteParams.modulename){
					this.router.navigate(['dashboard', this.activatedRouteParams.botId, this.activatedRouteParams.guildId, this.activatedRouteParams.modulename]);
				} else {
					this.router.navigate(['dashboard', this.activatedRouteParams.botId, this.activatedRouteParams.guildId]);
				}
			} else {
				this.router.navigate(['dashboard', this.activatedRouteParams.guildId]);
			}
		} else {
			this.router.navigate(['dashboard']);
		}
	}

	public logout() {
		this.guardService.logout();
	}
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { CommonService } from 'src/app/_services/common.service';
import { ToastrService } from 'ngx-toastr';
import loggerTypes  from '../../../_static/loggerTypes';

@Component({
  selector: 'moduledetail-logger',
  templateUrl: './logger.component.html'
})
export class LoggerModuleComponent implements OnInit {

  @Input() moduleData: any;
  @Output() closeEvent = new EventEmitter();

  loaded = false;
  form: FormGroup = new FormGroup({});

  trigggerId: number = 0;
  responseId: number = 0;

  subscriptionRoute: any;
  activatedRouteParams: any;

	availableRooms: any;

  embedId: string = "";

  loggerTypes = loggerTypes;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: APIService,
    private commonService: CommonService,
		private toastr: ToastrService
  ) { }

  ngOnInit(): void {
		this.subscriptionRoute = this.activatedRoute.params
		.pipe(
			switchMap((data) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(data));
				return this.apiService.fetchChannels(this.activatedRouteParams.guildId)
			})
		)
		.subscribe(async (data: any) => {
			this.availableRooms = data.filter((channel: any) => channel.type !== "GUILD_VOICE").sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})
      this.initForm();
		}, () => {
			this.toastr.error('Failed to load guild channels', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
		})
  }

  getGlobalEvent(form: any) {
    return form.controls.events.controls;
  }

  initForm(): void {
    const events = new FormArray([]);

    loggerTypes.forEach(event => {
      events.push(new FormGroup({
        enabled: new FormControl(this.moduleData.events[event].enabled ?? null),
        channel: new FormControl(this.commonService.fillString(this.moduleData.events[event].channel, this.availableRooms)),
        eventName: new FormControl(event)
      }))
    });

    this.form = new FormGroup({
      events: events,
      sendToChannel: new FormControl(this.moduleData.joinMessage ?? null),
    })

    console.log(this.form);
    this.loaded = true;
  }

  onSubmit() {
    if (this.form.valid) {
      const returnValue = this.form.value;
      const events: any = {};

      returnValue.events.forEach((event: any) => {
        events[event.eventName] = {
          channel: this.commonService.clearString(event.channel),
          enabled: event.enabled
        }
        event.channel = this.commonService.clearString(event.channel);
      });

      returnValue.events = events;
      returnValue.sendToChannel = this.commonService.clearString(returnValue.sendToChannel);
      this.closeEvent.emit(returnValue);
    } else {
      // TODO: notify error
    }

  }

}

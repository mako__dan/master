import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { CommonService } from 'src/app/_services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'moduledetail-verify',
  templateUrl: './verification.component.html'
})
export class VerificationModuleComponent implements OnInit {

  @Input() moduleData: any;
  @Output() closeEvent = new EventEmitter();

  loaded: boolean = false;
  form: FormGroup = new FormGroup({});

  modalData: any;
  modalOpen: boolean = false;
  trigggerId: number = 0;
  responseId: number = 0;

  subscriptionRoute: any;
  activatedRouteParams: any;

  customEmojis: any[] = [];
  availableEmotes: any;
  availableRoles: any;
  availableRooms: any;

  embedId: number = 0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: APIService,
    private commonService: CommonService,
		private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.initForm();
		this.subscriptionRoute = this.activatedRoute.params
		.pipe(
			switchMap((data) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(data));
				return this.apiService.fetchChannels(this.activatedRouteParams.guildId)
      }),
      switchMap((data: any) => {
				this.availableRooms = data.filter((channel: any) => channel.type !== "GUILD_VOICE").sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})
				return this.apiService.fetchRoles(this.activatedRouteParams.guildId)
      }),
      switchMap((data: any) => {
				this.availableRoles= data.sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})
				return this.apiService.fetchEmotes(this.activatedRouteParams.guildId)
      })
  	)
		.subscribe(async (data: any) => {
			this.availableEmotes = data.sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})
      this.fillForm();
      this.normalizeEmotes();
      this.loaded = true;
		}, () => {
			this.toastr.error('Failed to load guild channels, roles or emotes', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
		})
  }

  initForm(): void {
    this.form = new FormGroup({
      channel: new FormControl(),
      emotes: new FormGroup({
        yes: new FormControl(Validators.required),
        yesDisplay: new FormControl(Validators.required),
        undecided: new FormControl(Validators.required),
        undecidedDisplay: new FormControl(Validators.required),
        no: new FormControl(Validators.required),
        noDisplay: new FormControl(Validators.required),
        forceYes: new FormControl(Validators.required),
        forceYesDisplay: new FormControl(Validators.required),
        forceNo: new FormControl(Validators.required),
        forceNoDisplay: new FormControl(Validators.required),
        reset: new FormControl(Validators.required),
        resetDisplay: new FormControl(Validators.required)
      }),
      verifiedRole: new FormControl(Validators.required),
      points: new FormGroup({
        yes: new FormControl(Validators.required),
        undecided: new FormControl(Validators.required),
        no: new FormControl(Validators.required)
      }),
      groups: new FormArray([]),
      neededPoints: new FormControl(),
      authority: new FormControl([]),
      respondRoleWhitelist: new FormControl([]),
      respondRoleBlacklist: new FormControl([])
    })
  }

  fillForm(): void {
    this.moduleData.channel = this.commonService.fillString(this.moduleData.channel, this.availableRooms);
    this.moduleData.verifiedRole = this.commonService.fillString(this.moduleData.verifiedRole, this.availableRoles);
    this.moduleData.authority = this.commonService.fillArray(this.moduleData.authority, this.availableRoles);
    this.moduleData.respondRoleWhitelist = this.commonService.fillArray(this.moduleData.respondRoleWhitelist, this.availableRoles);
    this.moduleData.respondRoleBlacklist = this.commonService.fillArray(this.moduleData.respondRoleBlacklist, this.availableRoles);
    this.form.patchValue(this.moduleData);

    this.moduleData.groups.map((control: any) => {
      this.addGroup(control);
    })
  }

  addGroup(data?: any) {
    const form = new FormGroup({
      role: new FormControl(Validators.required),
      points: new FormGroup({
        yes: new FormControl(Validators.required),
        undecided: new FormControl(Validators.required),
        no: new FormControl(Validators.required)
      }),
    });
    if (data) {
      form.patchValue(data);
    }
    if (form.value.role) {
      form.controls.role.setValue(this.commonService.fillString(form.value.role, this.availableRoles));
    }
    (this.form.get('groups') as FormArray).push(form);
  }

  getGroup(form: any) {
     return form.controls.groups.controls;
  }

  onSubmit() {
    this.form.updateValueAndValidity();
    if (this.form.valid && this.form.value.verifiedRole && this.form.value.channel ) {
      this.form.value.channel = this.commonService.clearString(this.form.value.channel);
      this.form.value.verifiedRole = this.commonService.clearString(this.form.value.verifiedRole);
      this.form.value.authority = this.commonService.clearArray(this.form.value.authority);
      this.form.value.respondRoleWhitelist = this.commonService.clearArray(this.form.value.respondRoleWhitelist);
      this.form.value.respondRoleBlacklist = this.commonService.clearArray(this.form.value.respondRoleBlacklist);

      this.form.value.groups.forEach((group: any) => {
        group.role = this.commonService.clearString(group.role);
      });
      this.closeEvent.emit(this.form.value);
    } else {
      // TODO: notify error
    }

  }

  normalizeEmotes(){
    this.availableEmotes.forEach((emote: any) => {
      this.customEmojis.push(
        {
          name: emote.name,
          shortNames: [emote.name],
          text: emote.id,
          emoticons: [],
          keywords: [emote.name],
          spriteUrl: emote.url,
          imageUrl: emote.url,
        }
      );
    });
  }

  selectEmoji(id: any, event: any) {
    if (event.emoji.native) {
      this.form.value.emotes[id] = event.emoji.native;
      this.form.value.emotes[id+'Display'] = event.emoji.native;
    } else {
      this.form.value.emotes[id] = event.emoji.text;
      this.form.value.emotes[id+'Display'] = event.emoji.imageUrl;
    }
  }
}

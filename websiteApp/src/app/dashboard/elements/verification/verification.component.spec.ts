import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationModuleComponent } from './verification.component';

describe('VerificationModuleComponent', () => {
  let component: VerificationModuleComponent;
  let fixture: ComponentFixture<VerificationModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerificationModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { CommonService } from 'src/app/_services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'moduledetail-statusrole',
  templateUrl: './statusrole.component.html'
})
export class StatusroleModuleComponent implements OnInit {

  @Input() moduleData: any;
  @Output() closeEvent = new EventEmitter();

  form: FormGroup = new FormGroup({});

  modalData: any;
  modalOpen: boolean = false;
  trigggerId: number = 0;
  responseId: number = 0;

  subscriptionRoute: any;
  activatedRouteParams: any;

  customEmojis: any[] = [];
  availableEmotes: any;
	availableRoles: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: APIService,
    private commonService: CommonService,
		private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.initForm();
		this.subscriptionRoute = this.activatedRoute.params
		.pipe(
			switchMap((data) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(data));
				return this.apiService.fetchRoles(this.activatedRouteParams.guildId)
      }),
      switchMap((data: any) => {
        this.availableRoles = data.sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})	
				return this.apiService.fetchEmotes(this.activatedRouteParams.guildId)
      }),
		)
		.subscribe(async (data: any) => {
			this.availableEmotes = data.sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})	
      this.fillForm();
      this.normalizeEmotes();
		}, () => {
			this.toastr.error('Failed to load guild roles, or emotes', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
		})
  }

  initForm(): void {
    this.form = new FormGroup({
      groups: new FormArray([])
    })
  }

  fillForm(): void {
    this.form.patchValue(this.moduleData);

    this.moduleData.groups.map((control: any) => {
      this.addMessage(control);
    })
  }

  addMessage(reply?: any) {
    const form = new FormGroup({
      text: new FormControl(reply?.text ?? "", Validators.required),
      icon: new FormControl(reply?.icon ?? null),
      iconUrl: new FormControl(reply?.iconUrl ?? null),
      roles: new FormControl(reply?.roles ?? []),
    });
    if (form.value.roles) {
      form.controls.roles.setValue(this.commonService.fillArray(form.value.roles, this.availableRoles))
    }
    (this.form.get('groups') as FormArray).push(form);
  }

  normalizeEmotes(){
    this.availableEmotes.forEach((emote: any) => {
      this.customEmojis.push(
        {
          name: emote.name,
          shortNames: [emote.name],
          text: emote.id,
          emoticons: [],
          keywords: [emote.name],
          spriteUrl: emote.url,
          imageUrl: emote.url,
        }
      );
    });
  }

  selectEmoji(id: any, event: any) {
    if (event.emoji.text) {
      this.form.value.groups[id].icon = event.emoji.text;
      this.form.value.groups[id].iconUrl = event.emoji.imageUrl;
    } else {
      this.form.value.groups[id].icon = event.emoji.native;
      this.form.value.groups[id].iconUrl = event.emoji.native;
    }
  }

  getMessage(form: any) {
     return form.controls.groups.controls;
  }

  onSubmit() {
    this.form.value.groups.forEach((embed: any) => {
      embed.roles = this.commonService.clearArray(embed.roles);
    });
    this.closeEvent.emit(this.form.value);
  }

}
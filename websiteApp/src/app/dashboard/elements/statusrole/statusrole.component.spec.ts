import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusroleModuleComponent } from './statusrole.component';

describe('StatusroleComponent', () => {
  let component: StatusroleModuleComponent;
  let fixture: ComponentFixture<StatusroleModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatusroleModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusroleModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbedsenderModuleComponent } from './embedsender.component';

describe('EmbedsenderModuleComponent', () => {
  let component: EmbedsenderModuleComponent;
  let fixture: ComponentFixture<EmbedsenderModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmbedsenderModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbedsenderModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { CommonService } from 'src/app/_services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'moduledetail-embedsender',
  templateUrl: './embedsender.component.html'
})
export class EmbedsenderModuleComponent implements OnInit {

  @Input() moduleData: any;
  @Output() closeEvent = new EventEmitter();

  form: FormGroup = new FormGroup({});

  modalData: any;
  modalOpen: boolean = false;
  trigggerId: number = 0;
  responseId: number = 0;

  subscriptionRoute: any;
  activatedRouteParams: any;

	availableRooms: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: APIService,
    private commonService: CommonService,
		private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.initForm();
		this.subscriptionRoute = this.activatedRoute.params
		.pipe(
			switchMap((data) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(data));
				return this.apiService.fetchChannels(this.activatedRouteParams.guildId)
			})
		)
		.subscribe(async (data: any) => {
			this.availableRooms = data.filter((channel: any) => channel.type !== "GUILD_VOICE").sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})
      this.fillForm();
		}, () => {
			this.toastr.error('Failed to load guild channels', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
		})
  }

  initForm(): void {
    this.form = new FormGroup({
      embeds: new FormArray([])
    })
  }

  fillForm(): void {
    this.form.patchValue(this.moduleData);

    this.moduleData.embeds.map((control: any) => {
      this.addMessage(control);
    })
  }

  addMessage(reply?: any) {
    const form = new FormGroup({
      name: new FormControl(reply?.name ?? "", Validators.required),
      channel: new FormControl(reply?.channel ?? null),
      messages: new FormControl(reply?.messages ?? null),
    });
    if (form.value.channel) {
      form.controls.channel.setValue(this.commonService.fillString(form.value.channel, this.availableRooms))
    }
    (this.form.get('embeds') as FormArray).push(form)
  }

  getMessage(form: any) {
     return form.controls.embeds.controls;
   }

  openModal(replyData: any, trigggerId: number, responseId: number) {
    this.modalData = replyData;
    this.trigggerId = trigggerId;
    this.responseId = responseId;
    this.modalOpen = true;
  }

  closeModal(data?: any) {
    if (data === null) {
      this.modalOpen = false;
    } else {
      this.form.value.embeds[this.trigggerId].messages[this.responseId] = data
      this.modalOpen = false;
    }
  }

  addSubMessage(i: number) {
    if ((this.form as any).controls.embeds.controls[i].controls.messages.value === null) {
      (this.form as any).controls.embeds.controls[i].controls.messages.value = [{}];
    } else {
      (this.form as any).controls.embeds.controls[i].controls.messages.value.push({});
    }
    (this.form as any).controls.embeds.controls[i].controls.messages.updateValueAndValidity({ onlySelf: false, emitEvent: true });
  }

  onSubmit() {
    this.form.value.embeds.forEach((embed: any) => {
      embed.channel = this.commonService.clearString(embed.channel);
    });
    this.closeEvent.emit(this.form.value);
  }

}

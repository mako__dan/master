import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/_services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'moduledetail-roomtheme',
  templateUrl: './roomtheme.component.html'
})
export class RoomthemeModuleComponent implements OnInit {

  @Input() moduleData: any;
  @Output() closeEvent = new EventEmitter();

  form: FormGroup = new FormGroup({});

  modalData: any;
  modalOpen: boolean = false;
  trigggerId: number = 0;
  responseId: number = 0;

  subscriptionRoute: any;
  activatedRouteParams: any;

	availableRooms: any;
  autoArchiveDurations = [{name: 60, id: 60}, {name: 1440, id: 1440}];
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: APIService,
    private commonService: CommonService,
		private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.initForm();
		this.subscriptionRoute = this.activatedRoute.params
		.pipe(
			switchMap((data) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(data));
				return this.apiService.fetchChannels(this.activatedRouteParams.guildId)
			})
		)
		.subscribe(async (data: any) => {
			this.availableRooms = data.filter((channel: any) => channel.type !== "GUILD_VOICE").sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})	
      this.fillForm();
    }, () => {
			this.toastr.error('Failed to load guild channels', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
		})
  }

  initForm(): void {
    this.form = new FormGroup({
      channels: new FormControl(this.moduleData.channels ?? []),
      autoThread: new FormGroup({
        enabled: new FormControl(this.moduleData.autoThread.enabled ?? true),
        defaultThread: new FormGroup({
          name: new FormControl(this.moduleData.autoThread.defaultThread.name ?? ""),
          autoArchiveDuration: new FormControl(this.moduleData.autoThread.defaultThread.name ?? ""),
        })
      })
    })
  }

  fillForm() {
    this.moduleData.channels = this.commonService.fillArray(this.moduleData.channels, this.availableRooms);
    this.moduleData.autoThread.defaultThread.autoArchiveDuration = this.commonService.fillString(this.moduleData.autoThread.defaultThread.autoArchiveDuration, this.autoArchiveDurations);
    this.form.patchValue(this.moduleData);
  }

  getMessage(form: any) {
     return form.controls.embeds.controls;
  }

  onSubmit() {
    this.form.value.channels = this.commonService.clearArray(this.form.value.channels);
    this.form.value.autoThread.defaultThread.autoArchiveDuration = this.commonService.clearString(this.form.value.autoThread.defaultThread.autoArchiveDuration);
    this.closeEvent.emit(this.form.value);
  }
  
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomthemeComponent } from './roomtheme.component';

describe('RoomthemeComponent', () => {
  let component: RoomthemeComponent;
  let fixture: ComponentFixture<RoomthemeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoomthemeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomthemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

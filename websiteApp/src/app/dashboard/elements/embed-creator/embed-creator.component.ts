import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { TransformVisitor } from '@angular/compiler/src/render3/r3_ast';
import { FormatWidth } from '@angular/common';
import { CommonService } from 'src/app/_services/common.service';

@Component({
  selector: 'app-embed-creator',
  templateUrl: './embed-creator.component.html'
})
export class EmbedCreatorComponent implements OnInit {

  constructor(
    private commonService: CommonService,
  ) { }

  @Input() embedData: any;
  @Output() closeEvent = new EventEmitter();

  form: FormGroup = new FormGroup({});

  ngOnInit(): void {
    this.initForm();
    this.fillform();
  }

  closeModal() {
    this.closeEvent.emit(null);
  }

  initForm() {
    this.form = new FormGroup({
      content: new FormControl(),
      embeds: new FormArray([]),
      files: new FormControl([])
      // components?: (MessageActionRow | MessageActionRowOptions)[]
      // allowedMentions?: MessageMentionOptions
      // reply: ReplyOptions
      // stickers: StickerResolvable[]
    })
  }

  fillform() {
    if (this.embedData) {
      this.form.patchValue(this.embedData);
      if (this.embedData.embeds) {
        this.embedData.embeds.map((data: any) => {
          this.addEmbed(data);
        });
      }
    } else {
      this.embedData = {};
      this.fillform();
    }

  }

  addEmbed(data?: any ) {
    const formGroupMain = new FormGroup({
      title: new FormControl(data?.title ?? null),
      description: new FormControl(data?.description ?? null),
      url: new FormControl(data?.null ?? null),
      color: new FormControl(data?.color ?? null),
      fields: new FormArray([]),
      author: new FormGroup({
        name: new FormControl(data?.author?.name ?? null),
        url: new FormControl(data?.author?.url ?? null),
        iconURL: new FormControl(data?.author?.iconURL ?? null),
      }),
      thumbnail: new FormGroup({
        url: new FormControl(data?.thumbnail?.url ?? null),
      }),
      image: new FormGroup({
        url: new FormControl(data?.image?.url ?? null),
      }),
      footer: new FormGroup({
        text: new FormControl(data?.footer?.text ?? null),
        iconURL: new FormControl(data?.footer?.iconURL ?? null),
      })
    });

    if (data?.fields) {
      data.fields.map((data2: any) => {
        this.addField(formGroupMain, data2);
      })
    }
    (this.form.get('embeds') as FormArray).push(formGroupMain);
  }

  addField(formGroupMain: any, data?: any){
    const formGorup = new FormGroup({
      name: new FormControl(data?.name ?? null),
      value: new FormControl(data?.value ?? null),
      inline: new FormControl(data?.inline ?? null),
    });
    (formGroupMain.get('fields') as FormArray).push(formGorup);
  }

  getEmbeds(form: any) {
    return form.controls.embeds.controls;
  }

  getFields(form: any) {
    return form.controls.fields.controls;
  }

  onSubmit() {
    this.form.value.files = this.commonService.clearArray(this.form.value.files);
    this.clearObject(this.form.value);
    this.embedData = this.form.value;
    this.closeEvent.emit(this.embedData);
  }

  clearObject(objectToClear: any) {
    Object.keys(objectToClear).forEach((k) =>  {
      objectToClear[k] == null && delete objectToClear[k];
      if(typeof objectToClear[k] === "object") {
        this.clearObject(objectToClear[k]);
        if (Object.keys(objectToClear[k]).length === 0 ) {
          delete objectToClear[k];
        }
      }
    });
  }
}

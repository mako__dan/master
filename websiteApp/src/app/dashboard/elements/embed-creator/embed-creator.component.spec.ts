import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbedCreatorComponent } from './embed-creator.component';

describe('EmbedCreatorComponent', () => {
  let component: EmbedCreatorComponent;
  let fixture: ComponentFixture<EmbedCreatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmbedCreatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbedCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

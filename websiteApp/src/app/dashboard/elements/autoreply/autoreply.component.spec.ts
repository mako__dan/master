import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoreplyModuleComponent } from './autoreply.component';

describe('AutoreplyModuleComponent', () => {
  let component: AutoreplyModuleComponent;
  let fixture: ComponentFixture<AutoreplyModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoreplyModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoreplyModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { CommonService } from 'src/app/_services/common.service';

@Component({
  selector: 'moduledetail-autoreply',
  templateUrl: './autoreply.component.html'
})
export class AutoreplyModuleComponent implements OnInit {

  @Input() moduleData: any;
  @Output() closeEvent = new EventEmitter();

  form: FormGroup = new FormGroup({});

  modalData: any;
  modalOpen: boolean = false;
  trigggerId: number = 0;
  responseId: number = 0;

  constructor(
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.fillForm();
  }

  initForm(): void {
    this.form = new FormGroup({
      enabled: new FormControl(false, Validators.required),
      replies: new FormArray([])
    })
  }

  fillForm(): void {
    this.form.patchValue(this.moduleData);

    this.moduleData.replies.map((control: any) => {
      this.addReply(control);
    })
  }

  addReply(reply?: any) {
    const form = new FormGroup({
      autodelete: new FormControl(reply?.autodelete ?? 0, Validators.required),
      replies: new FormControl(reply?.replies ?? null, Validators.required),
      triggers: new FormControl(reply?.triggers ?? null, Validators.required),
    });
    (this.form.get('replies') as FormArray).push(form)
  }

  removeReply(replyIndex: number) {
    (this.form.get('replies') as FormArray).removeAt(replyIndex);
  }

  getGlobalReply(form: any) {
     return form.controls.replies.controls;
   }

  openModal(data: any, trigggerId: number, responseId: number) {
    this.modalData = data;
    this.trigggerId = trigggerId;
    this.responseId = responseId;
    this.modalOpen = true;
  }

  closeModal(data?: any) {
    if (data === null) {
      this.modalOpen = false;
    } else {
      this.form.value.replies[this.trigggerId].replies[this.responseId] = data;
      this.modalOpen = false;
    }
  }

  addModalReply(i: number) {
    if ((this.form as any).controls.replies.controls[i].controls.replies.value === null) {
      (this.form as any).controls.replies.controls[i].controls.replies.value = [{}];
    } else {
      (this.form as any).controls.replies.controls[i].controls.replies.value.push({});
    }
    (this.form as any).controls.replies.controls[i].controls.replies.updateValueAndValidity({ onlySelf: false, emitEvent: true });
  }

  removeModalReply(i: number, k: number) {
    ((this.form as any).controls.replies.controls[i].controls.replies.value as Array<any>).splice(k, 1);
  }

  onSubmit() {
    this.form.value.replies.forEach((reply: any) => {
      if (reply.triggers) {
        reply.triggers = this.commonService.clearArray(reply.triggers);
      }
    });
    this.closeEvent.emit(this.form.value);
  }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { CommonService } from 'src/app/_services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'moduledetail-welcome',
  templateUrl: './welcome.component.html'
})
export class WelcomeModuleComponent implements OnInit {

  @Input() moduleData: any;
  @Output() closeEvent = new EventEmitter();

  form: FormGroup = new FormGroup({});

  modalData: any;
  modalOpen: boolean = false;
  trigggerId: number = 0;
  responseId: number = 0;

  subscriptionRoute: any;
  activatedRouteParams: any;

	availableRooms: any;

  embedId: string = "";

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: APIService,
    private commonService: CommonService,
		private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.initForm();
		this.subscriptionRoute = this.activatedRoute.params
		.pipe(
			switchMap((data) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(data));
				return this.apiService.fetchChannels(this.activatedRouteParams.guildId)
			})
		)
		.subscribe(async (data: any) => {
			this.availableRooms = data.filter((channel: any) => channel.type !== "GUILD_VOICE").sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})	
      this.fillForm();
		}, () => {
			this.toastr.error('Failed to load guild channels', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
		})
  }

  initForm(): void {
    this.form = new FormGroup({
      sendToChannel: new FormControl(this.moduleData.sendToChannel ?? null, Validators.required),
      joinMessage: new FormControl(this.moduleData.joinMessage ?? null),
      leaveMessage: new FormControl(this.moduleData.leaveMessage ?? null),
    })
  }

  fillForm(): void {
    this.moduleData.sendToChannel = this.commonService.fillString(this.moduleData.sendToChannel, this.availableRooms);
    
    this.form.patchValue(this.moduleData);
  }

  openModal(type: string) {
    if (type === 'join') {
      this.modalData = this.form.controls.joinMessage.value;
      this.embedId = "join";
    } else {
      this.modalData = this.form.controls.leaveMessage.value;
      this.embedId = "leave";
    }
    this.modalOpen = true;
  }

  closeModal(data?: any) {
    if (data === null) {
      this.modalOpen = false;
    } else {
      if (this.embedId === 'join') {
        this.form.value.joinMessage = data
      } else {
        this.form.value.leaveMessage = data
      }
      this.modalOpen = false;
    }
  }

  onSubmit() {
    if (this.form.valid) {
      this.form.value.sendToChannel = this.commonService.clearString(this.form.value.sendToChannel);
      this.closeEvent.emit(this.form.value);
    } else {
      // TODO: notify error
    }

  }

}

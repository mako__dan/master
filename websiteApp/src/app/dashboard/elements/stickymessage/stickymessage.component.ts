import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { CommonService } from 'src/app/_services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'moduledetail-stickymessage',
  templateUrl: './stickymessage.component.html'
})
export class StickymessageModuleComponent implements OnInit {

  @Input() moduleData: any;
  @Output() closeEvent = new EventEmitter();

  form: FormGroup = new FormGroup({});

  modalData: any;
  modalOpen: boolean = false;
  trigggerId: number = 0;
  responseId: number = 0;

  subscriptionRoute: any;
  activatedRouteParams: any;

	availableRooms: any;

  embedId: number = 0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: APIService,
    private commonService: CommonService,
		private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.initForm();
		this.subscriptionRoute = this.activatedRoute.params
		.pipe(
			switchMap((data) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(data));
				return this.apiService.fetchChannels(this.activatedRouteParams.guildId)
			})
		)
		.subscribe(async (data: any) => {
			this.availableRooms = data.filter((channel: any) => channel.type !== "GUILD_VOICE").sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})
      this.fillForm();
		}, () => {
			this.toastr.error('Failed to load guild channels', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
		})
  }

  initForm(): void {
    this.form = new FormGroup({
      messages: new FormArray([])
    })
  }

  fillForm(): void {
    this.form.patchValue(this.moduleData);

    this.moduleData.messages.map((control: any) => {
      this.addMessage(control);
    })
  }

  addMessage(reply?: any) {
    const form = new FormGroup({
      channel: new FormControl(reply?.channel ?? null, Validators.required),
      message: new FormControl(reply?.message ?? null),
    });
    if (form.value.channel) {
      form.controls.channel.setValue(this.commonService.fillString(form.value.channel, this.availableRooms));
    }
    if (form.value.message) {
      form.controls.message.setValue(form.value.message);
    } else {
      form.controls.message.setValue({});
    }
    form.controls.message.updateValueAndValidity({ onlySelf: false, emitEvent: true });
    (this.form.get('messages') as FormArray).push(form);
  }

  getMessage(form: any) {
     return form.controls.messages.controls;
   }

  openModal(replyData: any, trigggerId: number) {
    this.modalData = replyData.controls.message.value;
    this.embedId = trigggerId;
    this.modalOpen = true;
  }

  closeModal(data?: any) {
    if (data === null) {
      this.modalOpen = false;
    } else {
      this.form.value.messages[this.trigggerId].message = data
      this.modalOpen = false;
    }
  }

  onSubmit() {
    if (this.form.valid) {
      this.form.value.messages.forEach((messageChannel: any) => {
        messageChannel.channel = this.commonService.clearString(messageChannel.channel);
      });
      this.closeEvent.emit(this.form.value);
    } else {
      // TODO: notify error
    }

  }
}

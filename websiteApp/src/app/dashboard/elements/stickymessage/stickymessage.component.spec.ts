import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StickymessageComponent } from './stickymessage.component';

describe('StickymessageComponent', () => {
  let component: StickymessageComponent;
  let fixture: ComponentFixture<StickymessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StickymessageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StickymessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

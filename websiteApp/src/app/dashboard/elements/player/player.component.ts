import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/_services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'moduledetail-player',
  templateUrl: './player.component.html'
})
export class PlayerModuleComponent implements OnInit {

  @Input() moduleData: any;
  @Output() closeEvent = new EventEmitter();

  form: FormGroup = new FormGroup({});

  modalData: any;
  modalOpen: boolean = false;
  trigggerId: number = 0;
  responseId: number = 0;

  subscriptionRoute: any;
  activatedRouteParams: any;

	availableRooms: any;
  availableVoiceRooms: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: APIService,
    private commonService: CommonService,
		private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.initForm();
		this.subscriptionRoute = this.activatedRoute.params
		.pipe(
			switchMap((data) => {
				this.activatedRouteParams = JSON.parse(JSON.stringify(data));
				return this.apiService.fetchChannels(this.activatedRouteParams.guildId)
			})
		)
		.subscribe(async (data: any) => {
			this.availableRooms = data.filter((channel: any) => channel.type !== "GUILD_VOICE").sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})	
      this.availableVoiceRooms = data.filter((channel: any) => channel.type === "GUILD_VOICE").sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})	
      this.fillForm();
    }, () => {
			this.toastr.error('Failed to load guild channels', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
		})
  }

  initForm(): void {
    this.form = new FormGroup({
      autoplay: new FormGroup({
        enabled: new FormControl(this.moduleData.autoplay.enabled ?? true),
        returnEmptyQueue: new FormControl(this.moduleData.autoplay.returnEmptyQueue ?? true),
        startOnMemberJoin: new FormControl(this.moduleData.autoplay.startOnMemberJoin ?? true),
        autoclearWhenPlayCommand: new FormControl(this.moduleData.autoplay.autoclearWhenPlayCommand ?? true),
        autoShuffle: new FormControl(this.moduleData.autoplay.autoShuffle ?? true),
        voiceRoom: new FormControl(this.moduleData.autoplay.voiceRoom ?? ""),
        djRoom: new FormControl(this.moduleData.autoplay.djRoom ?? ""),
        playlist: new FormControl(this.moduleData.autoplay.playlist ?? "")
      })
    })
  }

  fillForm() {
    this.moduleData.autoplay.djRoom = this.commonService.fillString(this.moduleData.autoplay.djRoom, this.availableRooms);
    this.moduleData.autoplay.voiceRoom = this.commonService.fillString(this.moduleData.autoplay.voiceRoom, this.availableVoiceRooms);
    this.form.patchValue(this.moduleData);
  }

  getMessage(form: any) {
     return form.controls.embeds.controls;
  }

  onSubmit() {
    const sendForm = this.form.value;
    sendForm.autoplay.djRoom = this.commonService.clearString(this.form.value.autoplay.djRoom);
    sendForm.autoplay.voiceRoom = this.commonService.clearString(this.form.value.autoplay.voiceRoom);
    this.closeEvent.emit(sendForm);
  }

}

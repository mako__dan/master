import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { APIService } from 'src/app/_services/api.service';
import { switchMap } from 'rxjs/operators';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { CommonService } from 'src/app/_services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'moduledetail-moderation',
  templateUrl: './moderation.component.html',
})
export class ModerationModuleComponent implements OnInit {
  @Input() moduleData: any;
  @Output() closeEvent = new EventEmitter();

  availableRoles: any;
  availableRooms: any;

  activatedRouteParams: any;
  subscriptionRoute: any;

  form: FormGroup = new FormGroup({});

  modalData: any;
  modalOpen: boolean = false;
  embedId: string = "";

  constructor(
    private apiService: APIService,
    private activatedRoute: ActivatedRoute,
    private commonService: CommonService,
		private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.subscriptionRoute = this.activatedRoute.params
      .pipe(
        switchMap((data: any) => {
          this.activatedRouteParams = JSON.parse(JSON.stringify(data));
          return this.apiService.fetchChannels(this.activatedRouteParams.guildId);
        }),
        switchMap((data: any) => {
          this.availableRooms = data.filter((channel: any) => channel.type !== "GUILD_VOICE").sort((b: any, a: any) => {return a.rawPosition - b.rawPosition})	
          return this.apiService.fetchRoles(this.activatedRouteParams.guildId);
        })
      )
      .subscribe(async (data: any) => {
        this.availableRoles = data.sort((b: any, a: any) => {
          return a.rawPosition - b.rawPosition;
        });
        this.fillForm();
      }, () => {
        this.toastr.error('Failed to load guild roles', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
      });
  }

  ngOnDestroy() {
    this.subscriptionRoute.unsubscribe();
  }

  initForm() {
    this.form = new FormGroup({
      sbanroles: new FormControl([], Validators.required),
      muteroles: new FormControl([], Validators.required),
      penalizations: new FormGroup({
        ban: new FormGroup({
          channel: new FormControl(""),
          message: new FormControl("")
        }),
        mute: new FormGroup({
          channel: new FormControl(""),
          message: new FormControl("")
        }),
        sban: new FormGroup({
          channel: new FormControl(""),
          message: new FormControl("")
        }),
        unban: new FormGroup({
          channel: new FormControl(""),
          message: new FormControl("")
        }),
        unmute: new FormGroup({
          channel: new FormControl(""),
          message: new FormControl("")
        })
      })
    });
  }

  onSubmit() {
    this.form.value.sbanroles = this.commonService.clearArray(this.form.value.sbanroles);
    this.form.value.muteroles = this.commonService.clearArray(this.form.value.muteroles);
    this.closeEvent.emit(this.form.value);
  }

  fillForm(): void {
    this.moduleData.sbanroles = this.commonService.fillArray(
      this.moduleData.sbanroles,
      this.availableRoles
    );
    this.moduleData.muteroles = this.commonService.fillArray(
      this.moduleData.muteroles,
      this.availableRoles
    );
    this.form.patchValue(this.moduleData);
  }

  openModal(id: string) {
    switch (id) {
      case "mute":
        this.modalData = this.form.value.penalizations.mute.message;
        break;
      case "unmute":
        this.modalData = this.form.value.penalizations.unmute.message;
        break;
      case "ban":
        this.modalData = this.form.value.penalizations.ban.message;
        break;
      case "unban":
        this.modalData = this.form.value.penalizations.unban.message;
        break;
      case "sban":
        this.modalData = this.form.value.penalizations.sban.message;
        break;
    }
    this.embedId = id;
    this.modalOpen = true;
  }

  closeModal(data?: any) {
    if (data === null) {
      this.modalOpen = false;
    } else {
      switch (this.embedId) {
        case "mute":
          this.form.value.penalizations.mute.message = data;
          break;
        case "unmute":
          this.form.value.penalizations.unmute.message = data;
          break;
        case "ban":
          this.form.value.penalizations.ban.message = data;
          break;
        case "unban":
          this.form.value.penalizations.unban.message = data;
          break;
        case "sban":
          this.form.value.penalizations.sban.message = data;
          break;
      }
      this.modalOpen = false;
    }
  }

}

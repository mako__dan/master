import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/_services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'moduledetail-autorole',
  templateUrl: './autorole.component.html'
})
export class AutoroleModuleComponent implements OnInit {
  @Input() moduleData: any;
  @Output() closeEvent = new EventEmitter();

  form: FormGroup = new FormGroup({});

  availableRoles: any;

  activatedRouteParams: any;
  subscriptionRoute: any;

  constructor(
    private apiService: APIService,
    private activatedRoute: ActivatedRoute,
    private commonService: CommonService,
		private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.subscriptionRoute = this.activatedRoute.params
      .pipe(
        switchMap((data: any) => {
          this.activatedRouteParams = JSON.parse(JSON.stringify(data));
          return this.apiService.fetchRoles(this.activatedRouteParams.guildId);
        })
      )
      .subscribe(async (data: any) => {
        this.availableRoles = data.sort((b: any, a: any) => {return a.rawPosition - b.rawPosition});
        this.fillForm();
      }, () => {
        this.toastr.error('Failed to load guild channels', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
      });
  }

  initForm(): void {
    this.form = new FormGroup({
      roles: new FormControl([], Validators.required),
    });
  }

  fillForm(): void {
    this.moduleData.roles = this.commonService.fillArray(
      this.moduleData.roles,
      this.availableRoles
    );
    this.form.controls.roles.setValue(this.moduleData.roles);
  }

  onSubmit() {
    this.closeEvent.emit({roles:this.commonService.clearArray(this.form.value.roles)});
  }
}

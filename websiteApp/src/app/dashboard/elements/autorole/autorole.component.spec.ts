import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoroleComponent } from './autorole.component';

describe('AutoroleComponent', () => {
  let component: AutoroleComponent;
  let fixture: ComponentFixture<AutoroleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoroleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoroleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

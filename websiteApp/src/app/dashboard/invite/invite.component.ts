import { Component, OnInit } from '@angular/core';
import { APIService } from 'src/app/_services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { switchMap } from 'rxjs/operators';
import { GuardService } from 'src/app/_services/guard.service';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html'
})
export class InviteComponent implements OnInit {
  activatedRouteParams: any;

  selectedBot: any;
  selectedGuild: any;
  guildData: any;
  showmessage: boolean =  false;

  constructor(
    private apiService: APIService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private guardService: GuardService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params
    .pipe(
      switchMap((data: any) => {
        this.activatedRouteParams = JSON.parse(JSON.stringify(data));
        return this.apiService.listAvailableBots(data.guildId)
      }),
    )
    .subscribe(async (data: any) => {
      this.selectedBot = data.find((bot:any) => bot.id === this.activatedRouteParams.botId);
      this.selectedGuild = (await this.guardService.getLoggedUser()).guilds.find((guild: any) => guild.id === this.activatedRouteParams.guildId);
    }, () => {
      this.toastr.error('There are no available bots currently', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
    });
  }

  invite(): void {
    window.open("https://discord.com/api/oauth2/authorize?client_id=" + this.selectedBot.id + "&permissions=8&scope=identify%20bot%20applications.commands&guild_id=" + this.selectedGuild.id + "&response_type=code&redirect_uri=" + encodeURIComponent(window.location.origin + "/dashboard/invitecomplete"),  "Invite " + this.selectedBot.name + " bot to server", "menubar=no,width=500,height=777,location=no,resizable=no,scrollbars=yes,status=no");
		window.addEventListener("message", () => {
			this.router.navigate(["/dashboard", this.activatedRouteParams.botId, this.activatedRouteParams.guildId])
		})
    this.showmessage = true;
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invite-complete',
  template: ''
})
export class InviteCompleteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.opener?.postMessage("", '*');
    window.close();
  }

}

import { Component, OnInit } from '@angular/core';
import { SyncService } from 'src/app/_services/sync.service';
import { ActivatedRoute, Router } from '@angular/router';
import { APIService } from 'src/app/_services/api.service';
import { switchMap } from 'rxjs/operators';
import { moduleTypes } from 'src/app/_static/moduleTypes';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/_services/common.service';

@Component({
  selector: 'app-guild-detail',
  templateUrl: './guild-detail.component.html'
})
export class GuildDetailComponent implements OnInit {
  activatedRouteParams: any;

  guildData: any;
  modulesList: any = [];
  moduleTypes: any;

	form: FormGroup = new FormGroup({});
  color: string = "";

	availableRoles: any;
  availableRooms: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: APIService,
    private toastr: ToastrService,
    private commonService: CommonService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.activatedRoute.params
    .pipe(
      switchMap((data: any) => {
        this.activatedRouteParams = JSON.parse(JSON.stringify(data));
        console.log("params activated");
        return this.apiService.getGuild(this.activatedRouteParams.botId, this.activatedRouteParams.guildId);
      }),
      switchMap((data: any) => {
        if (!data) {
          this.router.navigate(['/dashboard',this.activatedRouteParams.botId,this.activatedRouteParams.guildId,'invite'])
        }
        this.guildData = data;
        console.log("data found");
        return this.apiService.fetchChannels(this.activatedRouteParams.guildId)
      }),
      switchMap((data: any) => {
        this.availableRooms = data.filter((channel: any) => channel.type !== "GUILD_VOICE").sort((a: any, b: any) => {return a.rawPosition - b.rawPosition})
        console.log("rooms got");
        return this.apiService.listModule(this.activatedRouteParams.botId, this.activatedRouteParams.guildId);
      })
    )
    .subscribe((data: any) => {
      console.log("test150");
      data.forEach((module: any) => {
        delete module.data;
        module.name = module.id.split("/")[2];
        module.moduleSubData = moduleTypes.find((modules: any) => modules.dbName == module.name);
        this.modulesList.push(module);
      });
      this.modulesList = data;
      this.fillForm();
      console.log("form filled");
    });
  }

  initForm() {
    this.form = new FormGroup({
      argsDivider: new FormControl(),
      commander: new FormControl(),
      guildId: new FormControl(),
      name: new FormControl(),
      prefix: new FormControl(),
      removeInvalidSelf: new FormControl(),
      removeInvalidTrigger: new FormControl(),
      removeUnknown: new FormControl(),
      themeColor: new FormControl()
    })
  }

  fillForm() {
    this.guildData.commander = this.commonService.fillString(this.guildData.commander, this.availableRooms);
    this.color = this.guildData.themeColor;
    this.form.patchValue(this.guildData);
  }

  setColor(data: any) {
    console.log(data)
  }

  toggleModule(module: string) {
    this.apiService.toggleModule(this.activatedRouteParams.botId, this.guildData.guildId, module).subscribe(()=>{
      this.toastr.success('Module settings saved', 'Success', {timeOut: 3000, closeButton: true, progressBar: true});
    },
    ()=> {
      this.toastr.error('Failed to save module settings', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
    })
  }

  submitForm() {
    const toSave = this.form.value;
    toSave.commander = this.commonService.clearString(toSave.commander);
    toSave.themeColor = this.color;
    this.apiService.updateGuild(this.activatedRouteParams.botId, this.activatedRouteParams.guildId, this.guildData._id, toSave)
    .subscribe(() => {
      this.toastr.success('Guild updated', 'Success', {timeOut: 3000, closeButton: true, progressBar: true});
    }, () => {
      this.toastr.error('Failed to update guild', 'Error', {timeOut: 3000, closeButton: true, progressBar: true});
    });
  }
}

import { NgModule } from '@angular/core';
import { routedComponents, DashboardRoutingModule } from './dashboard-routing.module';
import { CommonModule } from '@angular/common';


import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgSelectModule } from '@ng-select/ng-select';
import { CommandDetailComponent } from './command-detail/command-detail.component';
import { ModuleDetailComponent } from './module-detail/module-detail.component';
import { GuildSelectorComponent } from './guild-selector/guild-selector.component';
import { BotSelectorComponent } from './bot-selector/bot-selector.component';
import { GuildDetailComponent } from './guild-detail/guild-detail.component';
import { TagInputModule } from 'ngx-chips';
import { AutoreplyModuleComponent } from './elements/autoreply/autoreply.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EmbedCreatorComponent } from './elements/embed-creator/embed-creator.component';
import { AutoroleModuleComponent } from './elements/autorole/autorole.component';
import { EmbedsenderModuleComponent } from './elements/embedsender/embedsender.component';
import { ModerationModuleComponent } from './elements/moderation/moderation.component';
import { PlayerModuleComponent } from './elements/player/player.component';
import { RoomthemeModuleComponent } from './elements/roomtheme/roomtheme.component';
import { StatusroleModuleComponent } from './elements/statusrole/statusrole.component';
import { StickymessageModuleComponent } from './elements/stickymessage/stickymessage.component';
import { VerificationModuleComponent } from './elements/verification/verification.component';
import { WelcomeModuleComponent } from './elements/welcome/welcome.component';
import { LoggerModuleComponent } from './elements/logger/logger.component';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { ColorPickerModule } from '@iplab/ngx-color-picker';
import { InviteComponent } from './invite/invite.component';
import { CustomCommonModule } from '../common.module';
import { InviteCompleteComponent } from './invite-complete/invite-complete.component';

@NgModule({
	imports: [
		DashboardRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		CommonModule,
		NgSelectModule,
		TagInputModule,
		NgbModule,
		PickerModule,
		ColorPickerModule,
		CustomCommonModule
	],
	declarations: [
		CommandDetailComponent,
		ModuleDetailComponent,
		GuildSelectorComponent,
		BotSelectorComponent,
		GuildDetailComponent,
		AutoreplyModuleComponent,
		EmbedCreatorComponent,
		AutoroleModuleComponent,
		EmbedsenderModuleComponent,
		ModerationModuleComponent,
		PlayerModuleComponent,
		RoomthemeModuleComponent,
		StatusroleModuleComponent,
		StickymessageModuleComponent,
		VerificationModuleComponent,
		WelcomeModuleComponent,
		InviteComponent,
    LoggerModuleComponent,
    InviteCompleteComponent
	],
	exports: [
	],
	providers: [
    ...routedComponents,
	]
})
export class DashboardModule { }

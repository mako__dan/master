import { Component, OnInit } from '@angular/core';
import { GuardService } from 'src/app/_services/guard.service';
import { SyncService } from 'src/app/_services/sync.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-guild-selector',
  templateUrl: './guild-selector.component.html'
})
export class GuildSelectorComponent implements OnInit {
  userGuilds: any;

  constructor(
    private guardService: GuardService,
    private syncService: SyncService,
    private router: Router
    ) { }

  async ngOnInit(): Promise<void> {
    this.userGuilds = (await this.guardService.getLoggedUser()).guilds;
    this.syncService.loggedUser.subscribe((newUser: any) => {
      this.userGuilds = newUser.guilds;
    })
  }

  guildSelected(guildId: string): void {
    this.router.navigate(['/dashboard', guildId]);
  }
}

import { Component, OnInit } from '@angular/core';
import { AppData } from './_services/app.service';
export interface Message {
  message: any;
}

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {
  bootstrap: any;
  appConfig: null;
  constructor(public appData: AppData) {}

  ngOnInit() {
    this.appData.initAppConfig();
    this.appConfig = this.appData._state.appName;
  }
}

import {
	Snowflake,
	EmbedFieldData,
	MessageEmbed,
	FileOptions,
	ReplyOptions,
	StickerResolvable,
	MessageMentionOptions,
	BufferResolvable,
	MessageAttachment,
	MessageActionRowOptions,
	MessageActionRow,
	MessageEmbedOptions,
	ColorResolvable,
	ThreadAutoArchiveDuration,
	ChatInputApplicationCommandData,
  MessageOptions
} from "discord.js";

export type StickyMessageDatabase = {
	channelId: string
	stickyMessageId: string
	messagesCount: number
	maxMessages: number
};

export type SystemConfig = {
	secretKey: string[]
	activity: SystemConfigActivity[]
};

export type SystemConfigActivity = {
	type: number
	time: number
	message: string
};

export type ConfigType = Record<Snowflake, ConfigGuild>;

export type ConfigGuild = {
	id: string
	guildId: string
	name: string
	commander: Snowflake
	removeUnknown: boolean
	themeColor: ColorResolvable | string
	prefix: string
	argsDivider: string
	removeInvalidTrigger: boolean
	removeInvalidSelf: number
};

export type VerifyGroup = {
	role:  Snowflake
	points: Record<string, number>
};

export type SlashCommandParameters = SlashCommandParameter[];

export type SlashCommandParameter = {
	type: string
	name: string
	description: string
	required: boolean
};
export type VerifyEmotes = Record<string, Snowflake>;

export type StatusroleGroup = {
	text: string
	icon: Snowflake
	role: Snowflake
};

export type ConfigBatch = {
	guild: ConfigGuild
	module: ConfigModule
	command: ConfigCommandObject
};

export type FullConfig = {
	modules: ConfigModule[] | undefined
	commands: ConfigCommandObject[] | undefined
};

export type ClientConfig = {
	token: string
	activities: ClientActivity[]
	mainClusterId: number
	logicId: number
	availability: string
	reservedFor: string[]
	icon: string
	id: Snowflake
	name: string
};

export type ClientActivity = {
	type: number
	time: number
	message: string
};

export type ConfigCommandObject = {
	id: string

	enabled: boolean
	prefixedPhrases: string[]
	slashCommand: ChatInputApplicationCommandData
	phrases: []
	respondChannelWhitelist: Snowflake[]
	respondChannelBlacklist: Snowflake[]
	respondRoleWhitelist: Snowflake[]
	respondRoleBlacklist: Snowflake[]
	removeSelf: boolean
	removeTrigger: boolean
	removeInvalidTrigger: boolean
	removeInvalidSelf: number
	sendToChannel: Snowflake
	sendToOrigin: boolean
};

export type ConfigLoggerModule = {
	events: {
		channelCreate: LoggerEvent
		channelDelete: LoggerEvent
		channelUpdate: LoggerEvent
		emojiCreate: LoggerEvent
		emojiDelete: LoggerEvent
		emojiUpdate: LoggerEvent
		guildBanAdd: LoggerEvent
		guildBanRemove: LoggerEvent
		guildMemberAdd: LoggerEvent
		guildMemberRemove: LoggerEvent
		guildMemberUpdate: LoggerEvent
		guildUpdate: LoggerEvent
		messageDelete: LoggerEvent
		messageUpdate: LoggerEvent
		roleCreate: LoggerEvent
		roleDelete: LoggerEvent
		roleUpdate: LoggerEvent
		voiceStateUpdate: LoggerEvent
		inviteCreate: LoggerEvent
		inviteDelete: LoggerEvent
		stageInstanceCreate: LoggerEvent
		stageInstanceDelete: LoggerEvent
		stageInstanceUpdate: LoggerEvent
		stickerCreate: LoggerEvent
		stickerDelete: LoggerEvent
		stickerUpdate: LoggerEvent
		threadCreate: LoggerEvent
		threadDelete: LoggerEvent
		threadUpdate: LoggerEvent
	},
	defaultChannel: Snowflake
};

export type LoggerEvent = {
	enabled: boolean
	channel: Snowflake
};

export type ConfigMessageSender = {
	sendToChannel: Snowflake
	sendToOrigin: boolean
};

export type ConfigEmbedJson = {
	color: string
	title: string
	description: string
	thumbnail: {
		url: string
	}
	fields: EmbedFieldData[]
	footer: {
		text: string
	}
};

export type ConfigModule = {
	id: string
	data: ConfigTypes
	enabled : boolean
};

export type ConfigTypes = ConfigLoggerModule | ConfigWelcomerModule | ConfigModerationModule | ConfigVerifyModule | ConfigEmbedsenderModule | ConfigAutoreplyModule | ConfigAutoreplyReply | ConfigStickymessageModule | ConfigRoomthemerModule | ConfigStatusroleModule | ConfigAutoroleModule | ConfigMessagesequenceModule | ConfigPlayerModule;

export enum ConfigTypesEnum {
	ConfigWelcomerModule,
	ConfigModerationModule,
	ConfigVerifyModule,
	ConfigEmbedsenderModule,
	ConfigAutoreplyModule,
	ConfigAutoreplyReply,
	ConfigStickymessageModule,
	ConfigRoomthemerModule,
	ConfigStatusroleModule,
	ConfigAutoroleModule,
	ConfigMessagesequenceModule,
	ConfigPlayerModule
}

export type ConfigWelcomerModule = {
	sendToChannel: Snowflake;
	joinMessage: MessageOptions;
	leaveMessage: MessageOptions;
};

export type ConfigModerationModule = {
	sbanroles: Snowflake[];
	muteroles: Snowflake[];
	penalizations: {
		mute: {
			channel: Snowflake
			message: MessageOptions
		}
		tempmute: {
			channel: Snowflake
			message: MessageOptions
		}
		unmute: {
			channel: Snowflake
			message: MessageOptions
		}
		sban: {
			channel: Snowflake
			message: MessageOptions
		}
		ban: {
			channel: Snowflake
			message: MessageOptions
		}
		tempban: {
			channel: Snowflake
			message: MessageOptions
		}
		unban: {
			channel: Snowflake
			message: MessageOptions
		}
	}
	sentry: {
		action: ("mute" | "sban")
		limit: number
		time: number
		enabled: boolean
	}
};

export type ConfigVerifyModule = {
	channel: Snowflake;
	emotes: VerifyEmotes;
	forceYes: Snowflake | string;
	forceNo: Snowflake | string;
	reset: Snowflake | string;
	verifiedRole: Snowflake;
	groups: VerifyGroup[];
	points: Record<string, number>;
	neededPoints: number;
	authority: Snowflake[];
	respondRoleWhitelist: Snowflake[];
	respondRoleBlacklist: Snowflake[];
};

export type ConfigEmbedsenderModule = {
	embeds: EmbedsenderEmbed[];
};

export type EmbedsenderEmbed = {
	name: string
	channel: Snowflake
	messages: [MessageOptions]
};

export type ConfigAutoreplyModule = {
	replies: ConfigAutoreplyReply[];
};

export type ConfigAutoreplyReply = {
	triggers: string[];
	replies: MessageOptions[];
	autodelete: number;
};

export type ConfigStickymessageModule = {
	messages: StickyMessage[];
};

export type StickyMessage = {
	channel: string,
	message: MessageOptions
};

export type ConfigRoomthemerModule = {
	channels: Snowflake[];
	autoThread: autoThreadSettings;
};

export type autoThreadSettings = {
	enabled: boolean
	customThreads: Record<Snowflake, ThreadSettings>
	defaultThread: ThreadSettings
};

export type ThreadSettings = {
	name: string
	autoArchiveDuration: ThreadAutoArchiveDuration
};

export type ConfigStatusroleModule = {
	groups: StatusroleGroup[]
};

export type ConfigAutoroleModule = {
	roles: Snowflake[]
};

export type ConfigMessagesequenceModule = {
	channels: Record<Snowflake, MessageOptions[]>
};

export type ConfigPlayerModule = {
	autoplay: AutoplayConfig
};

export type AutoplayConfig = {
	enabled: boolean
	returnEmptyQueue: boolean
	startOnMemberJoin: boolean
	autoclearWhenPlayCommand: boolean
	autoShuffle: boolean
	voiceRoom: Snowflake
	playlist: string
	djRoom: Snowflake
};

export type PenalisationData = {
	guildId: Snowflake
	guildMemberId: Snowflake
	penalisationType: PenalisationEnum
	endsIn: number
};

export type InfractionData = {
	id: string,
	guildId: Snowflake
	memberId: Snowflake
	penalisationType: PenalisationEnum
	penalized: number
	endsIn: number
	reason: string
};

export enum PenalisationEnum {
	MUTE,
	BAN,
	SBAN,
	WARN
}

export type SocketRequest = {
	guildId: Snowflake
};

// Express types

export type User = {
	discordId: string;
	discordTag: string;
	avatar: string;
	guilds: [];
	isAdministrator?: boolean;
};

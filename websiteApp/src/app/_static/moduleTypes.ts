export const moduleTypes = [
    {
        name: "Automatic reply",
        description: "",
        componentName: "autoreply",
        dbName: "autoreply",
        public: true,
        iconAndColor: "fas fa-reply-all text-cyan"
    },
    {
        name: "Autorole",
        description: "",
        componentName: "autorole",
        dbName: "autorole",
        public: true,
        iconAndColor: "fas fa-magic text-green"
    },
    {
        name: "Message sequence",
        description: "",
        componentName: "embedsender",
        dbName: "embedsender",
        public: true,
        iconAndColor: "fas fa-list-ul text-indigo"
    },
    {
        name: "Moderation",
        description: "",
        componentName: "moderation",
        dbName: "moderation",
        public: true,
        iconAndColor: "fas fa-gavel text-red"
    },
    {
        name: "Player",
        description: "",
        componentName: "player",
        dbName: "player",
        public: true,
        iconAndColor: "fas fa-headphones-alt text-purple"
    },
    {
        name: "Room theme",
        description: "",
        componentName: "roomthemer",
        dbName: "roomthemer",
        public: true,
        iconAndColor: "far fa-lightbulb text-orange"
    },
    {
        name: "Status role",
        description: "",
        componentName: "statusRole",
        dbName: "statusRole",
        public: true,
        iconAndColor: "fas fa-user-check text-yellow"
    },
    {
        name: "Sticky message",
        description: "",
        componentName: "stickymessage",
        dbName: "stickymessage",
        public: true,
        iconAndColor: "fas fa-paperclip text-pink"
    },
    {
        name: "Verification",
        description: "",
        componentName: "verify",
        dbName: "verify",
        public: true,
        iconAndColor: "fas fa-user-check text-green"
    },
    {
        name: "Welcome",
        description: "",
        componentName: "welcomer",
        dbName: "welcomer",
        public: true,
        iconAndColor: "fas fa-user-check text-orange"
    },
    {
        name: "Logger",
        description: "",
        componentName: "logger",
        dbName: "logger",
        public: true,
        iconAndColor: "fas fa-pen text-purple"
    },
]

export default [
  "channelCreate",
  "channelDelete",
  "channelUpdate",
  "emojiCreate",
  "emojiDelete",
  "emojiUpdate",
  "guildBanAdd",
  "guildBanRemove",
  "guildMemberAdd",
  "guildMemberRemove",
  "guildMemberUpdate",
  "guildUpdate",
  "messageDelete",
  "messageUpdate",
  "roleCreate",
  "roleDelete",
  "roleUpdate",
  "voiceStateUpdate",
  "inviteCreate",
  "inviteDelete",
  "stageInstanceCreate",
  "stageInstanceDelete",
  "stageInstanceUpdate",
  "stickerCreate",
  "stickerDelete",
  "stickerUpdate",
  "threadCreate",
  "threadDelete",
  "threadUpdate",
]

import {Component} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppData } from '../../_services/app.service';

@Component({
	selector: 'website-layout',
	templateUrl: './layout.component.html',
})

export class WebsiteLayoutComponent {

	loggedUser: any = null;
    bodyElement: any;
    
	constructor(
		private titleService: Title,
		public appData: AppData,
	) {
		this.titleService.setTitle(this.appData.appConfig.hostnameTitle);
	}

	public async ngOnInit(): Promise<void> {

		let bodyElement = document.querySelector("body");
		if (bodyElement) {
			this.bodyElement = bodyElement;
        }

        this.toggleSideNav();
    }

    redirectToHomepage() {
		window.location.replace("/");
	}

	toggleSideNav() {
		if (localStorage.getItem('sidenav-state') == 'true') {
			this.sidenavUnpin();
		} else {
			this.sidenavPin();
		}
	}

	sidenavPin() {
		localStorage.setItem('sidenav-state', 'true')
		this.bodyElement.classList.add("g-sidenav-pinned");
	}

	sidenavUnpin() {
		localStorage.setItem('sidenav-state', 'false')
		this.bodyElement.classList.remove("g-sidenav-pinned");
	}
    
}

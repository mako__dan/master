import {AfterViewInit, OnInit, Component} from '@angular/core';
import { GuardService } from 'src/app/_services/guard.service';



@Component({
  selector: 'router-outlet', 
  templateUrl: 'index.component.html',
})
export class IndexComponent implements AfterViewInit, OnInit {

  constructor(
    private guardService: GuardService,
    ) {
    }

    async ngAfterViewInit(): Promise<void> {

    }

  async ngOnInit(): Promise<void> {
  }

  public loginToApp() {
    this.guardService.openLoginWindow();
  }
}
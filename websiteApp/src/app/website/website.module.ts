import { NgModule } from '@angular/core';
import { WebsiteRoutingModule, routedComponents } from './website-routing.module';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';

@NgModule({
	imports: [
		WebsiteRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		CommonModule,
	],
	declarations: [
		IndexComponent,
  LoginComponent
	],
	exports: [
		IndexComponent
	],
	providers: [
    ...routedComponents,
	],
})
export class WebsiteModule { }

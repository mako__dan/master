import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'icon'
})
export class IconPipe implements PipeTransform {

  defaultImages = [
    "/assets/0e291f67c9274a1abdddeb3fd919cbaa.png",
    "/assets/1cbd08c76f8af6dddce02c5138971129.png",
    "/assets/6debd47ed13483642cf09e832ed0bc1b.png",
    "/assets/322c936a8c8be1b803cd94861bdfa868.png",
    "/assets/dd4dbc0016779df1378e7812eabaa04d.png"
  ]

  transform(value: any): unknown {
    if (value.icon && value.id) {
      return "https://cdn.discordapp.com/icons/" + value.id + "/" + value.icon + ".png";
    }
    if (value.avatar && value.discordId) {
      return "https://cdn.discordapp.com/icons/" + value.discordId + "/" + value.avatar + ".png";
    }
    return this.defaultImages[Math.floor(Math.random()*5)];
  }
}

import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AvatarPipe } from './_pipes/avatar.pipe';
import { IconPipe } from './_pipes/icon.pipe';

@NgModule({
  declarations: [
    AvatarPipe,
    IconPipe
  ],
	exports: [
    AvatarPipe,
    IconPipe
	],
  imports: [

  ],
  providers: [    

  ],
  bootstrap: [AppComponent]
})
export class CustomCommonModule { }

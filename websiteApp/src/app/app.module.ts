import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppData } from './_services/app.service';
import { GuardService } from './_services/guard.service';
import { CookieService } from 'ngx-cookie-service';

import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from '@angular/router';
import { DashboardLayoutComponent } from './dashboard/layout/layout.component';
import { WebsiteLayoutComponent } from './website/layout/layout.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { SyncService } from './_services/sync.service';
import { TagInputModule } from 'ngx-chips';
import { ToastrModule } from 'ngx-toastr';
import { CommonService } from './_services/common.service';
import { ColorPickerModule } from '@iplab/ngx-color-picker';
import { DashboardModule } from './dashboard/dashboard.module';
import { CustomCommonModule } from './common.module';

@NgModule({
  declarations: [
    AppComponent,
    DashboardLayoutComponent,
    WebsiteLayoutComponent
  ],
	exports: [
    DashboardLayoutComponent,
    WebsiteLayoutComponent
	],
  imports: [
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgbModule,
    NgSelectModule,
    FormsModule,
    TagInputModule,
    BrowserAnimationsModule,
    ColorPickerModule,
    ToastrModule.forRoot({
      positionClass :'toast-bottom-right'
    }),
    CustomCommonModule
  ],
  providers: [    
    AppData,
    GuardService,
    SyncService,
    CommonService,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

let config = {
	'appVersion':			"0.0.1", // verze aplikace
	'hostname':				window.location.hostname,
	'hostnameTitle':		'Adimi administrator tool'
};

export const AppConfig =  Object.freeze(config);


/* eslint-disable no-console */
// tslint:disable-next-line:no-console
/* eslint-enable no-console */

declare global {
	interface Window {
		convertDate: any;
		sendError: any;
		cycleJson: any;
		seen: any;
		gc: any;
	}
}

window.seen = [];

window.cycleJson = (key: any, value: any) => {

	if (value != null && typeof value == "object") {

		if (window.seen.indexOf(value) >= 0) {
			return;
		}
		window.seen.push(value);
	}
	return value;
};

window.convertDate = (date: any) => {
	let newDate = date.year + '-' + date.month + '-' + date.day;

	if (date.hours) {
		newDate += ' ' + date.hours + ':' + date.min + ':' + date.sec;
	}
	return newDate;
};

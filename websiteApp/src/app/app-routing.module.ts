import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WebsiteModule } from './website/website.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { WebsiteLayoutComponent } from './website/layout/layout.component';
import { DashboardLayoutComponent } from './dashboard/layout/layout.component';


const routes: Routes = [
  { 
    path: '',
    component: WebsiteLayoutComponent,
    loadChildren: (): any => WebsiteModule
  },
  {
    path: 'dashboard',
    component: DashboardLayoutComponent,
    loadChildren: (): any => DashboardModule
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

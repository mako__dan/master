import { Injectable } from '@angular/core';
import { APIService } from './api.service';
import { Router } from '@angular/router';
import { User } from '../_types/types';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { SyncService } from './sync.service';

@Injectable()
export class GuardService {
	public isLogged: boolean = false;
	public userData: User | undefined;

	constructor(
		private apiService: APIService,
		private router: Router,
		private cookieService: CookieService,
		private syncService: SyncService
	) {}

	/**
	 * return logged user to component
	 */
	public async getLoggedUser() {
		return JSON.parse(localStorage.getItem("loggedUser") ?? "{}");
	}

	/**
	 * return logged user to component
	 */
	public async updateLoggedUser(): Promise<void> {
		return new Promise(resolve => {

			this.apiService.getLoggedUserData().subscribe(async (userData: any) => {
				userData.guilds = userData.guilds?.filter((guild: any) => {
					if (guild.permissions & 0x8) {
						return true;
					}
					return false;
				});
				if (userData.discordId) {
					localStorage.setItem("loggedUser", JSON.stringify(userData));
					this.syncService.updateLoggedUser(await this.getLoggedUser());
					resolve();
				} else {
					localStorage.removeItem("loggedUser");
					this.router.navigate(['/login']);
				}
			}, () => {
				this.router.navigate(['/login']);
			});
		 });
	}


	public async openLoginWindow(): Promise<void> {
		window.open(environment.apiUrl + "api/auth/discord/", "Login to ZakkBot", "menubar=no,width=500,height=777,location=no,resizable=no,scrollbars=yes,status=no")
		window.addEventListener("message", (data) => {
			if (data) {
				this.updateLoggedUser();
			}
			this.router.navigate(["/dashboard"]);
		})
	}
	/**
	 * redirects to main page if user is not logged
	 */
	public async isUserLogged() {

	}

	/**
	 * logs user out
	 */
	public async logout() {
		await this.apiService.logout().subscribe(async () => {
			await localStorage.clear();
			await this.cookieService.deleteAll();
			this.router.navigate(["/", "login"]);
		});
	}
}

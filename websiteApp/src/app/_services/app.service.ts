import { AppConfig } from '../_config/app_config';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router, NavigationStart } from '@angular/router';

export type InternalStateType = {
	[key: string]: any,
};

@Injectable()
export class AppData {
	public appConfig = AppConfig;

	constructor(
		public route: ActivatedRoute,
		public router: Router,
	) {

		this.router.events.subscribe((event) => {

			if (event instanceof NavigationStart) {
				let routeUrl = this.router.url;

				if (routeUrl.indexOf('?') != -1) {
					routeUrl = routeUrl.substring(0, routeUrl.indexOf("?"));
				}

				if (routeUrl.indexOf(';') != -1) {
					routeUrl = routeUrl.substring(0, routeUrl.indexOf(";"));
				}

				if (routeUrl.indexOf('#') != -1) {
					routeUrl = routeUrl.substring(0, routeUrl.indexOf("#"));
				}

				this.set('previousUrl', routeUrl);
			}
		});
	}

	public _state: InternalStateType = { };

	public initAppConfig() {
		this.set('AppConfig', this.appConfig);
	}

	/**
	 * Already return a clone of the current state.
	 */
	public get state() {
		return this._state = this._clone(this._state);
	}

	/**
	 * Never allow mutation
	 */
	public set state(value) {
		throw new Error('do not mutate the `.state` directly');
	}


	/**
	 * Use our state getter for the clone.
	 */
	public get(prop?: any) {
		const state = this.state;
		return state.hasOwnProperty(prop) ? state[prop] : state;
	}

	public remove(prop: string) {

		if (this._state[prop]) {
			delete this._state[prop];
		}
	}

	/**
	 * Internally mutate our state.
	 */
	public set(prop: string, value: any) {
		return this._state[prop] = value;
	}

	/**
	 * Simple object clone.
	 */
	private _clone(object: InternalStateType) {
		return JSON.parse(JSON.stringify( object ));
	}

	/**
	 * get previous url to history || DEPRECATED !!!
	 */
	public getUrlHistory() {
		return true;
	}
}

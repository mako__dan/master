export class CommonService {
  constructor() {}

  public clearString(object: any): string {
    if (object) {
      if (object.id) {
        return object.id;
      }
      if (object.value) {
        return object.value;
      }
      return object;
    } else {
      return "";
    }
  }

  public fillString(string: string, arrayToCompare: any): any {
    if (arrayToCompare) {
      return arrayToCompare.find((channel: any) => channel.id === string);
    }
  }

  public clearArray(array: any): string[] {
    const newArray: string[] = [];
    array.forEach((data: any) => {
      if (data.id) {
        newArray.push(data.id);
        return;
      }
      if (data.value) {
        newArray.push(data.value);
        return;
      }
      newArray.push(data);
    });
    return newArray;
  }

  public fillArray(array: any, arrayToCompare: any): string[] {
    const newArray: string[] = [];
    if (arrayToCompare) {
      array.forEach((data: string) => {
        const newObject = arrayToCompare.find(
          (channel: any) => channel.id === data
        );
        newArray.push(newObject);
      });
    }
    return newArray;
  }
}

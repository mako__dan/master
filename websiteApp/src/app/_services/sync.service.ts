import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SyncService {

	constructor() { }

	private _routeToParent = new BehaviorSubject({});
	routeToParent = this._routeToParent.asObservable();

	sendRouteToParent(newGuildId: any) {
		this._routeToParent.next(newGuildId);
	}

	private _loggedUser = new BehaviorSubject({});
	loggedUser = this._loggedUser.asObservable();

	async updateLoggedUser(newUserData: any) {
		this._loggedUser.next(await newUserData);
	}
}
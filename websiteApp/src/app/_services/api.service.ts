/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.
import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: "root"
})
export class APIService {

	constructor(
		private http: HttpClient
	){}

	//#region  User
	public getLoggedUserData(): Observable<Object> {
		return this.http.get(environment.apiUrl + 'api/auth/discord/loggedData',{ 
			withCredentials: true 
		});
	}

	public logout(): Observable<Object> {
		return this.http.get(environment.apiUrl + 'api/auth/logout',{ 
			withCredentials: true 
		});
	}
	//#endregion

	//#region  Client
	public listAvailableBots(guildId: string): Observable<Object> {
		return this.http.get(environment.apiUrl + 'api/manage/client/list',
		{ 
			withCredentials: true, 
			params: new HttpParams()
				.append("guildid", guildId)
		});
	}
	//#endregion

	//#region  Guild
	public getGuild(botId: string, guildId: string): Observable<Object> {
		return this.http.get(environment.apiUrl + 'api/manage/guild/get', {
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
				.append("botid", botId) 
		});
	}

	public updateGuild(botId: string, guildId: string, id: string, data: any): Observable<Object> {
		return this.http.post(environment.apiUrl + 'api/manage/guild/set', data, {
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
				.append("_id", id)
				.append("botid", botId)
		});
	}
	//#endregion

	//#region  Module
	public getModule(botId: string, guildId: string, moduleName: string): Observable<Object> {
		return this.http.get(environment.apiUrl + 'api/manage/module/get', { 
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
				.append("modulename", moduleName)
				.append("botid", botId
				)
		});
	}

	public listModule(
	botId: string, guildId: string): Observable<Object> {
		return this.http.get(environment.apiUrl + 'api/manage/module/list', { 
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
				.append("botid", botId)
		});
	}

	public updateModule(botId: string, guildId: string, id: string, data: any): Observable<Object> {
		return this.http.post(environment.apiUrl + 'api/manage/module/set', data, { 
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
				.append("_id", id)
				.append("botid", botId)
		});
	}

	public toggleModule(botId: string, guildId: string, id: string): Observable<Object> {
		return this.http.post(environment.apiUrl + 'api/manage/module/toggle', null, { 
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
				.append("_id", id)
				.append("systemid", id)
				.append("botid", botId) 
		});
	}
	//#endregion

	//#region  Command
	public getCommand(botId: string, guildId: string, moduleName: string, commandName: string): Observable<Object> {
		return this.http.get(environment.apiUrl + 'api/manage/command/get', { 
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
				.append("modulename", moduleName)
				.append("commandname", commandName)
				.append("botid", botId) 
		});
	}

	public listCommands(botId: string, guildId: string, moduleName: string): Observable<Object> {
		return this.http.get(environment.apiUrl + 'api/manage/command/list', { 
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
				.append("modulename", moduleName)
				.append("botid", botId)
		});
	}

	public updateCommand(botId: string, guildId: string, id: string, data: any): Observable<Object> {
		return this.http.post(environment.apiUrl + 'api/manage/command/set', data, { 
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
				.append("_id", id)
				.append("botid", botId)
		});
	}

	public toggleCommand(botId: string, guildId: string, id: string): Observable<Object> {
		return this.http.post(environment.apiUrl + 'api/manage/command/toggle', null, { 
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
				.append("_id", id)
				.append("systemid", id)
				.append("botid", botId) 
		});
	}
	//#endregion

	//#region  Fetch
	public fetchChannels(guildId: string): Observable<Object> {
		return this.http.get(environment.apiUrl + 'api/fetch/channels', { 
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
		});
	}

	public fetchRoles(guildId: string): Observable<Object> {
		return this.http.get(environment.apiUrl + 'api/fetch/roles', { 
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
		});
	}

	public fetchEmotes(guildId: string): Observable<Object> {
		return this.http.get(environment.apiUrl + 'api/fetch/emotes', { 
			withCredentials: true,
			params: new HttpParams()
				.append("guildid", guildId)
		});
	}
	//#endregion
	

}

